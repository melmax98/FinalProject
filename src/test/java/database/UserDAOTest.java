package database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import database.entity.user.User;
import database.entity.user.UserType;

public class UserDAOTest {
	private DBManager database;
	private Connection connection;
	private Statement initStatement;

	@Before
	public void init() throws SQLException {
		database = Mockito.mock(DBManager.class);
		connection = Mockito.mock(Connection.class);
		initStatement = Mockito.mock(Statement.class);

		when(database.getConnection()).thenReturn(connection);
		when(connection.createStatement()).thenReturn(initStatement);
		when(initStatement.executeUpdate(UserDAO.INIT_TABLE)).thenReturn(0);
	}

	@Test
	public void initTableOnGetInstanceTest() throws SQLException {
		UserDAO.getInstance(database);

		verify(database).getConnection();
		verify(connection).createStatement();
		verify(initStatement).executeUpdate(UserDAO.INIT_TABLE);
		// Verify that all db enteties are closed
		verify(database).close(connection, initStatement);
	}

	@Test
	public void shouldCreateUserTest() throws SQLException {
		UserDAO dbm = UserDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(UserDAO.INSERT_USER, Statement.RETURN_GENERATED_KEYS)).thenReturn(ps);
		when(ps.getGeneratedKeys()).thenReturn(rs);
		doNothing().when(ps).setString(1, "login");
		doNothing().when(ps).setString(2, "pass");
		doNothing().when(ps).setString(3, "administrator");
		when(ps.executeUpdate()).thenReturn(1);
		when(rs.next()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(1);

		User user = new User();
		user.setId(1);
		user.setLogin("login");
		user.setPassword("pass");
		user.setRole(UserType.ADMIN);

		User user2 = dbm.createUser("login", "pass", UserType.ADMIN);

		assertEquals(user.toString(), user2.toString());
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldThrowSQLExceptionWhenNoRowsModifiedTest() throws SQLException {
		UserDAO dbm = UserDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(UserDAO.INSERT_USER, Statement.RETURN_GENERATED_KEYS)).thenReturn(ps);
		when(ps.getGeneratedKeys()).thenReturn(rs);
		doNothing().when(ps).setString(1, "login");
		doNothing().when(ps).setString(2, "pass");
		doNothing().when(ps).setString(3, "administrator");
		when(ps.executeUpdate()).thenReturn(0);

		SQLException thrown = assertThrows(SQLException.class, () -> dbm.createUser("login", "pass", UserType.ADMIN));

		assertEquals("Couldn't insert user to database", thrown.getMessage());
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldThrowSQLExceptionWhenCannotGetIdModifiedTest() throws SQLException {
		UserDAO dbm = UserDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(UserDAO.INSERT_USER, Statement.RETURN_GENERATED_KEYS)).thenReturn(ps);
		when(ps.getGeneratedKeys()).thenReturn(rs);
		doNothing().when(ps).setString(1, "login");
		doNothing().when(ps).setString(2, "pass");
		doNothing().when(ps).setString(3, "administrator");
		when(ps.executeUpdate()).thenReturn(1);
		when(rs.next()).thenReturn(false);

		SQLException thrown = assertThrows(SQLException.class, () -> dbm.createUser("login", "pass", UserType.ADMIN));

		assertEquals("Couldn't insert user to database", thrown.getMessage());
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldGetAllUsersTest() throws SQLException {
		List<User> listActual = new ArrayList<>();

		List<User> listExpected = new ArrayList<>();
		User user = new User();
		user.setId(1);
		user.setLogin("test");
		user.setRole(UserType.DRIVER);
		listExpected.add(user);

		UserDAO dbm = UserDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.createStatement()).thenReturn(st);
		when(st.executeQuery(UserDAO.FIND_ALL_USERS)).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("login")).thenReturn("test");
		when(rs.getString("role")).thenReturn("driver");

		listActual = dbm.getAllUsers();

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, st, rs);
	}

	@Test
	public void shouldThrowIfNoConnectionWithDBAllUsersTest() throws SQLException {
		Statement st = Mockito.mock(Statement.class);
		when(connection.createStatement()).thenReturn(st);
		doThrow(new SQLException("Was unnable  to get users from db")).when(st).executeQuery(UserDAO.FIND_ALL_USERS);

		UserDAO dbm = UserDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.getAllUsers());
		verify(database).close(connection, st);
	}

	@Test
	public void shouldGetUserByIdTest() throws SQLException {
		User expectedUser = new User();
		expectedUser.setId(1);
		expectedUser.setLogin("test");
		expectedUser.setRole(UserType.ADMIN);

		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(UserDAO.FIND_USER_BY_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);

		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("login")).thenReturn("test");
		when(rs.getString("role")).thenReturn("administrator");

		UserDAO dbm = UserDAO.getInstance(database);

		User actualUser = dbm.getUser(1);

		assertEquals(expectedUser.toString(), actualUser.toString());
		verify(database).close(connection, ps, rs);
	}

	@Test
	public void shouldThrowIfNoConnectionWithDBGetUserByIdTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(UserDAO.FIND_USER_BY_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);

		doThrow(new SQLException("")).when(ps).executeQuery();

		UserDAO dbm = UserDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.getUser(1));
		verify(database).close(connection, ps, null);
	}

	@Test
	public void shouldReturnTrueIfIsAnyAdminTest() throws SQLException {
		Statement s = Mockito.mock(Statement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)).thenReturn(s);
		when(s.executeQuery(UserDAO.IS_ANY_ADMIN)).thenReturn(rs);
		when(rs.getRow()).thenReturn(1);

		UserDAO dbm = UserDAO.getInstance(database);

		assertTrue(dbm.isAnyAdmin());
		verify(database).close(connection, s, rs);
	}

	@Test
	public void shouldReturnFalseIfNoAdminTest() throws SQLException {
		Statement st = Mockito.mock(Statement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)).thenReturn(st);
		when(st.executeQuery(UserDAO.IS_ANY_ADMIN)).thenReturn(rs);
		when(rs.getRow()).thenReturn(0);

		UserDAO dbm = UserDAO.getInstance(database);

		assertFalse(dbm.isAnyAdmin());
		verify(database).close(connection, st, rs);
	}

	@Test
	public void shouldThrowIfNoConnectionWithDBisAnyAdminTest() throws SQLException {
		Statement st = Mockito.mock(Statement.class);

		when(connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)).thenReturn(st);
		doThrow(new SQLException("")).when(st).executeQuery(UserDAO.IS_ANY_ADMIN);

		UserDAO dbm = UserDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.isAnyAdmin());
		verify(database).close(connection, st, null);
	}

	@Test
	public void shouldDeleteUserTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(UserDAO.DELETE_USER)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);

		UserDAO dbm = UserDAO.getInstance(database);

		assertTrue(dbm.deleteUser(1));
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldThrowIfNoConnetionWithDBTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(UserDAO.DELETE_USER)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doThrow(new SQLException("")).when(ps).executeUpdate();

		UserDAO dbm = UserDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.deleteUser(1));
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldAuthUserIfExistsTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(UserDAO.AUTH_USER)).thenReturn(ps);
		doNothing().when(ps).setString(1, "test");
		doNothing().when(ps).setString(2, "pass");
		when(ps.executeQuery()).thenReturn(rs);

		when(rs.next()).thenReturn(true).thenReturn(false);

		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("login")).thenReturn("test");
		when(rs.getString("role")).thenReturn("driver");

		UserDAO dbm = UserDAO.getInstance(database);

		dbm.authUser("test", "pass");
		verify(database).close(connection, ps, rs);
	}

	@Test
	public void shouldThrowExceptionIfNoConnectionWithDBTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(UserDAO.AUTH_USER)).thenReturn(ps);
		doNothing().when(ps).setString(1, "test");
		doNothing().when(ps).setString(1, "pass");
		doThrow(new SQLException("")).when(ps).executeQuery();

		UserDAO dbm = UserDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.authUser("test", "pass"));
		verify(database).close(connection, ps, null);
	}

	@Test
	public void shouldUpdateUserTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(UserDAO.UPDATE_USER)).thenReturn(ps);

		User testUser = new User();
		testUser.setLogin("test");
		testUser.setPassword("pass");
		testUser.setRole(UserType.DRIVER);
		testUser.setId(1);

		doNothing().when(ps).setString(1, testUser.getLogin());
		doNothing().when(ps).setString(2, testUser.getPassword());
		doNothing().when(ps).setString(3, testUser.getRole().getValue());
		doNothing().when(ps).setInt(4, testUser.getId());
		when(ps.executeUpdate()).thenReturn(1);

		UserDAO dbm = UserDAO.getInstance(database);

		dbm.updateUser(testUser);

		verify(database).close(connection, ps);
	}

	@Test
	public void shouldSetNullIfNoRoleUpdateUserTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(UserDAO.UPDATE_USER)).thenReturn(ps);

		User testUser = new User();
		testUser.setLogin("test");
		testUser.setPassword("pass");
		testUser.setId(1);

		doNothing().when(ps).setString(1, testUser.getLogin());
		doNothing().when(ps).setString(2, testUser.getPassword());
		doNothing().when(ps).setString(3, null);
		doNothing().when(ps).setInt(4, testUser.getId());
		when(ps.executeUpdate()).thenReturn(1);

		UserDAO dbm = UserDAO.getInstance(database);

		dbm.updateUser(testUser);

		verify(database).close(connection, ps);
	}

	@Test
	public void shouldThrowIfNoConnectionWithDBUpdateUserTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(UserDAO.UPDATE_USER)).thenReturn(ps);

		User testUser = new User();
		testUser.setLogin("test");
		testUser.setPassword("pass");
		testUser.setId(1);

		doNothing().when(ps).setString(1, testUser.getLogin());
		doNothing().when(ps).setString(2, testUser.getPassword());
		doNothing().when(ps).setString(3, null);
		doNothing().when(ps).setInt(4, testUser.getId());

		doThrow(new SQLException("")).when(ps).executeUpdate();

		UserDAO dbm = UserDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.updateUser(testUser));
		verify(database).close(connection, ps);
	}
	
	@Test
	public void shouldGetCountOfUsersTest() throws SQLException {
		UserDAO dbm = UserDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);
		
		when(connection.createStatement()).thenReturn(st);
		when(st.executeQuery(UserDAO.COUNT_USERS)).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(1);
		assertEquals(new Integer(1), dbm.getCountUsers());
		verify(database).close(connection, st, rs);
	}
	
	@Test
	public void shouldThrowIfNoConnectionCountOfUsersTest() throws SQLException {
		UserDAO dbm = UserDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);
		
		when(connection.createStatement()).thenReturn(st);
		doThrow(new SQLException("")).when(st).executeQuery(UserDAO.COUNT_USERS);
		assertThrows(SQLException.class, () -> dbm.getCountUsers());
	}
}