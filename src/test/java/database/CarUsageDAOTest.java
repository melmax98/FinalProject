package database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import database.bean.CarUsage;
import database.bean.UsageStatus;

public class CarUsageDAOTest {
	private DBManager database;
	private Connection connection;
	private Statement initStatement;

	@Before
	public void init() throws SQLException {
		database = Mockito.mock(DBManager.class);
		connection = Mockito.mock(Connection.class);
		initStatement = Mockito.mock(Statement.class);

		when(database.getConnection()).thenReturn(connection);
		when(connection.createStatement()).thenReturn(initStatement);
		when(initStatement.executeUpdate(CarUsageDAO.INIT_TABLE)).thenReturn(0);
		when(initStatement.executeUpdate(CarDAO.INIT_TABLE)).thenReturn(0);
	}

	@Test
	public void initTableOnGetInstanceTest() throws SQLException {
		CarUsageDAO.getInstance(database);

		verify(database).getConnection();
		verify(connection).createStatement();
		verify(initStatement).executeUpdate(CarUsageDAO.INIT_TABLE);
		/** Verify that all db enteties are closed*/
		 
		verify(database).close(connection, initStatement);
	}

	@Test
	public void shouldCreateCarUsageTest() throws SQLException {
		CarUsageDAO dbm = CarUsageDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		doNothing().when(connection).setAutoCommit(false);
		when(connection.prepareStatement(CarDAO.FIND_CAR_BY_ID + " FOR UPDATE")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 3);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);

		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("manufacturer")).thenReturn("manufacturer");
		when(rs.getString("model")).thenReturn("model");
		when(rs.getString("type")).thenReturn("basic");
		when(rs.getInt("seats")).thenReturn(5);
		when(rs.getString("status")).thenReturn("without driver");

		doNothing().when(database).close(rs);
		doNothing().when(database).close(ps);

		when(connection.prepareStatement(CarUsageDAO.CREATE_CAR_USAGE)).thenReturn(ps);

		doNothing().when(ps).setString(1, UsageStatus.AVAILABLE.getValue());
		doNothing().when(ps).setInt(2, 2);
		doNothing().when(ps).setInt(3, 3);
		when(ps.executeUpdate()).thenReturn(1);

		doNothing().when(database).close(ps);

		when(connection.prepareStatement(CarUsageDAO.UPDATE_CAR_STATUS)).thenReturn(ps);
		doNothing().when(ps).setString(1, UsageStatus.AVAILABLE.getValue());
		doNothing().when(ps).setInt(2, 2);

		assertTrue(dbm.createCarUsage(UsageStatus.AVAILABLE, 2, 3));
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldReturnFalseIfCarIsNullTest() throws SQLException {
		CarUsageDAO dbm = CarUsageDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		doNothing().when(connection).setAutoCommit(false);
		when(connection.prepareStatement(CarDAO.FIND_CAR_BY_ID + " FOR UPDATE")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 3);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(false);

		assertFalse(dbm.createCarUsage(UsageStatus.AVAILABLE, 2, 3));
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldReturnFalseIfCarIsInUsage() throws SQLException {
		CarUsageDAO dbm = CarUsageDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		doNothing().when(connection).setAutoCommit(false);
		when(connection.prepareStatement(CarDAO.FIND_CAR_BY_ID + " FOR UPDATE")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 3);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);

		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("manufacturer")).thenReturn("manufacturer");
		when(rs.getString("model")).thenReturn("model");
		when(rs.getString("type")).thenReturn("basic");
		when(rs.getInt("seats")).thenReturn(5);
		when(rs.getString("status")).thenReturn("in usage");

		assertFalse(dbm.createCarUsage(UsageStatus.WITHOUT_DRIVER, 2, 3));
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldReturnFalseIfTheSameStatus() throws SQLException {
		CarUsageDAO dbm = CarUsageDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		doNothing().when(connection).setAutoCommit(false);
		when(connection.prepareStatement(CarDAO.FIND_CAR_BY_ID + " FOR UPDATE")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 3);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);

		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("manufacturer")).thenReturn("manufacturer");
		when(rs.getString("model")).thenReturn("model");
		when(rs.getString("type")).thenReturn("basic");
		when(rs.getInt("seats")).thenReturn(5);
		when(rs.getString("status")).thenReturn("without driver");

		assertFalse(dbm.createCarUsage(UsageStatus.WITHOUT_DRIVER, 2, 3));
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldThrowIfNoConnectionWithDBCreateTest() throws SQLException {
		CarUsageDAO dbm = CarUsageDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		doNothing().when(connection).setAutoCommit(false);
		when(connection.prepareStatement(CarDAO.FIND_CAR_BY_ID + " FOR UPDATE")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 3);

		doThrow(new SQLException("")).when(ps).executeQuery();

		assertThrows(SQLException.class, () -> dbm.createCarUsage(UsageStatus.WITHOUT_DRIVER, 2, 3));
		verify(database).rollback(connection);
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldGetCarUsageStatisticsTest() throws SQLException {
		List<CarUsage> listActual = new ArrayList<>();

		List<CarUsage> listExpected = new ArrayList<>();
		CarUsage carUsage = new CarUsage();

		carUsage.setId(1);
		carUsage.setStatus(UsageStatus.AVAILABLE);
		carUsage.setDriverId(2);
		carUsage.setCarId(3);
		carUsage.setAt(LocalDateTime.parse("2020-09-30 00:00:00", CarUsage.formatter));
		listExpected.add(carUsage);

		CarUsageDAO dbm = CarUsageDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.createStatement()).thenReturn(st);
		when(st.executeQuery(CarUsageDAO.GET_CAR_USAGE_STATISTICS)).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);

		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("status")).thenReturn("available");
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getInt("car_id")).thenReturn(3);
		when(rs.getString("at")).thenReturn("2020-09-30 00:00:00");

		listActual = dbm.getCarUsageStatistics();

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, st, rs);
	}
	
	@Test
	public void shouldGetCarUsageStatisticsWithPaginationTest() throws SQLException {
		List<CarUsage> listActual = new ArrayList<>();

		List<CarUsage> listExpected = new ArrayList<>();
		CarUsage carUsage = new CarUsage();

		carUsage.setId(1);
		carUsage.setStatus(UsageStatus.AVAILABLE);
		carUsage.setDriverId(2);
		carUsage.setCarId(3);
		carUsage.setAt(LocalDateTime.parse("2020-09-30 00:00:00", CarUsage.formatter));
		listExpected.add(carUsage);

		CarUsageDAO dbm = CarUsageDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.createStatement()).thenReturn(ps);
		when(connection.prepareStatement(CarUsageDAO.GET_CAR_USAGE_STATISTICS + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(2, 1);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);

		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("status")).thenReturn("available");
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getInt("car_id")).thenReturn(3);
		when(rs.getString("at")).thenReturn("2020-09-30 00:00:00");

		listActual = dbm.getCarUsageStatistics(1, 1);

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, null, rs);
		verify(database).close(ps);
	}

	@Test
	public void shouldThrowIfNoConnectionWithDBGetTest() throws SQLException {
		CarUsageDAO dbm = CarUsageDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);

		when(connection.createStatement()).thenReturn(st);

		doThrow(SQLException.class).when(st).executeQuery(CarUsageDAO.GET_CAR_USAGE_STATISTICS);

		assertThrows(SQLException.class, () -> dbm.getCarUsageStatistics());
		verify(database).close(connection, st, null);
	}
	
	@Test
	public void shouldGetCarUsageCountTest() throws SQLException {
		CarUsageDAO dbm = CarUsageDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);
		
		when(connection.createStatement()).thenReturn(st);
		when(st.executeQuery(CarUsageDAO.COUNT_CAR_USAGE)).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(1);
		assertEquals(new Integer(1), dbm.getCountCarUsage());
		verify(database).close(connection, st, rs);
	}
	
	@Test
	public void shouldThrowIfNoConnectionCountOfCarUsageTest() throws SQLException {
		CarUsageDAO dbm = CarUsageDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);
		
		when(connection.createStatement()).thenReturn(st);
		doThrow(new SQLException("")).when(st).executeQuery(CarUsageDAO.COUNT_CAR_USAGE);
		assertThrows(SQLException.class, () -> dbm.getCountCarUsage());
	}
	
	@Test
	public void shouldGetDriverIdByIdTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarUsageDAO.GET_DRIVER_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);

		when(rs.getInt(1)).thenReturn(1);
		
		CarUsageDAO dbm = CarUsageDAO.getInstance(database);

		assertEquals(new Integer(1), dbm.getDriverIdByCarId(1));
		verify(database).close(connection, ps, rs);
	}
	
	@Test
	public void shouldThrowInNoConnectionGetDriverIdByIdTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarUsageDAO.GET_DRIVER_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);

		doThrow(new SQLException("")).when(ps).executeQuery();

		CarUsageDAO dbm = CarUsageDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.getDriverIdByCarId(1));
		verify(database).close(connection, ps, null);
	}
	
	@Test
	public void shouldGetLastCarUsageByCarId() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarUsageDAO.GET_LAST_CAR_USAGE_BY_CAR_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);

		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("status")).thenReturn("available");
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getInt("car_id")).thenReturn(3);
		when(rs.getString("at")).thenReturn("2020-09-30 00:00:00");
		
		CarUsageDAO dbm = CarUsageDAO.getInstance(database);

		assertNotNull(dbm.getLastCarUsageByCarId(1));
		verify(database).close(connection, ps, rs);
	}
	
	@Test
	public void shouldReturnNullIfNoCarUsageGetLastCarUsageByCarId() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarUsageDAO.GET_LAST_CAR_USAGE_BY_CAR_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(false);
		
		CarUsageDAO dbm = CarUsageDAO.getInstance(database);

		assertNull(dbm.getLastCarUsageByCarId(1));
		verify(database).close(connection, ps, rs);
	}
	
	@Test
	public void shouldThrowIfNoConnectionGetLastCarUsageByCarId() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarUsageDAO.GET_LAST_CAR_USAGE_BY_CAR_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);

		doThrow(new SQLException("")).when(ps).executeQuery();

		CarUsageDAO dbm = CarUsageDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.getLastCarUsageByCarId(1));
		verify(database).close(connection, ps, null);
	}
	
	@Test
	public void shouldGetLastCarUsageByDriverId() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarUsageDAO.GET_LAST_CAR_USAGE_BY_DRIVER_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);

		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("status")).thenReturn("available");
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getInt("car_id")).thenReturn(3);
		when(rs.getString("at")).thenReturn("2020-09-30 00:00:00");
		
		CarUsageDAO dbm = CarUsageDAO.getInstance(database);

		assertNotNull(dbm.getLastCarUsageByDriverId(1));
		verify(database).close(connection, ps, rs);
	}
	
	@Test
	public void shouldReturnNullIfNoCarUsageGetLastCarUsageByDriverId() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarUsageDAO.GET_LAST_CAR_USAGE_BY_DRIVER_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(false);
		
		CarUsageDAO dbm = CarUsageDAO.getInstance(database);

		assertNull(dbm.getLastCarUsageByDriverId(1));
		verify(database).close(connection, ps, rs);
	}
	
	@Test
	public void shouldThrowIfNoConnectionGetLastCarUsageByDriverId() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarUsageDAO.GET_LAST_CAR_USAGE_BY_DRIVER_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);

		doThrow(new SQLException("")).when(ps).executeQuery();

		CarUsageDAO dbm = CarUsageDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.getLastCarUsageByDriverId(1));
		verify(database).close(connection, ps, null);
	}
}