package database.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDateTime;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import database.bean.CarUsage;
import database.entity.car.CarType;
import database.entity.order.Order;
import database.entity.order.OrderStatus;

public class OrderTest {

	@Test
	public void shouldCreateJSONObjectTest() {
		Order order = new Order();
		order.setId(1);
		order.setCarId(2);
		order.setDriverId(3);
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setTo(new JSONArray(new Double[] { 48.22812192199157, 27.414292335510254 }));
		order.setStatus(OrderStatus.CREATED);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:10:00", CarUsage.formatter));
		order.setPrice(100);
		order.setPassengers(5);
		order.setUserId(4);
		order.setWaitTime(3);

		assertEquals(new JSONObject(
				"{\"driver_id\":3,\"passengers\":5,\"finished_at\":\"2020-10-01 00:10:00\",\"wait_time\":3,\"created_at\":\"2020-10-01 00:00:00\",\"car_type\":\"BASIC\",\"user_id\":4,\"price\":100,\"from\":[49.22812192199157,28.414292335510254],\"id\":1,\"to\":[48.22812192199157,27.414292335510254],\"car_id\":2,\"status\":\"CREATED\"}")
						.toString(),
				order.toString());
	}

	@Test
	public void shouldCreateOrderObjectFromJSONObject() {
		Order orderExpected = new Order();
		orderExpected.setId(1);
		orderExpected.setCarId(2);
		orderExpected.setDriverId(3);
		orderExpected.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		orderExpected.setTo(new JSONArray(new Double[] { 48.22812192199157, 27.414292335510254 }));
		orderExpected.setStatus(OrderStatus.CREATED);
		orderExpected.setCarType(CarType.BASIC);
		orderExpected.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		orderExpected.setFinishedAt(LocalDateTime.parse("2020-10-01 00:10:00", CarUsage.formatter));
		orderExpected.setPrice(100);
		orderExpected.setPassengers(5);
		orderExpected.setUserId(4);
		orderExpected.setWaitTime(3);

		JSONObject json = new JSONObject(
				"{\"driver_id\":3,\"passengers\":5,\"finished_at\":\"2020-10-01 00:10:00\",\"wait_time\":3,\"created_at\":\"2020-10-01 00:00:00\",\"car_type\":\"BASIC\",\"user_id\":4,\"price\":100,\"from\":[49.22812192199157,28.414292335510254],\"id\":1,\"to\":[48.22812192199157,27.414292335510254],\"car_id\":2,\"status\":\"CREATED\"}");

		Order orderActual = Order.fromJSON(json);

		assertEquals(orderExpected.toString(), orderActual.toString());
	}

	@Test
	public void shouldCreateOrderObjectFromJSONString() {
		Order orderExpected = new Order();
		orderExpected.setId(1);
		orderExpected.setCarId(2);
		orderExpected.setDriverId(3);
		orderExpected.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		orderExpected.setTo(new JSONArray(new Double[] { 48.22812192199157, 27.414292335510254 }));
		orderExpected.setStatus(OrderStatus.CREATED);
		orderExpected.setCarType(CarType.BASIC);
		orderExpected.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		orderExpected.setFinishedAt(LocalDateTime.parse("2020-10-01 00:10:00", CarUsage.formatter));
		orderExpected.setPrice(100);
		orderExpected.setPassengers(5);
		orderExpected.setUserId(4);
		orderExpected.setWaitTime(3);

		String json = new String(
				"{\"driver_id\":3,\"passengers\":5,\"finished_at\":\"2020-10-01 00:10:00\",\"wait_time\":3,\"created_at\":\"2020-10-01 00:00:00\",\"car_type\":\"BASIC\",\"user_id\":4,\"price\":100,\"from\":[49.22812192199157,28.414292335510254],\"id\":1,\"to\":[48.22812192199157,27.414292335510254],\"car_id\":2,\"status\":\"CREATED\"}");

		Order orderActual = Order.fromJSON(json);

		assertEquals(orderExpected.toString(), orderActual.toString());
	}
	
	@Test
	public void shouldConvertToJSONWithoutCreatedAndFinishedTimeTest() {
		Order orderExpected = new Order();
		orderExpected.setId(1);
		orderExpected.setCarId(2);
		orderExpected.setDriverId(3);
		orderExpected.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		orderExpected.setTo(new JSONArray(new Double[] { 48.22812192199157, 27.414292335510254 }));
		orderExpected.setStatus(OrderStatus.CREATED);
		orderExpected.setCarType(CarType.BASIC);
		orderExpected.setPrice(100);
		orderExpected.setPassengers(5);
		orderExpected.setUserId(4);
		orderExpected.setWaitTime(3);
		
		assertNotNull(orderExpected.toJSON());
	}
	
	@Test
	public void shouldReturnGeoStringFromJSONArrayTest() {
		JSONArray jsonArray = new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 });
		assertEquals("POINT(28.414292335510254 49.22812192199157)", Order.jsonArrayToGeoString(jsonArray));
	}
	
	@Test
	public void shouleReturnJSONArrayFromGeoString() {
		JSONArray jsonArray = Order.geoStringToJSONArray("POINT(28.414292335510254 49.22812192199157)");
		assertEquals(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }).toString(), jsonArray.toString());
	}

	@Test
	public void shouldGetOrderStatusValueTest() {
		assertEquals("done", OrderStatus.DONE.getValue());
	}

	@Test
	public void shouldValueFindByValueOrderStatusTest() {
		assertNotNull(OrderStatus.findByValue("done"));
	}
	
	@Test
	public void shouldReturnNullIfNoSuchTypeFindByValueOrderStatusTest() {
		assertNull(OrderStatus.findByValue("nothing"));
	}
}
