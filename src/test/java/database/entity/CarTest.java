package database.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.json.JSONObject;
import org.junit.Test;

import database.entity.car.Car;
import database.entity.car.CarStatus;
import database.entity.car.CarType;

public class CarTest {

	@Test
	public void shouldCreateJSONObjectTest() {
		Car car = new Car();

		car.setId(1);
		car.setManufacturer("Lamborgini");
		car.setModel("Galardo");
		car.setSeats(2);
		car.setStatus(CarStatus.AVAILABLE);
		car.setType(CarType.COMFORT);

		assertEquals(new JSONObject("{\"id\": 1,\"manufacturer\": \"Lamborgini\",\"model\": \"Galardo\",\"seats\": 2,\"status\": \"AVAILABLE\",\"type\": \"COMFORT\"}").toString(),
				car.toString());
	}

	@Test
	public void shouldCreateCarObjectFromJSONObject() {
		Car carExpected = new Car();

		carExpected.setId(1);
		carExpected.setManufacturer("Lamborgini");
		carExpected.setModel("Galardo");
		carExpected.setSeats(2);
		carExpected.setStatus(CarStatus.AVAILABLE);
		carExpected.setType(CarType.COMFORT);
		
		JSONObject json = new JSONObject("{\"id\": 1,\"manufacturer\": \"Lamborgini\",\"model\": \"Galardo\",\"seats\": 2,\"status\": \"AVAILABLE\",\"type\": \"COMFORT\"}");
		
		Car carActual = Car.fromJSON(json);
		
		assertEquals(carExpected.toString(), carActual.toString());
	}
	
	@Test
	public void shouldCreateCarObjectFromJSONString() {
		Car carExpected = new Car();

		carExpected.setId(1);
		carExpected.setManufacturer("Lamborgini");
		carExpected.setModel("Galardo");
		carExpected.setSeats(2);
		carExpected.setStatus(CarStatus.AVAILABLE);
		carExpected.setType(CarType.COMFORT);
		
		String json = new String("{\"id\": 1,\"manufacturer\": \"Lamborgini\",\"model\": \"Galardo\",\"seats\": 2,\"status\": \"AVAILABLE\",\"type\": \"COMFORT\"}");
		
		Car carActual = Car.fromJSON(json);
		
		assertEquals(carExpected.toString(), carActual.toString());
	}
	
	@Test
	public void shouldGetCarStatusValueTest() {
		assertEquals("available" ,CarStatus.AVAILABLE.getValue());
	}
	
	@Test
	public void shouldFindByValueCarStatusTest() {
		assertEquals(CarStatus.AVAILABLE ,CarStatus.findByValue("available"));
	}
	
	@Test
	public void shouldReturnNullIfNoSuchTypeFindByValueCarStatusTest() {
		assertNull(CarStatus.findByValue("nothing"));
	}
	
	@Test
	public void shouldGetCarTypeValueTest() {
		assertEquals("basic" ,CarType.BASIC.getValue());
	}
	
	@Test
	public void shouldFindByValueCarTypeTest() {
		assertEquals(CarType.BASIC ,CarType.findByValue("basic"));
	}
	
	@Test
	public void shouldReturnNullIfNoSuchTypeFindByValueCarTypeTest() {
		assertNull(CarType.findByValue("nothing"));
	}

}
