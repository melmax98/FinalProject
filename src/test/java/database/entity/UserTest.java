package database.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.json.JSONObject;
import org.junit.Test;

import com.auth0.jwt.interfaces.DecodedJWT;

import database.entity.user.User;
import database.entity.user.UserType;

public class UserTest {

	@Test
	public void toStringShouldAvoidPasswordTest() {
		User user = new User();

		user.setId(1);
		user.setLogin("First");
		user.setPassword("pass");
		user.setRole(UserType.CLIENT);

		assertEquals(new JSONObject("{\"id\": 1,\"login\": \"First\",\"role\": \"CLIENT\"}").toString(),
				user.toString());
	}

	@Test
	public void shouldGetUserFromJSONObjectTest() {
		User userExpected = new User();
		userExpected.setId(5);
		userExpected.setLogin("Second");
		userExpected.setPassword("2");
		userExpected.setRole(UserType.DRIVER);

		User userFromJSON = User.fromJSON(
				new JSONObject("{\"id\": 5,\"login\": \"Second\",\"role\": \"DRIVER\", \"password\": \"2\"}"));
		assertEquals(userExpected.toString(), userFromJSON.toString());
		assertEquals(userExpected.getPassword(), userFromJSON.getPassword());
	}

	@Test
	public void shouldHandleJSONExceptionIfNoPassword() {
		User userFromJSON = User.fromJSON(new JSONObject("{\"id\": 5,\"login\": \"Second\",\"role\": \"DRIVER\"}"));
		assertNotNull(userFromJSON);
	}

	@Test
	public void shouldReturnJWTString() {
		User user = new User();
		user.setId(1);
		user.setLogin("First");
		user.setPassword("pass");
		user.setRole(UserType.CLIENT);

		assertNotNull(user.toJWT("secret"));
	}

	@Test
	public void shouldGetUserFromJSONStringTest() {
		User userExpected = new User();
		userExpected.setId(5);
		userExpected.setLogin("Second");
		userExpected.setPassword("2");
		userExpected.setRole(UserType.DRIVER);

		User userFromJSON = User
				.fromJSON(new String("{\"id\": 5,\"login\": \"Second\",\"role\": \"DRIVER\", \"password\": \"2\"}"));
		assertEquals(userExpected.toString(), userFromJSON.toString());
		assertEquals(userExpected.getPassword(), userFromJSON.getPassword());
	}

	@Test
	public void JWTLifeCycleTest() {
		User user = new User();
		user.setId(1);
		user.setLogin("First");
		user.setPassword("pass");
		user.setRole(UserType.CLIENT);

		String jwtString = user.toJWT("secret");
		DecodedJWT decodedJWT = User.verifyJWT("secret", jwtString);
		User userFromJWT = User.fromJWT(decodedJWT);

		assertEquals(user.toString(), userFromJWT.toString());

		userFromJWT = User.fromJWT(jwtString);
		assertEquals(user.toString(), userFromJWT.toString());
	}

	@Test
	public void shouldWorkUserTypeCorrectly() {
		UserType role = UserType.findByValue("administrator");
		assertEquals(UserType.ADMIN, role);

		UserType role1 = UserType.findByValue("vasya");
		assertNull(role1);
	}

}
