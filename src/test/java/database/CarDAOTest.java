package database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import database.entity.car.Car;
import database.entity.car.CarStatus;
import database.entity.car.CarType;

public class CarDAOTest {
	private DBManager database;
	private Connection connection;
	private Statement initStatement;

	@Before
	public void init() throws SQLException {
		database = Mockito.mock(DBManager.class);
		connection = Mockito.mock(Connection.class);
		initStatement = Mockito.mock(Statement.class);

		when(database.getConnection()).thenReturn(connection);
		when(connection.createStatement()).thenReturn(initStatement);
		when(initStatement.executeUpdate(CarDAO.INIT_TABLE)).thenReturn(0);
	}

	@Test
	public void initTableOnGetInstanceTest() throws SQLException {
		CarDAO.getInstance(database);

		verify(database).getConnection();
		verify(connection).createStatement();
		verify(initStatement).executeUpdate(CarDAO.INIT_TABLE);
		verify(database).close(connection, initStatement);
	}

	@Test
	public void shouldCreateCarTest() throws SQLException {
		CarDAO dbm = CarDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarDAO.INSERT_CAR, Statement.RETURN_GENERATED_KEYS)).thenReturn(ps);
		when(ps.getGeneratedKeys()).thenReturn(rs);
		doNothing().when(ps).setString(1, "model");
		doNothing().when(ps).setString(2, "manufacturer");
		doNothing().when(ps).setString(3, "basic");
		doNothing().when(ps).setInt(4, 5);
		doNothing().when(ps).setString(5, "available");
		
		when(ps.executeUpdate()).thenReturn(1);
		when(rs.next()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(1);

		Car carExpected = new Car();
		carExpected.setId(1);
		carExpected.setManufacturer("manufacturer");
		carExpected.setModel("model");
		carExpected.setType(CarType.BASIC);
		carExpected.setSeats(5);
		carExpected.setStatus(CarStatus.AVAILABLE);

		Car carActual = dbm.createCar("manufacturer", "model", CarType.BASIC, 5, CarStatus.AVAILABLE);
		assertEquals(carExpected.toString(), carActual.toString());
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldThrowSQLExceptionWhenNoRowsModifiedCreateCarTest() throws SQLException {
		CarDAO dbm = CarDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarDAO.INSERT_CAR, Statement.RETURN_GENERATED_KEYS)).thenReturn(ps);
		when(ps.getGeneratedKeys()).thenReturn(rs);
		doNothing().when(ps).setString(1, "model");
		doNothing().when(ps).setString(2, "manufacturer");
		doNothing().when(ps).setString(3, "basic");
		doNothing().when(ps).setInt(4, 5);
		doNothing().when(ps).setString(5, "available");
		when(ps.executeUpdate()).thenReturn(0);

		SQLException thrown = assertThrows(SQLException.class, () -> dbm.createCar("manufacturer", "model", CarType.BASIC, 5, CarStatus.AVAILABLE));

		assertEquals("Couldn't insert car to database", thrown.getMessage());
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldThrowSQLExceptionWhenCannotGetIdModifiedCreateCarTest() throws SQLException {
		CarDAO dbm = CarDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarDAO.INSERT_CAR, Statement.RETURN_GENERATED_KEYS)).thenReturn(ps);
		when(ps.getGeneratedKeys()).thenReturn(rs);
		doNothing().when(ps).setString(1, "model");
		doNothing().when(ps).setString(2, "manufacturer");
		doNothing().when(ps).setString(3, "basic");
		doNothing().when(ps).setInt(4, 5);
		doNothing().when(ps).setString(5, "available");
		when(ps.executeUpdate()).thenReturn(1);
		when(rs.next()).thenReturn(false);

		SQLException thrown = assertThrows(SQLException.class, () -> dbm.createCar("manufacturer", "model", CarType.BASIC, 5, CarStatus.AVAILABLE));

		assertEquals("Couldn't insert car to database", thrown.getMessage());
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldGetAllCarsTest() throws SQLException {
		List<Car> listActual = new ArrayList<>();

		List<Car> listExpected = new ArrayList<>();
		Car car = new Car();
		car.setId(1);
		car.setManufacturer("manufacturer");
		car.setModel("model");
		car.setType(CarType.BASIC);
		car.setSeats(5);
		car.setStatus(CarStatus.AVAILABLE);
		listExpected.add(car);

		CarDAO dbm = CarDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.createStatement()).thenReturn(st);
		when(st.executeQuery(CarDAO.FIND_ALL_CARS)).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("manufacturer")).thenReturn("manufacturer");
		when(rs.getString("model")).thenReturn("model");
		when(rs.getString("type")).thenReturn("basic");
		when(rs.getInt("seats")).thenReturn(5);
		when(rs.getString("status")).thenReturn("available");

		listActual = dbm.getAllCars();

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, st, rs);
	}

	@Test
	public void shouldThrowIfNoConnectionWithDBAllCarsTest() throws SQLException {
		Statement st = Mockito.mock(Statement.class);
		when(connection.createStatement()).thenReturn(st);
		doThrow(new SQLException("Was unnable  to get users from db")).when(st).executeQuery(CarDAO.FIND_ALL_CARS);

		CarDAO dbm = CarDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.getAllCars());
		verify(database).close(connection, st);
	}

	@Test
	public void shouldGetCarByIdTest() throws SQLException {
		Car carExpected = new Car();
		carExpected.setId(1);
		carExpected.setManufacturer("manufacturer");
		carExpected.setModel("model");
		carExpected.setType(CarType.BASIC);
		carExpected.setSeats(5);
		carExpected.setStatus(CarStatus.AVAILABLE);

		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarDAO.FIND_CAR_BY_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);

		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("manufacturer")).thenReturn("manufacturer");
		when(rs.getString("model")).thenReturn("model");
		when(rs.getString("type")).thenReturn("basic");
		when(rs.getInt("seats")).thenReturn(5);
		when(rs.getString("status")).thenReturn("available");
		
		CarDAO dbm = CarDAO.getInstance(database);

		Car actualUser = dbm.getCar(1);

		assertEquals(carExpected.toString(), actualUser.toString());
		verify(database).close(connection, ps, rs);
	}

	@Test
	public void shouldThrowIfNoConnectionWithDBGetCarByIdTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarDAO.FIND_CAR_BY_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);

		doThrow(new SQLException("")).when(ps).executeQuery();

		CarDAO dbm = CarDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.getCar(1));
		verify(database).close(connection, ps, null);
	}

	@Test
	public void shouldDeleteCarTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(CarDAO.DELETE_CAR)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);

		CarDAO dbm = CarDAO.getInstance(database);

		assertTrue(dbm.deleteCar(0));
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldThrowIfNoConnetionWithDBDeleteCarTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(CarDAO.DELETE_CAR)).thenReturn(ps);

		doThrow(new SQLException("")).when(ps).executeUpdate();

		CarDAO dbm = CarDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.deleteCar(0));
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldUpdateCarTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		Car car = new Car();
		car.setId(1);
		car.setManufacturer("manufacturer");
		car.setModel("model");
		car.setSeats(5);
		car.setStatus(CarStatus.AVAILABLE);
		car.setType(CarType.BASIC);
		
		when(connection.prepareStatement(CarDAO.UPDATE_CAR)).thenReturn(ps);

		doNothing().when(ps).setString(1, "model");
		doNothing().when(ps).setString(2, "manufacturer");
		doNothing().when(ps).setString(3, "basic");
		doNothing().when(ps).setInt(4, 5);
		doNothing().when(ps).setString(5, "available");
		when(ps.executeUpdate()).thenReturn(1);

		CarDAO dbm = CarDAO.getInstance(database);

		dbm.updateCar(car);

		verify(database).close(connection, ps);
	}

	@Test
	public void shouldSetNullIfNoTypeUpdateCarTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		
		Car car = new Car();
		car.setId(1);
		car.setManufacturer("manufacturer");
		car.setModel("model");
		car.setSeats(5);
		car.setStatus(CarStatus.AVAILABLE);

		when(connection.prepareStatement(CarDAO.UPDATE_CAR)).thenReturn(ps);

		doNothing().when(ps).setString(1, car.getModel());
		doNothing().when(ps).setString(2, car.getManufacturer());
		doNothing().when(ps).setString(3, null);
		doNothing().when(ps).setInt(4, car.getSeats());
		doNothing().when(ps).setString(5, car.getStatus().getValue());
		when(ps.executeUpdate()).thenReturn(1);

		CarDAO dbm = CarDAO.getInstance(database);

		dbm.updateCar(car);

		verify(database).close(connection, ps);
	}
	
	@Test
	public void shouldSetNullIfNoStatusUpdateCarTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		
		Car car = new Car();
		car.setId(1);
		car.setManufacturer("manufacturer");
		car.setModel("model");
		car.setSeats(5);
		car.setType(CarType.BASIC);
		

		when(connection.prepareStatement(CarDAO.UPDATE_CAR)).thenReturn(ps);

		doNothing().when(ps).setString(1, car.getModel());
		doNothing().when(ps).setString(2, car.getManufacturer());
		doNothing().when(ps).setString(3, car.getType().getValue());
		doNothing().when(ps).setInt(4, car.getSeats());
		doNothing().when(ps).setString(5, null);
		when(ps.executeUpdate()).thenReturn(1);

		CarDAO dbm = CarDAO.getInstance(database);

		dbm.updateCar(car);

		verify(database).close(connection, ps);
	}
	
	@Test
	public void shouldSetNullIfNoSeatsUpdateCarTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		Car car = new Car();
		car.setId(1);
		car.setManufacturer("manufacturer");
		car.setModel("model");
		car.setStatus(CarStatus.AVAILABLE);
		car.setType(CarType.BASIC);
		
		when(connection.prepareStatement(CarDAO.UPDATE_CAR)).thenReturn(ps);

		doNothing().when(ps).setString(1, "model");
		doNothing().when(ps).setString(2, "manufacturer");
		doNothing().when(ps).setString(3, "basic");
		doNothing().when(ps).setInt(4, 5);
		doNothing().when(ps).setString(5, "available");
		when(ps.executeUpdate()).thenReturn(1);

		CarDAO dbm = CarDAO.getInstance(database);

		dbm.updateCar(car);

		verify(database).close(connection, ps);
	}

	@Test
	public void shouldThrowIfNoConnectionWithDBUpdateCarTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(CarDAO.UPDATE_CAR)).thenReturn(ps);

		Car testCar = new Car();

		doNothing().when(ps).setString(1, "model");
		doNothing().when(ps).setString(2, "manufacturer");
		doNothing().when(ps).setString(3, "basic");
		doNothing().when(ps).setInt(4, 5);
		doNothing().when(ps).setString(5, "available");
		doNothing().when(ps).setInt(6, 1);
		doThrow(new SQLException("")).when(ps).executeUpdate();

		CarDAO dbm = CarDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.updateCar(testCar));
		verify(database).close(connection, ps);
	}
	
	@Test
	public void shouldFindAppropriateCarTest() throws SQLException {
		List<Car> listActual = new ArrayList<>();

		List<Car> listExpected = new ArrayList<>();
		Car car = new Car();
		car.setId(1);
		car.setManufacturer("manufacturer");
		car.setModel("model");
		car.setType(CarType.BASIC);
		car.setSeats(5);
		car.setStatus(CarStatus.AVAILABLE);
		listExpected.add(car);
		
		int seats = 5;
		CarType type = CarType.BASIC;

		CarDAO dbm = CarDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(CarDAO.FIND_APPROPRIATE_CAR)).thenReturn(ps);
		doNothing().when(ps).setInt(1, seats);
		doNothing().when(ps).setString(2, type.getValue());
		when(ps.executeQuery()).thenReturn(rs);
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		
		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("manufacturer")).thenReturn("manufacturer");
		when(rs.getString("model")).thenReturn("model");
		when(rs.getString("type")).thenReturn("basic");
		when(rs.getInt("seats")).thenReturn(5);
		when(rs.getString("status")).thenReturn("available");
		
		listActual = dbm.findAppropriateCar(seats, type);

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, ps, rs);
	}
	
	@Test
	public void shouldThrowIfNoConnectionWithDBFindAppropriateCarsTest() throws SQLException {

		CarDAO dbm = CarDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(CarDAO.FIND_APPROPRIATE_CAR)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 3);
		doNothing().when(ps).setString(2, CarType.BASIC.getValue());
		
		doThrow(new SQLException("")).when(ps).executeQuery();
		
		assertThrows(SQLException.class, () -> dbm.findAppropriateCar(1, CarType.BASIC));
	}
	
	@Test
	public void shouldGetCountOfCarsTest() throws SQLException {
		CarDAO dbm = CarDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);
		
		when(connection.createStatement()).thenReturn(st);
		when(st.executeQuery(CarDAO.COUNT_CARS)).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(1);
		assertEquals(new Integer(1), dbm.getCountCars());
		verify(database).close(connection, st, rs);
	}
	
	@Test
	public void shouldThrowIfNoConnectionCountOfCarsTest() throws SQLException {
		CarDAO dbm = CarDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);
		
		when(connection.createStatement()).thenReturn(st);
		doThrow(new SQLException("")).when(st).executeQuery(CarDAO.COUNT_CARS);
		assertThrows(SQLException.class, () -> dbm.getCountCars());
	}
	
	@Test
	public void shouldGetCountOfCarsWithoutDriverTest() throws SQLException {
		CarDAO dbm = CarDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);
		
		when(connection.createStatement()).thenReturn(st);
		when(st.executeQuery(CarDAO.COUNT_CARS + "WHERE `status` = 'without driver'")).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(1);
		assertEquals(new Integer(1), dbm.getCountCarsWithoutDriver());
		verify(database).close(connection, st, rs);
	}
	
	@Test
	public void shouldThrowIfNoConnectionCountOfCarsWithoutDriverTest() throws SQLException {
		CarDAO dbm = CarDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);
		
		when(connection.createStatement()).thenReturn(st);
		doThrow(new SQLException("")).when(st).executeQuery(CarDAO.COUNT_CARS + "WHERE `status` = 'without driver'");
		assertThrows(SQLException.class, () -> dbm.getCountCarsWithoutDriver());
	}
	
	@Test
	public void shouldGetAllCarsWithPaginationTest() throws SQLException {
		List<Car> listActual = new ArrayList<>();

		List<Car> listExpected = new ArrayList<>();
		Car car = new Car();
		car.setId(1);
		car.setManufacturer("manufacturer");
		car.setModel("model");
		car.setType(CarType.BASIC);
		car.setSeats(5);
		car.setStatus(CarStatus.AVAILABLE);
		listExpected.add(car);

		CarDAO dbm = CarDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);
		when(connection.prepareStatement(CarDAO.FIND_ALL_CARS + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(2, 2);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		
		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("manufacturer")).thenReturn("manufacturer");
		when(rs.getString("model")).thenReturn("model");
		when(rs.getString("type")).thenReturn("basic");
		when(rs.getInt("seats")).thenReturn(5);
		when(rs.getString("status")).thenReturn("available");
		
		listActual = dbm.getAllCars(1,1);

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, null, rs);
		verify(database).close(ps);
	}
	
	@Test
	public void shouldGetAllCarsForDriverWithPaginationTest() throws SQLException {
		List<Car> listActual = new ArrayList<>();

		List<Car> listExpected = new ArrayList<>();
		Car car = new Car();
		car.setId(1);
		car.setManufacturer("manufacturer");
		car.setModel("model");
		car.setType(CarType.BASIC);
		car.setSeats(5);
		car.setStatus(CarStatus.AVAILABLE);
		listExpected.add(car);

		CarDAO dbm = CarDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);
		when(connection.prepareStatement(CarDAO.FIND_CARS_FOR_DRIVER)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(2, 2);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		
		when(rs.getInt("id")).thenReturn(1);
		when(rs.getString("manufacturer")).thenReturn("manufacturer");
		when(rs.getString("model")).thenReturn("model");
		when(rs.getString("type")).thenReturn("basic");
		when(rs.getInt("seats")).thenReturn(5);
		when(rs.getString("status")).thenReturn("available");
		
		listActual = dbm.getAllCarsForDriver(1,1);

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, null, rs);
		verify(database).close(ps);
	}
	
	
	@Test
	public void shouldThrowIfNoConnectionGetAllCarsForDriverTest() throws SQLException {
		Statement st = Mockito.mock(Statement.class);
		when(connection.createStatement()).thenReturn(st);
		doThrow(new SQLException("Was unnable  to get users from db")).when(st).executeQuery(CarDAO.FIND_CARS_FOR_DRIVER);

		CarDAO dbm = CarDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.getAllCarsForDriver(null, null));
		verify(database).close(connection, st);
	}
}