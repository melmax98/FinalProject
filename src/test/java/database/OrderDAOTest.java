package database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import database.bean.CarUsage;
import database.entity.car.CarType;
import database.entity.order.Order;
import database.entity.order.OrderStatus;

public class OrderDAOTest {
	private DBManager database;
	private Connection connection;
	private Statement initStatement;

	@Before
	public void init() throws SQLException {
		database = Mockito.mock(DBManager.class);
		connection = Mockito.mock(Connection.class);
		initStatement = Mockito.mock(Statement.class);

		when(database.getConnection()).thenReturn(connection);
		when(connection.createStatement()).thenReturn(initStatement);
		when(initStatement.executeUpdate(OrderDAO.INIT_TABLE)).thenReturn(0);
	}

	@Test
	public void initTableOnGetInstanceTest() throws SQLException {
		OrderDAO.getInstance(database);

		verify(database).getConnection();
		verify(connection).createStatement();
		verify(initStatement).executeUpdate(OrderDAO.INIT_TABLE);
		// Verify that all db enteties are closed
		verify(database).close(connection, initStatement);
	}

	@Test
	public void shouldCreateOrderTest() throws SQLException {
		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.INSERT_ORDER, Statement.RETURN_GENERATED_KEYS)).thenReturn(ps);
		when(ps.getGeneratedKeys()).thenReturn(rs);

		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(2, 2);
		doNothing().when(ps).setString(3, "");
		doNothing().when(ps).setString(4, "");
		doNothing().when(ps).setString(5, "");
		doNothing().when(ps).setInt(6, 6);
		doNothing().when(ps).setInt(7, 7);
		doNothing().when(ps).setInt(8, 8);

		when(ps.executeUpdate()).thenReturn(1);
		when(rs.next()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(1);

		Order order = dbm.createOrder(1, 2, new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }),
				new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }), CarType.BASIC, 6, 100, 8);
		assertEquals(order.getTo().toString(),
				new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }).toString());
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldThrowSQLExceptionWhenNoRowsModifiedCreateOrderTest() throws SQLException {
		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.INSERT_ORDER, Statement.RETURN_GENERATED_KEYS)).thenReturn(ps);
		when(ps.getGeneratedKeys()).thenReturn(rs);

		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(2, 2);
		doNothing().when(ps).setString(3, "");
		doNothing().when(ps).setString(4, "");
		doNothing().when(ps).setString(5, "");
		doNothing().when(ps).setInt(6, 6);
		doNothing().when(ps).setInt(7, 7);
		doNothing().when(ps).setInt(8, 8);

		when(ps.executeUpdate()).thenReturn(0);

		SQLException thrown = assertThrows(SQLException.class,
				() -> dbm.createOrder(1, 2, new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }),
						new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }), CarType.BASIC, 6, 100,
						8));

		assertEquals("Couldn't insert order to database", thrown.getMessage());
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldThrowSQLExceptionWhenCannotGetIdModifiedCreateOrderTest() throws SQLException {
		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.INSERT_ORDER, Statement.RETURN_GENERATED_KEYS)).thenReturn(ps);
		when(ps.getGeneratedKeys()).thenReturn(rs);

		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(2, 2);
		doNothing().when(ps).setString(3, "");
		doNothing().when(ps).setString(4, "");
		doNothing().when(ps).setString(5, "");
		doNothing().when(ps).setInt(6, 6);
		doNothing().when(ps).setInt(7, 7);
		doNothing().when(ps).setInt(8, 8);

		when(ps.executeUpdate()).thenReturn(1);
		when(rs.next()).thenReturn(false);

		SQLException thrown = assertThrows(SQLException.class,
				() -> dbm.createOrder(1, 2, new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }),
						new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }), CarType.BASIC, 6, 100,
						8));

		assertEquals("Couldn't insert order to database", thrown.getMessage());
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldGetAllOrdersTest() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.createStatement()).thenReturn(st);
		when(st.executeQuery(OrderDAO.FIND_ALL_ORDERS)).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getAllOrders();

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, st, rs);
	}
	
	@Test
	public void shouldGetAllOrdersWithPaginationAndSort1Test() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ALL_ORDERS + " ORDER BY `price`" + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getAllOrders(1, 1, 1);

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, null, rs);
		verify(database).close(ps);
	}
	
	@Test
	public void shouldGetAllOrdersWithPaginationAndSort2Test() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ALL_ORDERS + " ORDER BY `price` DESC" + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getAllOrders(1, 1, 2);

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, null, rs);
		verify(database).close(ps);
	}
	
	@Test
	public void shouldGetAllOrdersWithPaginationAndSort3Test() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ALL_ORDERS + " ORDER BY `created_at`" + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getAllOrders(1, 1, 3);

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, null, rs);
		verify(database).close(ps);
	}
	
	@Test
	public void shouldGetAllOrdersWithPaginationAndSort4Test() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ALL_ORDERS + " ORDER BY `created_at` DESC" + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getAllOrders(1, 1, 4);

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, null, rs);
		verify(database).close(ps);
	}

	@Test
	public void shouldThrowIfNoConnectionWithDBAllOrdersTest() throws SQLException {
		Statement st = Mockito.mock(Statement.class);
		when(connection.createStatement()).thenReturn(st);
		doThrow(new SQLException("Was unnable  to get orders from db")).when(st).executeQuery(OrderDAO.FIND_ALL_ORDERS);

		OrderDAO dbm = OrderDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.getAllOrders());
		verify(database).close(connection, st);
	}

	@Test
	public void shouldGetOrderByIdTest() throws SQLException {
		Order orderExpected = new Order();
		orderExpected.setCarId(1);
		orderExpected.setCarType(CarType.BASIC);
		orderExpected.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		orderExpected.setDriverId(2);
		orderExpected.setFinishedAt(null);
		orderExpected.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		orderExpected.setId(3);
		orderExpected.setPassengers(4);
		orderExpected.setPrice(5);
		orderExpected.setStatus(OrderStatus.CREATED);
		orderExpected.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		orderExpected.setUserId(6);
		orderExpected.setWaitTime(7);

		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ORDER_BY_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);

		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn(null);
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		OrderDAO dbm = OrderDAO.getInstance(database);

		Order actualOrder = dbm.getOrder(1);

		assertEquals(orderExpected.toString(), actualOrder.toString());
		verify(database).close(connection, ps, rs);
	}

	@Test
	public void shouldThrowIfNoConnectionWithDBGetCarByIdTest() throws SQLException {
		Order orderExpected = new Order();
		orderExpected.setCarId(1);
		orderExpected.setCarType(CarType.BASIC);
		orderExpected.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		orderExpected.setDriverId(2);
		orderExpected.setFinishedAt(null);
		orderExpected.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		orderExpected.setId(3);
		orderExpected.setPassengers(4);
		orderExpected.setPrice(5);
		orderExpected.setStatus(OrderStatus.CREATED);
		orderExpected.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		orderExpected.setUserId(6);
		orderExpected.setWaitTime(7);

		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(OrderDAO.FIND_ORDER_BY_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);

		doThrow(new SQLException("")).when(ps).executeQuery();

		OrderDAO dbm = OrderDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.getOrder((1)));
		verify(database).close(connection, ps, null);
	}

	@Test
	public void shouldGetOrderByUserIdTest() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ORDERS_BY_USER_ID)).thenReturn(ps);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getOrdersByUserId(6);

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, ps, rs);
	}

	@Test
	public void shouldThrowWhenNoConnectionWithDBFindOrderbyUserIdTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(OrderDAO.FIND_ORDERS_BY_USER_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);

		doThrow(new SQLException("")).when(ps).executeQuery();

		OrderDAO dbm = OrderDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.getOrdersByUserId(6));
		verify(database).close(connection, ps, null);
	}

	@Test
	public void shouldDeleteOrderTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(OrderDAO.DELETE_ORDER)).thenReturn(ps);

		OrderDAO dbm = OrderDAO.getInstance(database);

		assertTrue(dbm.deleteOrder(0));
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldUpdateOrderTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(null);
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.DONE);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);

		when(connection.prepareStatement(OrderDAO.UPDATE_ORDER)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(2, 2);
		doNothing().when(ps).setInt(3, 3);
		doNothing().when(ps).setString(4, "");
		doNothing().when(ps).setString(5, "");
		doNothing().when(ps).setString(6, "");
		doNothing().when(ps).setString(7, "");
		doNothing().when(ps).setInt(8, 8);
		doNothing().when(ps).setInt(9, 9);
		doNothing().when(ps).setInt(10, 10);
		doNothing().when(ps).setString(11, "");
		when(ps.executeUpdate()).thenReturn(1);
		
		OrderDAO dbm = OrderDAO.getInstance(database);
		dbm.updateOrder(order);

		verify(database).close(connection, ps);
	}

	@Test
	public void shouldThrowIfNoConnetionWithDBDeleteOrderTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(OrderDAO.DELETE_ORDER)).thenReturn(ps);

		doThrow(new SQLException("")).when(ps).executeUpdate();

		OrderDAO dbm = OrderDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.deleteOrder(0));
		verify(database).close(connection, ps);
	}

	@Test
	public void shouldNotUpdateOrderIfNullTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(OrderDAO.UPDATE_ORDER)).thenReturn(ps);

		when(ps.executeUpdate()).thenReturn(1);

		OrderDAO dbm = OrderDAO.getInstance(database);

		dbm.updateOrder(new Order());

		verify(database).close(connection, ps);
	}

	@Test
	public void shouldThrowIfNoConnectionWithDBUpdateOrderTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(OrderDAO.UPDATE_ORDER)).thenReturn(ps);

		doThrow(new SQLException("")).when(ps).executeUpdate();

		OrderDAO dbm = OrderDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.updateOrder(new Order()));
		verify(database).close(connection, ps);
	}
	
	@Test
	public void shouldGetSpentMoneyTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);
		
		when(connection.prepareStatement(OrderDAO.GET_SPENT_MONEY_BY_USER_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(1);
		OrderDAO dbm = OrderDAO.getInstance(database);
		assertEquals(1, dbm.getSpentMoney(1));
		verify(database).close(connection, ps, rs);
	}
	
	@Test
	public void shouldThrowIfNoConnectionWithDBGetSpentMoneyTest() throws SQLException {
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);

		when(connection.prepareStatement(OrderDAO.GET_SPENT_MONEY_BY_USER_ID)).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doThrow(new SQLException("")).when(ps).executeQuery();

		OrderDAO dbm = OrderDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.getSpentMoney(1));
		verify(database).close(connection, ps, null);
	}
	
	@Test
	public void shouldGetOrdersFilterByDatePaginationAndSort4Test() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ALL_ORDERS_DATE + " ORDER BY `created_at` DESC" + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getOrdersFilterByDate(1, 1, 4, "2020-10-01 00:00:00", "2020-10-01 00:00:00");

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, null, rs);
		verify(database).close(ps);
	}
	
	@Test
	public void shouldGetOrdersFilterByDatePaginationAndSort3Test() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ALL_ORDERS_DATE + " ORDER BY `created_at`" + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getOrdersFilterByDate(1, 1, 3, "2020-10-01 00:00:00", "2020-10-01 00:00:00");

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, null, rs);
		verify(database).close(ps);
	}
	
	@Test
	public void shouldGetOrdersFilterByDatePaginationAndSort2Test() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ALL_ORDERS_DATE + " ORDER BY `price` DESC" + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getOrdersFilterByDate(1, 1, 2, "2020-10-01 00:00:00", "2020-10-01 00:00:00");

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, null, rs);
		verify(database).close(ps);
	}
	
	@Test
	public void shouldGetOrdersFilterByDatePaginationAndSort1Test() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ALL_ORDERS_DATE + " ORDER BY `price`" + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getOrdersFilterByDate(1, 1, 1, "2020-10-01 00:00:00", "2020-10-01 00:00:00");

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, null, rs);
		verify(database).close(ps);
	}
	
	@Test
	public void shouldGetOrdersFilterByDate() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		Statement st = Mockito.mock(Statement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);
		
		when(connection.createStatement()).thenReturn(st);
		when(st.executeQuery(OrderDAO.FIND_ALL_ORDERS)).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getOrdersFilterByDate(null, null, 1, "2020-10-01 00:00:00", "2020-10-01 00:00:00");

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, st, rs);
	}
	
	@Test
	public void shouldThrowIfNoConnectionGetOrdersFilterByDate() throws SQLException {
		Statement st = Mockito.mock(Statement.class);

		when(connection.createStatement()).thenReturn(st);
		doThrow(new SQLException("")).when(st).executeQuery(OrderDAO.FIND_ALL_ORDERS);

		OrderDAO dbm = OrderDAO.getInstance(database);

		assertThrows(SQLException.class, () -> dbm.getOrdersFilterByDate(null, null, 1, "2020-10-01 00:00:00", "2020-10-01 00:00:00"));
		verify(database).close(connection, st, null);
	}
	
	@Test
	public void shouldGetCountOfOrdersTest() throws SQLException {
		Statement st = Mockito.mock(Statement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);
		
		when(connection.createStatement()).thenReturn(st);
		when(st.executeQuery(OrderDAO.COUNT_ORDERS)).thenReturn(rs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(1);
		OrderDAO dbm = OrderDAO.getInstance(database);
		assertEquals(new Integer(1), dbm.getCountOrders());
		verify(database).close(connection, st, rs);
	}
	
	@Test
	public void shouldThrowIfNoConnectionGetCountOfOrdersTest() throws SQLException {
		Statement st = Mockito.mock(Statement.class);
		OrderDAO dbm = OrderDAO.getInstance(database);
		
		when(connection.createStatement()).thenReturn(st);
		doThrow(new SQLException("")).when(st).executeQuery(OrderDAO.COUNT_ORDERS);
		assertThrows(SQLException.class, () -> dbm.getCountOrders());
		verify(database).close(connection, st, null);
	}
	
	@Test
	public void shouldGetOrdersFilterByUserIdePaginationAndSort1Test() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ORDERS_BY_USER_ID + " ORDER BY `price`" + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getOrdersByUserId(1, 1, 1 ,1);

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, ps, rs);
	}
	
	@Test
	public void shouldGetOrdersFilterByUserIdePaginationAndSort2Test() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ORDERS_BY_USER_ID + " ORDER BY `price` DESC" + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getOrdersByUserId(1, 1, 1 , 2);

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, ps, rs);
	}
	
	@Test
	public void shouldGetOrdersFilterByUserIdePaginationAndSort3Test() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ORDERS_BY_USER_ID + " ORDER BY `created_at`" + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getOrdersByUserId(1, 1, 1 , 3);

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, ps, rs);
	}
	
	@Test
	public void shouldGetOrdersFilterByUserIdePaginationAndSort4Test() throws SQLException {
		List<Order> listActual = new ArrayList<>();

		List<Order> listExpected = new ArrayList<>();
		Order order = new Order();
		order.setCarId(1);
		order.setCarType(CarType.BASIC);
		order.setCreatedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setDriverId(2);
		order.setFinishedAt(LocalDateTime.parse("2020-10-01 00:00:00", CarUsage.formatter));
		order.setFrom(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setId(3);
		order.setPassengers(4);
		order.setPrice(5);
		order.setStatus(OrderStatus.CREATED);
		order.setTo(new JSONArray(new Double[] { 49.22812192199157, 28.414292335510254 }));
		order.setUserId(6);
		order.setWaitTime(7);
		listExpected.add(order);

		OrderDAO dbm = OrderDAO.getInstance(database);
		PreparedStatement ps = Mockito.mock(PreparedStatement.class);
		ResultSet rs = Mockito.mock(ResultSet.class);

		when(connection.prepareStatement(OrderDAO.FIND_ORDERS_BY_USER_ID + " ORDER BY `created_at` DESC" + " LIMIT ?, ?")).thenReturn(ps);
		doNothing().when(ps).setInt(1, 1);
		doNothing().when(ps).setInt(1, 1);
		when(ps.executeQuery()).thenReturn(rs);
		
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt("id")).thenReturn(3);
		when(rs.getInt("car_id")).thenReturn(1);
		when(rs.getInt("driver_id")).thenReturn(2);
		when(rs.getString("from")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("to")).thenReturn("POINT(28.414292335510254 49.22812192199157)");
		when(rs.getString("status")).thenReturn("created");
		when(rs.getString("car_type")).thenReturn("basic");
		when(rs.getString("created_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getString("finished_at")).thenReturn("2020-10-01 00:00:00");
		when(rs.getInt("price")).thenReturn(5);
		when(rs.getInt("passengers")).thenReturn(4);
		when(rs.getInt("wait_time")).thenReturn(7);
		when(rs.getInt("user_id")).thenReturn(6);

		listActual = dbm.getOrdersByUserId(1, 1, 1, 4);

		assertEquals(listExpected.toString(), listActual.toString());
		verify(database).close(connection, ps, rs);
	}
}