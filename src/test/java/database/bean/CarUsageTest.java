package database.bean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.LocalDateTime;

import org.json.JSONObject;
import org.junit.Test;

public class CarUsageTest {

	@Test
	public void shouldCreateJSONObjectTest() {
		CarUsage carUsage = new CarUsage();

		carUsage.setId(1);
		carUsage.setStatus(UsageStatus.AVAILABLE);
		carUsage.setDriverId(2);
		carUsage.setCarId(3);
		carUsage.setAt(LocalDateTime.parse("2020-09-30 00:00:00", CarUsage.formatter));

		assertEquals(new JSONObject(
				"{\"id\": 1,\"status\": \"AVAILABLE\",\"driver_id\": 2,\"car_id\": 3,\"at\": \"2020-09-30 00:00:00\"}")
						.toString(),
				carUsage.toString());
	}

	@Test
	public void shouldCreateCarObjectFromJSONObject() {
		CarUsage carUsageExpected = new CarUsage();

		carUsageExpected.setId(1);
		carUsageExpected.setStatus(UsageStatus.AVAILABLE);
		carUsageExpected.setDriverId(2);
		carUsageExpected.setCarId(3);
		carUsageExpected.setAt(LocalDateTime.parse("2020-09-30 00:00:00", CarUsage.formatter));

		JSONObject json = new JSONObject(
				"{\"id\": 1,\"status\": \"AVAILABLE\",\"driver_id\": 2,\"car_id\": 3,\"at\": \"2020-09-30 00:00:00\"}");

		CarUsage carUsageActual = CarUsage.fromJSON(json);

		assertEquals(carUsageExpected.toString(), carUsageActual.toString());
	}

	@Test
	public void shouldCreateCarUsageObjectFromJSONString() {
		CarUsage carUsageExpected = new CarUsage();

		carUsageExpected.setId(1);
		carUsageExpected.setStatus(UsageStatus.AVAILABLE);
		carUsageExpected.setDriverId(2);
		carUsageExpected.setCarId(3);
		carUsageExpected.setAt(LocalDateTime.parse("2020-09-30 00:00:00", CarUsage.formatter));

		String json = new String(
				"{\"id\": 1,\"status\": \"AVAILABLE\",\"driver_id\": 2,\"car_id\": 3,\"at\": \"2020-09-30 00:00:00\"}");

		CarUsage carUsageActual = CarUsage.fromJSON(json);

		assertEquals(carUsageExpected.toString(), carUsageActual.toString());
	}

	@Test
	public void shouldGetUsageStatusValueTest() {
		assertEquals("available", UsageStatus.AVAILABLE.getValue());
	}

	@Test
	public void shouldFindByValueUsageStatusTest() {
		assertEquals(UsageStatus.AVAILABLE, UsageStatus.findByValue("available"));
	}

	@Test
	public void shouldReturnNullIfNoSuchValueFindByValueUsageStatusTest() {
		assertNull(UsageStatus.findByValue("nothing"));
	}
}
