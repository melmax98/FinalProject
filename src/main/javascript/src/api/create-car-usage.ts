import queryString from "query-string";

export const CreateCarUsage = (alertSelector: string, data: any) => {
    $.ajax({
    url: "/api/car-usage",
    type: "PUT",
    data,
  })
    .done(async() => {
      const language = queryString.parse(window.location.search).language || 'en';
      $(alertSelector)
        .prepend(await $.get(`/alerts/car-usage.jsp?language=${language}`))
        .alert();
    })
    .fail(async (xhr) => {
      const language = queryString.parse(window.location.search).language || 'en';
      if (xhr.status >= 400) {
        $(alertSelector)
          .prepend(await $.get(`/alerts/car-usage-fail.jsp?language=${language}`)
          )
          .alert();
      }
    });
}