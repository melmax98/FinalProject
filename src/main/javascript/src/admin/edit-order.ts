import queryString from "query-string";

$(".change-order form").on("submit", function (e) {
  e.preventDefault();

  $.ajax({
    url: "/api/order/" + queryString.parse(window.location.search).id,
    type: "PUT",
    data: JSON.stringify($(this).serializeObject()),
  })
    .done(async(data) => {
      const language = queryString.parse(window.location.search).language || 'en';
      $(".change-order")
        .prepend(await $.get(`/alerts/edit-order.jsp?language=${language}`))
        .alert();
    })
    .fail(async (xhr) => {
       const language = queryString.parse(window.location.search).language || 'en';
      if (xhr.status >= 400) {
        $(".change-order")
          .prepend(await $.get(`/alerts/edit-order-fail.jsp?language=${language}`))
          .alert();
      }
    });
});
