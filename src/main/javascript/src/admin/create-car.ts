import queryString from "query-string";

$(".create-car form").on("submit", function (e) {
  e.preventDefault();
  $.ajax({
    url: "/api/car",
    type: "PUT",
    data: JSON.stringify($(this).serializeObject()),
  })
    .done(async(data) => {
      const language = queryString.parse(window.location.search).language || 'en';
      $(".create-car")
        .prepend(await $.get(`/alerts/create-car.jsp?language=${language}`))
        .alert();
      window.location.href = "/admin/index.jsp";
    })
    .fail(async (xhr) => {
      const language = queryString.parse(window.location.search).language || 'en';
      if (xhr.status >= 400) {
        $(".create-car")
          .prepend(await $.get(`/alerts/create-car-fail.jsp?language=${language}`))
          .alert();
      }
    });
});
