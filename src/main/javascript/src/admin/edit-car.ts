import queryString from "query-string";

$(".change-car form").on("submit", function (e) {
  e.preventDefault();

  $.ajax({
    url: "/api/car/" + queryString.parse(window.location.search).id,
    type: "PUT",
    data: JSON.stringify($(this).serializeObject()),
  })
    .done(async (data) => {
      const language = queryString.parse(window.location.search).language || 'en';
      $(".change-car")
        .prepend(await $.get(`/alerts/edit-car.jsp?language=${language}`))
        .alert();
    })
    .fail(async (xhr) => {
      const language = queryString.parse(window.location.search).language || 'en';
      if (xhr.status >= 400) {
        $(".change-car")
          .prepend(await $.get(`/alerts/edit-car-fail.jsp?language=${language}`))
          .alert();
      }
    });
});

$(".delete-car form").on("submit", function (e) {
  e.preventDefault();

  $.ajax({
    url: "/api/car/" + queryString.parse(window.location.search).id,
    type: "DELETE",
    data: JSON.stringify($(this).serializeObject()),
  })
    .done(async (data) => {
      const language = queryString.parse(window.location.search).language || 'en';
      $(".change-car")
        .prepend(await $.get(`/alerts/delete-car.jsp?language=${language}`))
        .alert();
    })
    .fail(async (xhr) => {
      const language = queryString.parse(window.location.search).language || 'en';
      if (xhr.status >= 400) {
        $(".change-car")
          .prepend(await $.get(`/alerts/delete-car-fail.jsp?language=${language}`))
          .alert();
      }
    });
});