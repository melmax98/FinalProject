import { CreateCarUsage } from "../api/create-car-usage";

$(".create-car-usage form").on("submit", function (e) {
  e.preventDefault();
  
  CreateCarUsage(".create-car-usage", JSON.stringify($(this).serializeObject()))
});
