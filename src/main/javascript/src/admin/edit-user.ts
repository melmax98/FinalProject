import queryString from "query-string";

$(".change-user form").on("submit", function (e) {
  e.preventDefault();

  $.ajax({
    url: "/api/user/" + queryString.parse(window.location.search).id,
    type: "PUT",
    data: JSON.stringify($(this).serializeObject()),
  })
    .done(async (data) => {
      const language = queryString.parse(window.location.search).language || 'en';
      $(".change-user")
        .prepend(await $.get(`/alerts/edit-user.jsp?language=${language}`))
        .alert();
    })
    .fail(async (xhr) => {
      const language = queryString.parse(window.location.search).language || 'en';
      if (xhr.status >= 400) {
        $(".change-user")
          .prepend(await $.get(`/alerts/edit-user-fail.jsp?language=${language}`))
          .alert();
      }
    });
});

$(".delete-user form").on("submit", function (e) {
  e.preventDefault();

  $.ajax({
    url: "/api/user/" + queryString.parse(window.location.search).id,
    type: "DELETE",
    data: JSON.stringify($(this).serializeObject()),
  })
    .done(async (data) => {
      const language = queryString.parse(window.location.search).language || 'en';
      $(".change-user")
        .prepend(await $.get(`/alerts/delete-user.jsp?language=${language}`))
        .alert();
    })
    .fail(async (xhr) => {
      const language = queryString.parse(window.location.search).language || 'en';
      if (xhr.status >= 400) {
        $(".change-user")
          .prepend(await $.get(`/alerts/delete-user-fail.jsp?language=${language}`))
          .alert();
      }
    });
});