import { CreateCarUsage } from "../api/create-car-usage";
import { getCurrentUser } from "../utils/get-current-user";

$(".take-car form button").on("click", function (e) {
  e.preventDefault();

  const user = getCurrentUser();

  const status = $(this).hasClass('take-car-button') ? 'AVAILABLE' : 'WITHOUT_DRIVER';

  if (user) {
    CreateCarUsage(".take-car", JSON.stringify({ ...$(this).parent().parent("form").serializeObject(), driver_id: user.id, status }))
  }
});
