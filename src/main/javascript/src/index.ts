import "./style";
import "bootstrap/js/dist/alert";
import "bootstrap/js/dist/dropdown";
import "bootstrap/js/dist/button";
import "bootstrap/js/dist/collapse";
import "bootstrap/js/dist/tooltip";
import jwtDecode from "jwt-decode";
import Cookies from 'js-cookie';
import "./admin/create-car-usage";
import "./admin/create-car";
import "./admin/edit-car";
import "./admin/edit-order";
import "./admin/edit-user";
import './driver/create-car-usage';
import queryString from "query-string";

declare global {
  interface JQuery<TElement = HTMLElement> {
    serializeObject(): any;
  }
}


$(".from-td, .to-td").tooltip();

$('a[data-append-query]').on('click', function (e) {
e.preventDefault();

const data = queryString.parse(window.location.search);

const urlParts = $(this).attr('href').split('?');

window.location.href = urlParts[0] + '?' + queryString.stringify({ ...data, ...queryString.parse(urlParts[1])});

return false;
});

$.fn.serializeObject = function () {
  var o: any = {};
  var a = this.serializeArray();
  $.each(a, function () {
    if (o[this.name]) {
      if (!o[this.name].push) {
        o[this.name] = [o[this.name]];
      }
      o[this.name].push(this.value || "");
    } else {
      o[this.name] = this.value || "";
    }
  });
  return o;
};

$(".login-jumbotron form").on("submit", function (e) {
  e.preventDefault();

  $.post("/api/user/login", JSON.stringify($(this).serializeObject()))
    .done((data) => {
      Cookies.set('token', data.token);

      const tokenData: {
        role: string;
      } = jwtDecode(data.token);

      if (tokenData.role === "ADMIN") {
        window.location.href = "/admin/index";
      } else if (tokenData.role === "DRIVER") {
        window.location.href = "/driver/panel";
      } else {
        window.location.href = "/client/index.jsp";
      }
    })
    .fail(async (xhr) => {
      const language = queryString.parse(window.location.search).language || 'en';
      if (xhr.status >= 500) {
        $(".login-jumbotron")
          .prepend(await $.get(`/alerts/server.jsp?language=${language}`))
          .alert();
      } else if (xhr.status >= 400) {
        $(".login-jumbotron")
          .prepend(await $.get(`/alerts/login.jsp?language=${language}`))
          .alert();
      }
    });
});

$(".registration form").on("submit", function (e) {
  e.preventDefault();
  $.ajax({
    url: "/api/user/register",
    type: "PUT",
    data: JSON.stringify($(this).serializeObject()),
  })
    .done(async (data) => {
      const language = queryString.parse(window.location.search).language || 'en';
      window.location.href = "/index.jsp";
      $(".registration")
          .prepend(await $.get(`/alerts/registration.jsp?language=${language}`))
        .alert();
    })
    .fail(async (xhr) => {
      const language = queryString.parse(window.location.search).language || 'en';
      if (xhr.status >= 500) {
        $(".registration")
          .prepend(await $.get(`/alerts/reg-fail.jsp?language=${language}`))
          .alert();
      }
      else if(xhr.status >=400) {
        $(".registration")
          .prepend(await $.get(`/alerts/reg-empty.jsp?language=${language}`))
          .alert();
      }
    });
});


$(".take-a-ride form").on("submit", function (e) {
  e.preventDefault();
  $.ajax({
    url: "/api/order",
    type: "PUT",
    data: JSON.stringify($(this).serializeObject()),
  })
    .done(async (data) => {
      const language = queryString.parse(window.location.search).language || 'en';
      window.location.href = "/client/order-details.jsp";
      $(".take-a-ride")
          .prepend(await $.get(`/alerts/success.jsp?language=${language}`)
        )
        .alert();
    })
    .fail(async (xhr) => {
      const language = queryString.parse(window.location.search).language || 'en';
      if (xhr.status >= 500) {
        $(".take-a-ride")
          .prepend(await $.get(`/alerts/fail.jsp?language=${language}`))
          .alert();
      }
    });
});

$(".confirm-order form").on("submit", function (e) {
  e.preventDefault();
  $.ajax({
    url: "/client/confirm-order",
    type: "POST",
    data: JSON.stringify($(this).serializeObject()),
  })
    .done((data) => {
      window.location.href = "/client/order.jsp";
    })
    .fail(async (xhr) => {
      const language = queryString.parse(window.location.search).language || 'en';
      if (xhr.status >= 400) {
        $(".confirm-order")
          .prepend(await $.get(`/alerts/server.jsp?language=${language}`))
          .alert();
      }
    });
});

$(".cancel-order form").on("submit", function (e) {
  e.preventDefault();
  $.ajax({
    url: "/client/cancel-order",
    type: "POST",
    data: JSON.stringify($(this).serializeObject()),
  })
    .done((data) => {
      window.location.href = "/client/index.jsp";
    })
    .fail(async (xhr) => {
      const language = queryString.parse(window.location.search).language || 'en';
      if (xhr.status >= 400) {
        $(".cancel-order")
           .prepend(await $.get(`/alerts/server.jsp?language=${language}`))
          .alert();
      }
    });
});

$(".finish-order form").on("submit", function (e) {
  e.preventDefault();
  $.ajax({
    url: "/client/done-order",
    type: "POST",
    data: JSON.stringify($(this).serializeObject()),
  })
    .done((data) => {
      window.location.href = "/client/index.jsp";
    })
    .fail(async (xhr) => {
      const language = queryString.parse(window.location.search).language || 'en';
      if (xhr.status >= 400) {
        $(".finish-order")
          .prepend(await $.get(`/alerts/server.jsp?language=${language}`))
          .alert();
      }
    });
});
