package web.viewservice.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.DBManager;
import database.OrderDAO;
import database.entity.order.Order;
import database.entity.user.User;
import database.entity.user.UserType;

/**
 * @see web.orderservice.GetUpdateDeleteOrder
 */
@WebServlet("/admin/orders")
public class OrdersView extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(OrdersView.class);
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");

		if (user == null || user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				logger.warn("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
				return;
			}
		}
		String spageid = request.getParameter("page");
		if (spageid == null) {
			spageid = "1";
		}
		int pageId;
		try {
			pageId = Integer.parseInt(spageid);
		} catch (NumberFormatException e) {
			pageId = 1;
		}
		int limit = 10;

		Integer sortValue;
		Integer filterValue;

		try {
			sortValue = Integer.valueOf(request.getParameter("sort"));
		} catch (NumberFormatException e) {
			sortValue = 4;
		}
		try {
			filterValue = Integer.valueOf(request.getParameter("filter"));
		} catch (NumberFormatException e) {
			filterValue = 0;
		}

		request.setAttribute("sort", sortValue);
		request.setAttribute("filter", sortValue);
		
		String dateFrom = request.getParameter("date_from");
		String dateTo = request.getParameter("date_to");

		try {
			List<Order> orders = new ArrayList<>();
			if (filterValue == 0) {
				orders = OrderDAO.getInstance(DBManager.getInstance()).getAllOrders(limit, limit * (pageId - 1), sortValue);
			}
			else if(filterValue == 1) {
				orders = OrderDAO.getInstance(DBManager.getInstance()).getOrdersFilterByDate(limit, limit * (pageId - 1), sortValue, dateFrom, dateTo);
			}
			else if(filterValue == 2) {
				int userId = 0;
				
				try {
					userId = Integer.valueOf(request.getParameter("id"));
				}
				catch (NumberFormatException e) {
					logger.error("No id");
				}
				orders = OrderDAO.getInstance(DBManager.getInstance()).getOrdersByUserId(userId, limit, limit * (pageId - 1), sortValue);
			}
			
			request.setAttribute("orders", orders);
			request.setAttribute("page", pageId);
			try {
				request.setAttribute("pages",
						Math.round(Math.ceil(OrderDAO.getInstance(DBManager.getInstance()).getCountOrders() / (double) limit)));
				request.getRequestDispatcher("/admin/orders.jsp").forward(request, response);
			} catch (SQLException | ServletException | IOException e1){
				logger.error("Database or server is not available.", e1);
			}
			
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
			request.getRequestDispatcher("/admin/orders.jsp").forward(request, response);
			logger.error("Couldn't view orders. Database is unavailable.\n", e);
			return;
		}
	}
}
