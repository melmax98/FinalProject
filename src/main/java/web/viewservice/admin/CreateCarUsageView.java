package web.viewservice.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.entity.user.User;
import database.entity.user.UserType;

/**
 * @see web.carservice.CreateCarUsageAndGetUsageStatistics
 */
@WebServlet("/admin/create-car-usage")
public class CreateCarUsageView extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(CreateCarUsageView.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");
		if (user == null || user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				logger.warn("Not enough permissions");
				response.getWriter().println("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
				return;
			}
		}
		try {
			request.getRequestDispatcher("/admin/create-car-usage.jsp").forward(request, response);
		} catch (ServletException | IOException e) {
			logger.error("Server is not available.", e);
			return;
		}
	}

}
