package web.viewservice.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.entity.user.User;
import database.entity.user.UserType;

/**
 * @see web.carservice.GetUpdateDeleteCar
 * @see web.viewservice.admin.CarShow
 */
@WebServlet("/admin/edit-car")
public class CarView extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(CarView.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");
		if (user == null || user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				logger.info("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
				return;
			}
		}
		try {
			request.getRequestDispatcher("/admin/edit-car.jsp").forward(request, response);			
		}
		catch(ServletException | IOException e) {
			logger.error("Wasn't able to redirect to a page");
			return;
		}
	}
}
