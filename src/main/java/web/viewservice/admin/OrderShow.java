package web.viewservice.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.entity.user.User;
import database.entity.user.UserType;

/**
 * @see web.orderservice.GetUpdateDeleteOrder
 */
@WebServlet("/admin/order")
public class OrderShow extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(OrderShow.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		int id = 0;

		User user = (User) session.getAttribute("user");
		if (user == null || user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				logger.warn("Not enough permissions");
				response.getWriter().println("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
				return;
			}
		}
		try {			
			id = Integer.valueOf(request.getParameter("id"));
		}
		catch(NumberFormatException e) {
			try {
				request.getRequestDispatcher("/admin/edit-order").forward(request, response);				
			} catch(ServletException | IOException e1) {
				logger.error("Server is not available.", e);
				return;
			}
			logger.warn("Invalid id");
			return;
		}
		try {
			request.getRequestDispatcher("/api/order/" + id).forward(request, response);			
		} catch(ServletException | IOException e) {
			logger.error("Server is not available", e);
			return;
		}
	}

}
