package web.viewservice.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.CarDAO;
import database.DBManager;
import database.entity.car.Car;
import database.entity.user.User;
import database.entity.user.UserType;

/**
 * Servlet checks for permissions, gets all Cars, gets parameters from page to implement pagination and then redirects to jsp page.
 * @see web.carservice.GetAllCars
 * @see database.entity.Car
 * @see database.CarDAO
 */
@WebServlet("/admin/cars")
public class CarsView extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(CarsView.class);
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		
		if (user == null || user.getRole()!=UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
			}
			logger.info("Not enough permissions");
			return;
		}
		
		String spageid = request.getParameter("page");
		if (spageid == null) {
			spageid = "1";
		}
		int pageId;
		try {
			pageId = Integer.parseInt(spageid);
		} catch (NumberFormatException e) {
			pageId = 1;
		}
		int limit = 10;
		
		try {
			List<Car> cars = new ArrayList<>(CarDAO.getInstance(DBManager.getInstance()).getAllCars(limit, limit * (pageId - 1)));
			
			request.setAttribute("cars", cars);
			request.setAttribute("page", pageId);
			request.setAttribute("pages",
					Math.round(Math.ceil(CarDAO.getInstance(DBManager.getInstance()).getCountCars() / (double) limit)));
			try {
				request.getRequestDispatcher("/admin/cars.jsp").forward(request, response);				
			} catch(ServletException | IOException e) {
				logger.error("Server is not available");
				return;
			}
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
			try {				
				response.getWriter().println("Couldn't view cars");
			}
			catch(IOException e1) {
				logger.error("Couldn't send text to user");
				return;
			}
			logger.error("Couldn't view cars. Database is unavailable.\n", e);
			return;
		}
	}
}
