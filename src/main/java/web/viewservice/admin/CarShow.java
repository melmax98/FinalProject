package web.viewservice.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.entity.user.User;
import database.entity.user.UserType;

/**
 * Servlet checks for permissions, gets id of Car and redirects to api.
 * @see web.carservice.GetUpdateDeleteCar
 * @see database.entity.Car
 * @see database.CarDAO
 */
@WebServlet("/admin/car")
public class CarShow extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(CarShow.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		int id = 0;

		User user = (User) session.getAttribute("user");
		if (user == null || user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				logger.info("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
				return;
			}
		}
		try {			
			id = Integer.valueOf(request.getParameter("id"));
		}
		catch(NumberFormatException e) {
			try {				
				request.getRequestDispatcher("/admin/edit-car").forward(request, response);
				return;
			} catch(ServletException | IOException e1) {
				logger.error("Server is not available.", e1);
				return;
			}
		}
		try {
			request.getRequestDispatcher("/api/car/" + id).forward(request, response);			
		} catch (ServletException | IOException e) {
			logger.error("Server is not available.", e);
			return;
		}
	}

}
