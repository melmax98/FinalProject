package web.viewservice.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.CarUsageDAO;
import database.DBManager;
import database.bean.CarUsage;
import database.entity.user.User;
import database.entity.user.UserType;

/**
 * Servlet checks for permissions, gets car usage statistics, gets parameters from page to implement pagination and then redirects to jsp page.
 * @see web.carservice.CreateCarUsageAndGetUsageStatistics
 * @see database.bean.CarUsage
 * @see database.CarUsageDAO
 */
@WebServlet("/admin/car-usage-statistics")
public class CarUsageStatisticsView extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(CarUsageStatisticsView.class);
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		
		if (user == null || user.getRole()!=UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				logger.warn("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
				return;
			}
		}
		
		String spageid = request.getParameter("page");
		if (spageid == null) {
			spageid = "1";
		}
		int pageId;
		try {
			pageId = Integer.parseInt(spageid);
		} catch (NumberFormatException e) {
			pageId = 1;
		}
		int limit = 10;
		
		try {
			List<CarUsage> carUsage = new ArrayList<>(CarUsageDAO.getInstance(DBManager.getInstance()).getCarUsageStatistics(limit, limit * (pageId - 1)));
			request.setAttribute("carUsage", carUsage);
			request.setAttribute("page", pageId);
			request.setAttribute("pages",
					Math.round(Math.ceil(CarUsageDAO.getInstance(DBManager.getInstance()).getCountCarUsage() / (double) limit)));
			try {
				request.getRequestDispatcher("/admin/car-usage-statistics.jsp").forward(request, response);				
			}
			catch(ServletException | IOException e) {
				logger.error("Server is not availabale.", e);
				return;
			}
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
			try {
				response.getWriter().println("Couldn't view car usage statistics");				
			} catch(IOException e1) {
				logger.error("Couldn't display message to user", e1);
				return;
			}
			logger.error("Couldn't view car usage statistics. Database is unavailable.\n", e);
			return;
		}
	}
}
