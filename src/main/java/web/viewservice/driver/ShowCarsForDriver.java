package web.viewservice.driver;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.CarDAO;
import database.CarUsageDAO;
import database.DBManager;
import database.bean.CarUsage;
import database.bean.UsageStatus;
import database.entity.car.Car;
import database.entity.user.User;
import database.entity.user.UserType;
/**
 * Shows available cars for driver.
 * 
 * @see database.CarDAO
 */
@WebServlet("/driver/index")
public class ShowCarsForDriver extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(ShowCarsForDriver.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		
		if (user == null || user.getRole()!=UserType.DRIVER) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				logger.warn("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
				return;
			}
		}
		
		String spageid = request.getParameter("page");
		if (spageid == null) {
			spageid = "1";
		}
		int pageId;
		try {
			pageId = Integer.parseInt(spageid);
		} catch (NumberFormatException e) {
			pageId = 1;
		}
		int limit = 10;
		
		try {
			CarUsageDAO dbm = CarUsageDAO.getInstance(DBManager.getInstance());
			CarUsage carUsage = dbm.getLastCarUsageByDriverId(user.getId());
			
			try {
				if (UsageStatus.AVAILABLE.equals(carUsage.getStatus())) {
					request.setAttribute("takenCar", carUsage.getCarId());
				}
			}
			catch (NullPointerException e) {
				logger.info("No taken car");
			}
			
			
		} catch(SQLException e) {
			response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
			try {
				response.getWriter().println("Couldn't view cars");				
			} catch(IOException e1) {
				logger.error("Server is not available.", e1);
				return;
			}
			logger.error("Couldn't get car in usage. Database is unavailable.\n", e);
			return;
		}
		
		try {
			List<Car> cars = new ArrayList<>(CarDAO.getInstance(DBManager.getInstance()).getAllCarsForDriver(limit, limit * (pageId - 1)));
			
			request.setAttribute("cars", cars);
			request.setAttribute("page", pageId);
			request.setAttribute("pages",
					Math.round(Math.ceil(CarDAO.getInstance(DBManager.getInstance()).getCountCarsWithoutDriver() / (double) limit)));
			try {
				request.getRequestDispatcher("/driver/cars.jsp").forward(request, response);				
			} catch(ServletException | IOException e1) {
				logger.error("Server is not available.", e1);
				return;
			}
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
			try {
				response.getWriter().println("Couldn't view cars");				
			} catch(IOException e1) {
				logger.error("Server is not available.", e1);
				return;
			}
			logger.error("Couldn't view cars. Database is unavailable.\n", e);
			return;
		}
	}
}
