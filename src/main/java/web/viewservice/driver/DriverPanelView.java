package web.viewservice.driver;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.entity.user.User;
import database.entity.user.UserType;

/**
 * Servlet checks for permissions redirects to jsp page Driver panel (driver/index.jsp).
 */
@WebServlet("/driver/panel")
public class DriverPanelView extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(DriverPanelView.class);
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		
		if (user == null || user.getRole()!=UserType.DRIVER) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
			}
			logger.info("Not enough permissions");
			return;
		}
			try {
				request.getRequestDispatcher("/driver/index.jsp").forward(request, response);				
			} catch(ServletException | IOException e) {
				logger.error("Server is not available");
				return;
			}
	}
}
