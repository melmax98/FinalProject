package web.viewservice.client;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.DBManager;
import database.OrderDAO;
import database.entity.order.Order;
import database.entity.order.OrderStatus;

/**
 * When the ride is finished - than status of order is being changed to "done" and statuses of cars
 * that were performing that order are being changed to "available".
 * User is redirected to main page.
 * 
 * @see database.OrderDAO
 * @see database.entity.Order
 */
@WebServlet("/client/confirm-order")
public class ConfirmOrder extends HttpServlet {

	private static final Logger logger = LogManager.getLogger(ConfirmOrder.class);
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		HttpSession session = request.getSession();
		Order order = (Order) session.getAttribute("order");
		order.setStatus(OrderStatus.IN_PROGRESS);

		OrderDAO dbm = OrderDAO.getInstance(DBManager.getInstance());
		try {
			dbm.updateOrder(order);
			response.getWriter().println(order);
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
			response.getWriter().println("Couldn't place order. We are working to fix this problem as soon as possible.");
			logger.error("Couldn't update order status. Database is unavailable.\n", e);
			return;
		}
	}

}
