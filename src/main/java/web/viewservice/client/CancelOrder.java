package web.viewservice.client;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.CarDAO;
import database.DBManager;
import database.OrderDAO;
import database.entity.car.Car;
import database.entity.car.CarStatus;
import database.entity.order.Order;
import database.entity.order.OrderStatus;

/**
 * If user wants to cancel an order - than status of order is being changed to "cancelled" and statuses of cars
 * that would be performing that order are being changed to "available"
 * 
 * @see database.OrderDAO
 * @see database.entity.Order
 */
@WebServlet("/client/cancel-order")
public class CancelOrder extends HttpServlet {

	private static final Logger logger = LogManager.getLogger(CancelOrder.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		response.setContentType("application/json");
		Order order = (Order) session.getAttribute("order");
		order.setStatus(OrderStatus.CANCELLED);
		OrderDAO dbm = OrderDAO.getInstance(DBManager.getInstance());
		CarDAO carDAO = CarDAO.getInstance(DBManager.getInstance());
		ArrayList<Car> cars = (ArrayList<Car>) session.getAttribute("cars");
		for(Car car: cars) {
			car.setStatus(CarStatus.AVAILABLE);
			try {
				carDAO.updateCar(car);
			} catch (SQLException e) {
				response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
				try {
					response.getWriter().println("Something went wrong. We are working to fix this problem as soon as possible.");					
				} catch(IOException e1) {
					logger.error("Server is not available.", e);
				}
				logger.error("Couldn't update car status. Database is unavailable.\n", e);
				return;
			}
		}
		
		try {
			dbm.updateOrder(order);
			try {
				response.getWriter().println(order);				
			} catch(IOException e1) {
				logger.error("Server is not available.", e1);
				return;
			}
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
			try{
				response.getWriter().println("Couldn't place order. We are working to fix this problem as soon as possible.");				
			} catch (IOException e1) {
				logger.error("Server is not available.", e1);
				return;
			}
			logger.error("Couldn't update order status. Database is unavailable.\n", e);
			return;
		}
	}

}
