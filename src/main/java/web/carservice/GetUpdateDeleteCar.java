package web.carservice;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import database.CarDAO;
import database.DBManager;
import database.entity.car.Car;
import database.entity.car.CarStatus;
import database.entity.car.CarType;
import database.entity.user.User;
import database.entity.user.UserType;
import json.JSONDescriptor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Get method searches for Car by provided id that is extracted from URL.
 * Put method updates Car's parameters.
 * Delete method deletes Car.
 * 
 * In put method data has to be send in JSON format. Type and status have to
 * correspond the certain ENUM.
 * @see database.CarDAO
 * @see database.entity.Car
 */
public class GetUpdateDeleteCar extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(GetUpdateDeleteCar.class);

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		HttpSession session = request.getSession();
		response.setCharacterEncoding("UTF-8");
		CarDAO dbm = CarDAO.getInstance(DBManager.getInstance());
		int id = 0;

		String pathInfo = request.getPathInfo();
		try {
			id = Integer.parseInt(pathInfo.substring(1));
		} catch (NumberFormatException e) {
			logger.error("Couldn't obtain car id");
			return;
		}

		User user = (User) session.getAttribute("user");
		logger.info(user);
		
		if (user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
				return;
			}
		}

		try {
			Car car = dbm.getCar(id);
			response.getWriter().println(car);
			request.setAttribute("car", car);
			request.getRequestDispatcher("/admin/car.jsp").forward(request, response);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (SQLException | IOException e) {
			logger.error("Couldn't get data from SQL./n", e);
			try {
				response.getWriter().println("Couldn't get data from SQL");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to user", e1);
			}
			return;
		}

	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		JSONObject data = null;
		HttpSession session = request.getSession();
		try {
			data = JSONDescriptor.getJSON(request);
		} catch (IOException e) {
			logger.error("Couldn't get data from json", e);
			return;
		}
		response.setContentType("application/json");

		User user = (User) session.getAttribute("user");
		logger.info(user);

		if (user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
			}
		}

		int id = 0;
		String manufacturer = null;
		String model = null;
		CarType type = null;
		Integer seats = null;
		CarStatus status = null;

		String pathInfo = request.getPathInfo();
		try {
			id = Integer.parseInt(pathInfo.substring(1));
		} catch (NumberFormatException e) {
			logger.error("Couldn't obtain car id");
			return;
		}

		try {
			manufacturer = data.getString("manufacturer");
			if ("".equals(manufacturer)) {
				manufacturer = null;
			}
		} catch (JSONException e) {
			logger.info("Parameter manufacturer will not be updated.");
			manufacturer = null;
		}
		try {
			model = data.getString("model");
			if ("".equals(model)) {
				model = null;
			}
		} catch (JSONException e) {
			logger.info("Parameter model will not be updated.");
			model = null;
		}
		try {
			type = data.getEnum(CarType.class, "type");
		} catch (JSONException e) {
			logger.info("Parameter type will not be updated.");
			type = null;
		}
		try {
			if (data.getString("seats").length() == 0) {
				seats = null;
			}
			else {
				seats = data.getInt("seats");
			}
		} catch (JSONException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		try {
			status = data.getEnum(CarStatus.class, "status");
		} catch (JSONException e) {
			logger.info("Parameter status will not be updated.");
			status = null;
		}

		Car car = new Car();
		car.setId(id);
		car.setManufacturer(manufacturer);
		car.setModel(model);
		car.setSeats(seats);

		car.setStatus(status);
		car.setType(type);

		CarDAO dbm = CarDAO.getInstance(DBManager.getInstance());

		try {
			dbm.updateCar(car);
			response.setStatus(HttpServletResponse.SC_OK);
			try {
				response.getWriter().print(car);
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
			}

		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			try {
				response.getWriter().println("Couldn't update car");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to the client", e);
			}
			logger.error("Couldn't update car", e);
			return;
		}
	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		CarDAO dbm = CarDAO.getInstance(DBManager.getInstance());
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");
		logger.info(user);

		if (user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				response.sendRedirect("/index.jsp");
			} catch (IOException e) {
				logger.error("Couldn't send character text to user", e);
			}
		}

		int id = 0;
		String pathInfo = request.getPathInfo();
		try {
			id = Integer.parseInt(pathInfo.substring(1));
		} catch (NumberFormatException e) {
			logger.error("Couldn't obtain car id", e);
			return;
		}

		try {
			boolean result = dbm.deleteCar(id);
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println("Car was deleted: " + result);
		} catch (SQLException | IOException e) {
			logger.error("Couldn't get data from SQL./n", e);
			try {
				response.getWriter().println("Couldn't get data from SQL");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to user", e);
			}
			return;
		}
	}
}
