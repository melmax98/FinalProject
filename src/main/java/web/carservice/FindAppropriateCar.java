package web.carservice;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import database.CarDAO;
import database.DBManager;
import database.entity.car.Car;
import database.entity.car.CarType;
import database.entity.user.User;
import json.JSONDescriptor;

/**
 * Servlet implementation class FindAppropriateCar
 * 
 * Data is returned in JSON format. If there is available car of the type that user asks for - it finds car for him.
 * If there is no available car of required type - car(s) of different type is suggested for user.
 */
public class FindAppropriateCar extends HttpServlet {

	private static final Logger logger = LogManager.getLogger(FindAppropriateCar.class);

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		JSONObject data;
		
		try {
			data = JSONDescriptor.getJSON(request);
		} catch (IOException e) {
			logger.error("Couldn't get data from json", e);
			return;
		}
		response.setContentType("application/json");
		
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");
		int id = user.getId();
		request.setAttribute("id", id);

		List<Car> appropriateCars = new ArrayList<>();

		try {
			int seats = data.getInt("seats");
			CarType type = data.getEnum(CarType.class, "type");

			CarDAO dbm = CarDAO.getInstance(DBManager.getInstance());

			try {
				appropriateCars = dbm.findAppropriateCar(seats, type);
				response.getWriter().println(appropriateCars);
				request.setAttribute("cars", appropriateCars);
				response.setStatus(HttpServletResponse.SC_OK);
				request.getRequestDispatcher("/client/order-details").forward(request, response);
			} catch (SQLException | ServletException e) {
				response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
				try {
					response.getWriter().println("Can't find car");
				} catch (IOException e1) {
					logger.error("Couldn't send character text to the client", e);
				}
				logger.error("Can't get data from SQL. \n", e);
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
			}

		} catch (JSONException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			try {
				response.getWriter().println("No login or password in JSON data");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to the client", e1);
			}
			logger.error("No such login or password in JSON data. \n", e);
			return;
		}
	}
}
