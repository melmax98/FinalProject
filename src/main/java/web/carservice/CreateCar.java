package web.carservice;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import database.CarDAO;
import database.DBManager;
import database.entity.car.Car;
import database.entity.car.CarStatus;
import database.entity.car.CarType;
import database.entity.user.User;
import database.entity.user.UserType;
import json.JSONDescriptor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * In put method data has to be send in JSON format. Type has to be one of
 * BASIC, COMFORT, VAN
 * 
 * This servlet interacts with CarDAO. 
 * @see CarDAO
 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
 */
public class CreateCar extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(CreateCar.class);

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		JSONObject data = null;
		HttpSession session = request.getSession();
		
		try {
			data = JSONDescriptor.getJSON(request);
		} catch (IOException e) {
			logger.error("Couldn't get data from json");
			return;
		}
		response.setContentType("application/json");

		User user = (User) session.getAttribute("user");
		logger.info("User: {}", user);

		if (user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				logger.info("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
			}
		}

		try {
			String manufacturer = data.getString("manufacturer");
			String model = data.getString("model");
			CarType type = data.getEnum(CarType.class, "type");
			Integer seats = data.getInt("seats");
			CarStatus status = data.getEnum(CarStatus.class, "status");

			if (manufacturer.length() == 0 || model.length() == 0 || seats == null) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().println("All fields are required to add a car.");
				logger.info("All fields are required to add a car.");

				return;
			}

			CarDAO dbm = CarDAO.getInstance(DBManager.getInstance());

			try {
				Car car = dbm.createCar(manufacturer, model, type, seats, status);
				response.setStatus(HttpServletResponse.SC_OK);
				response.getWriter().print(car);

			} catch (SQLException | IOException e) {
				response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
				response.getWriter().println("Couldn't create a car");
				logger.error("Couldn't create a car. Database is unavailable.\n", e);
				return;
			}

		} catch (JSONException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			try {
				response.getWriter().println("Error while formatting data from JSON");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to the client", e);
			}
			logger.error("Error while formatting data from JSON.\n", e);
			return;
		} catch (IOException e) {
			logger.error("Couldn't send character text to the client", e);
		}
	}
}
