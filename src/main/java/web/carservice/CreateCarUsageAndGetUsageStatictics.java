package web.carservice;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import database.CarUsageDAO;
import database.DBManager;
import database.bean.CarUsage;
import database.bean.UsageStatus;
import database.entity.user.User;
import database.entity.user.UserType;
import json.JSONDescriptor;

/**
 * Servlet implementation class CreateCarUsageAndGetUsageStatictics
 * 
 * In put method data has to be send in JSON format. There is no implementation
 * of update and delete car usage because it's statistics. Get method returns
 * JSON
 * 
 * @see CarUsageDAO
 */
public class CreateCarUsageAndGetUsageStatictics extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LogManager.getLogger(CreateCarUsageAndGetUsageStatictics.class);

	@Override
	public void init(ServletConfig sConfig) throws ServletException {
		super.init(sConfig);
		super.init();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");
		logger.info(user);

		if (user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				logger.warn("Not enough permissions.");
				response.getWriter().println("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
			}
		}

		CarUsageDAO dbm = CarUsageDAO.getInstance(DBManager.getInstance());

		try {
			List<CarUsage> cars = new ArrayList<>(dbm.getCarUsageStatistics());

			try {
				JSONArray jsArray = new JSONArray(cars);

				response.getWriter().print(jsArray);
				response.setStatus(HttpServletResponse.SC_OK);
			} catch (JSONException e) {
				logger.error(e);
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
			}

		} catch (SQLException e) {
			logger.error("Couldn't get data from SQL.\n", e);
			try {
				response.getWriter().println("Couldn't get data from SQL");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to the client", e);
			}
			return;
		}
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		JSONObject data = null;
		try {
			data = JSONDescriptor.getJSON(request);
		} catch (IOException e) {
			logger.error("Couldn't get data from json");
			return;
		}
		response.setContentType("application/json");

		User user = (User) session.getAttribute("user");
		logger.info("User: {}", user);

		if (user.getRole() != UserType.ADMIN && user.getRole() != UserType.DRIVER) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				logger.warn("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
			}
		}

		try {
			UsageStatus status = data.getEnum(UsageStatus.class, "status");
			Integer driverId = data.getInt("driver_id");
			Integer carId = data.getInt("car_id");

			if (status == null || driverId == null || carId == null) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().println(new JSONStringer().object().key("error")
						.value("All fields are required to add a car usage.").endObject().toString());
				logger.info("All fields are required to add a car usage.");

				return;
			}

			CarUsageDAO dbm = CarUsageDAO.getInstance(DBManager.getInstance());

			if (UsageStatus.WITHOUT_DRIVER.equals(status)) {
				CarUsage carUsage = dbm.getLastCarUsageByCarId(carId);
				
				if(carUsage.getDriverId() != driverId || carUsage == null) {
					logger.warn("Driver can only leave the car that he is driving.\n");
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					return;
				}
			} else if(UsageStatus.AVAILABLE.equals(status)) {
				CarUsage carUsage = dbm.getLastCarUsageByDriverId(user.getId());
				
				if(carUsage.getStatus().equals(UsageStatus.AVAILABLE)) {
					logger.warn("Driver can only take 1 car.\n");
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					return;
				}
			}

			try {
				boolean success = dbm.createCarUsage(status, driverId, carId);

				if (!success) {
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					response.getWriter().println(new JSONStringer().object().key("error")
							.value("Couldn't create a car usage").endObject().toString());
					logger.warn("Couldn't create a car usage.\n");
				} else {
					response.setStatus(HttpServletResponse.SC_OK);
					response.getWriter()
							.print(new JSONStringer().object().key("success").value(true).endObject().toString());
				}

			} catch (SQLException | IOException e) {
				response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
				response.getWriter().println(new JSONStringer().object().key("error")
						.value("Couldn't create a car usage").endObject().toString());
				logger.error("Couldn't create a car usage. Database is unavailable.\n", e);
				return;
			}

		} catch (JSONException | SQLException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			try {
				response.getWriter().println(new JSONStringer().object().key("error")
						.value("Error while formatting data from JSON").endObject().toString());
			} catch (IOException | JSONException e1) {
				logger.error("Couldn't send character text to the client", e);
			}
			logger.error("Error while formatting data from JSON.\n", e);
			return;
		} catch (IOException e) {
			logger.error("Couldn't send character text to the client", e);
		}
	}
}
