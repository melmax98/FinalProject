package web.orderservice;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import database.DBManager;
import database.OrderDAO;
import database.entity.car.CarType;
import database.entity.order.Order;
import database.entity.order.OrderStatus;
import database.entity.user.User;
import database.entity.user.UserType;
import json.JSONDescriptor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Get method searches for Order by provided id that is extracted from URL.
 * Put method updates Orders's parameters.
 * Delete method deletes an Order.
 * 
 * In put method data has to be send in JSON format. Type and status have to
 * correspond the certain ENUM.
 * @see database.OrderDAO
 * @see database.entity.Order
 */
public class GetUpdateDeleteOrder extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(GetUpdateDeleteOrder.class);

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		HttpSession session = request.getSession();

		OrderDAO dbm = OrderDAO.getInstance(DBManager.getInstance());
		int id = 0;

		String pathInfo = request.getPathInfo();
		try {
			id = Integer.parseInt(pathInfo.substring(1));
		} catch (NumberFormatException e) {
			logger.error("Couldn't obtain order id");
			return;
		}

		User user = (User) session.getAttribute("user");
		logger.info(user);

		if (user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
				return;
			}
		}

		try {
			Order order = dbm.getOrder(id);
			request.setAttribute("order", order);
			request.getRequestDispatcher("/admin/order.jsp").forward(request, response);
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println(order);
		} catch (SQLException | IOException e) {
			logger.error("Couldn't get data from SQL./n", e);
			try {
				response.getWriter().println("Couldn't get data from SQL");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to user", e1);
			}
			return;
		}

	}

	/**
	 * In put method data has to be send in JSON format. Type and status have to
	 * correspond to certain ENUMs.
	 */
	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		JSONObject data = null;
		HttpSession session = request.getSession();
		try {
			data = JSONDescriptor.getJSON(request);
		} catch (IOException e) {
			logger.error("Couldn't get data from json", e);
			return;
		}
		response.setContentType("application/json");

		User user = (User) session.getAttribute("user");
		logger.info(user);

		int id = 0;
		Integer carId = null;
		Integer driverId = null;
		JSONArray to = null;
		JSONArray from = null;
		CarType carType = null;
		Integer passengers = null;
		Integer price = null;
		Integer userId = null;
		OrderStatus status = null;
		Integer waitTime = null;

		String pathInfo = request.getPathInfo();

		try {
			id = Integer.parseInt(pathInfo.substring(1));
		} catch (NumberFormatException e) {
			logger.error("Couldn't obtain order id");
			return;
		}

		try {
			carId = data.getInt("car_id");
		} catch (JSONException e) {
			logger.info("Parameter car id will not be updated.");
			carId = null;
		}
		try {
			driverId = data.getInt("driver_id");
		} catch (JSONException e) {
			logger.info("Parameter driver id will not be updated.");
			driverId = null;
		}
		try {
			to = data.getJSONArray(("to"));
		} catch (JSONException e) {
			logger.info("Parameter to will not be updated.");
			to = null;
		}
		try {
			from = data.getJSONArray(("from"));
		} catch (JSONException e) {
			logger.info("Parameter from will not be updated.");
			from = null;
		}
		try {
			carType = data.getEnum(CarType.class, "car_type");
		} catch (JSONException e) {
			logger.info("Parameter type will not be updated.");
			carType = null;
		}
		try {
			passengers = data.getInt("passengers");
		} catch (JSONException e) {
			logger.info("Parameter passengers will not be updated.");
			passengers = null;
		}
		try {
			price = data.getInt("price");
		} catch (JSONException e) {
			logger.info("Parameter price will not be updated.");
			price = null;
		}

		try {
			userId = data.getInt("user_id");
		} catch (JSONException e) {
			logger.info("Parameter user id will not be updated.");
			userId = null;
		}

		try {
			waitTime = data.getInt("wait_time");
		} catch (JSONException e) {
			logger.info("Parameter wait time will not be updated.");
			waitTime = null;
		}

		try {
			status = data.getEnum(OrderStatus.class, "status");
		} catch (JSONException e) {
			logger.info("Parameter status will not be updated.");
			status = null;
		}

		if (user.getRole() != UserType.ADMIN && (user.getRole() != UserType.CLIENT && user.getId() != userId)) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				logger.warn("Not enough permissions");
				response.getWriter().println("Not enough permissions");
				response.sendRedirect("/index.jsp");
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
				return;
			}
		}

		Order order = new Order();

		if (OrderStatus.DONE.equals(status)) {
			order.setFinishedAt(LocalDateTime.now());
		}

		order.setId(id);
		order.setCarId(carId);
		order.setDriverId(driverId);
		order.setFrom(from);
		order.setTo(to);
		order.setStatus(status);
		order.setCarType(carType);
		order.setPrice(price);
		order.setPassengers(passengers);
		order.setWaitTime(waitTime);
		order.setUserId(userId);

		OrderDAO dbm = OrderDAO.getInstance(DBManager.getInstance());

		try {
			dbm.updateOrder(order);
			response.setStatus(HttpServletResponse.SC_OK);
			try {
				response.getWriter().print(order);
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
			}

		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			try {
				response.getWriter().println("Couldn't update order");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to the client", e);
			}
			logger.error("Couldn't update order", e);
			return;
		}
	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		OrderDAO dbm = OrderDAO.getInstance(DBManager.getInstance());
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");
		logger.info(user);

		if (user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				logger.warn("Not enough permissions");
				response.getWriter().println("Not enough permissions");
				response.sendRedirect("/index.jsp");
			} catch (IOException e) {
				logger.error("Couldn't send character text to user", e);
				return;
			}
		}

		int id = 0;
		String pathInfo = request.getPathInfo();
		try {
			id = Integer.parseInt(pathInfo.substring(1));
		} catch (NumberFormatException e) {
			logger.error("Couldn't obtain order id", e);
			return;
		}

		try {
			boolean result = dbm.deleteOrder(id);
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println("Order was deleted: " + result);
		} catch (SQLException | IOException e) {
			logger.error("Couldn't get data from SQL./n", e);
			try {
				response.getWriter().println("Couldn't get data from SQL");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to user", e);
			}
			return;
		}
	}
}
