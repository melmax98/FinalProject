package web.orderservice;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;

import database.DBManager;
import database.OrderDAO;
import database.entity.order.Order;
import database.entity.user.User;
import database.entity.user.UserType;

/**
 * Servlet implementation class GetAllOrders
 * Gets all orders from database, converts them into Order entities
 * 
 * @see database.OrderDAO
 * @see database.entity.Order
 */
public class GetAllOrders extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(GetAllOrders.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		logger.info(user);

		if (user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				logger.warn("Not enough permissions.");
				response.sendRedirect("/index.jsp");
				return;
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
				return;
			}
		}

		OrderDAO dbm = OrderDAO.getInstance(DBManager.getInstance());

		try {
			List<Order> orders = new ArrayList<>(dbm.getAllOrders());

			try {
				JSONArray jsArray = new JSONArray(orders);

				response.getWriter().print(jsArray);
				response.setStatus(HttpServletResponse.SC_OK);
			} catch (JSONException e) {
				logger.error(e);
			} catch (IOException e) {
				logger.error("Couldn't send character text to the client", e);
			}

		} catch (SQLException e) {
			logger.error("Couldn't get data from SQL.\n", e);
			try {
				response.getWriter().println("Couldn't get data from SQL");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to the client", e);
			}
			return;
		}
	}
}