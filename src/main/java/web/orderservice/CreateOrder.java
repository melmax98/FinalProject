package web.orderservice;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixElement;
import com.google.maps.model.DistanceMatrixRow;
import com.google.maps.model.GeocodingResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import database.CarDAO;
import database.CarUsageDAO;
import database.DBManager;
import database.GetFromProperties;
import database.OrderDAO;
import database.entity.car.Car;
import database.entity.car.CarStatus;
import database.entity.car.CarType;
import database.entity.order.Order;
import database.entity.user.User;
import json.JSONDescriptor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Servlet implementation class CreateOrder
 * 
 * In put method data has to be send in JSON format. 
 * Enums has to he sent all UPPERCASE
 * CarType: BASIC, COMFORT, VAN
 * OrderStatus: CREATED, DRIVER_ON_THE_WAY, IN_PROGRESS, DONE, CANCELLED
 * This servlet uses Google Maps API to build root, get parameters of it.
 * To use Google Maps API - API key must be provided.
 * It's stored in app.properties file.
 * @see database.OrderDAO
 * @see database.entity.Order
 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
 */
public class CreateOrder extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(CreateOrder.class);

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		JSONObject data = null;
		HttpSession session = request.getSession();

		try {
			data = JSONDescriptor.getJSON(request);
		} catch (IOException e) {
			logger.error("Couldn't get data from json");
			return;
		}
		response.setContentType("application/json");

		User user = (User) session.getAttribute("user");
		Integer userId = user.getId();
		logger.info("User: {}", user);

		try {
			String toString = data.getString("to");
			String fromString = data.getString("from");
			CarType carType = data.getEnum(CarType.class, "car_type");
			Integer passengers = data.getInt("passengers");

			CarDAO carDAO = CarDAO.getInstance(DBManager.getInstance());
			List<Car> carList;
			try {
				carList = carDAO.findAppropriateCar(passengers, carType);
				session.setAttribute("cars", carList);
			} catch (SQLException e3) {
				response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
				logger.error("Couldn't find appropriate cars. Database is unavailable.\n", e3);
				return;
			}

			if (toString == null || fromString == null || passengers == 0 || userId == 0) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				logger.info("All fields are required to add a car.");

				return;
			}

			GeoApiContext context = new GeoApiContext.Builder().apiKey(GetFromProperties.getPropValue("api-key"))
					.build();
			String city = ", Vinnytsia, Vinnyts'ka oblast, 21000";
			String[] fromAdress = (fromString + city).split("asd fuaslkfhjjakldhf jka fghehas ");
			String[] toAdress = (toString + city).split("asd fuaslkfhjjakldhf jka fghehas ");
			DistanceMatrix distanceMatrix = null;
			try {
				distanceMatrix = DistanceMatrixApi.getDistanceMatrix(context, fromAdress, toAdress).await();
			} catch (ApiException | InterruptedException e1) {
				logger.error("API error\n", e1);
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				try {
					Thread.currentThread().interrupt();
					response.getWriter().println("Wrong addresses");
				} catch (IOException e2) {
					logger.error("Couldn't send character text to the client", e2);
				}
			}
			DistanceMatrixRow row = distanceMatrix.rows[0];
			DistanceMatrixElement element = row.elements[0];

			JSONArray from = null;
			JSONArray to = null;
			try {
			GeocodingResult[] results = GeocodingApi.geocode(context, distanceMatrix.originAddresses[0]).await();

				from = new JSONArray().put(results[0].geometry.location.lat)
						.put(results[0].geometry.location.lng);

			results = GeocodingApi.geocode(context, distanceMatrix.destinationAddresses[0]).await();
				to = new JSONArray().put(results[0].geometry.location.lat).put(results[0].geometry.location.lng);				
			} catch (JSONException | ApiException| InterruptedException e) {
				logger.error("Couldn't form JSON arrays.", e);
				Thread.currentThread().interrupt();
				return;
			}

			OrderDAO dbm = OrderDAO.getInstance(DBManager.getInstance());
			Integer price = 0;
			for (Car car : carList) {
				if (car.getType().equals(CarType.BASIC)) {
					price += (int) Math.ceil((element.distance.inMeters * 0.015));
				} else if (car.getType().equals(CarType.VAN)) {
					price += (int) Math.ceil((element.distance.inMeters * 0.02));
				} else if (car.getType().equals(CarType.COMFORT)) {
					price += (int) Math.ceil((element.distance.inMeters * 0.025));
				}
				car.setStatus(CarStatus.IN_USAGE);
				try {
					carDAO.updateCar(car);
				} catch (SQLException e) {
					response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
					logger.error("Couldn't update car status. Database is unavailable.\n", e);
					return;
				}
			}

			Integer carId = carList.get(0).getId();
			int spent = dbm.getSpentMoney(userId);
			if (spent >= 1000) {
				price = (int) (price * 0.9);
			}

			if (price < 40) {
				price = 40;
			}
			Integer driverId = null;
			try {
				driverId = CarUsageDAO.getInstance(DBManager.getInstance()).getDriverIdByCarId(carId);
			} catch (SQLException e1) {
				response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
				logger.error("Couldn't get driver id. Database is unavailable.\n", e1);
				return;
			}

			session.setAttribute("distance", element.distance);
			session.setAttribute("drivingTime", element.duration);
			logger.info("Car id: {}, Driver id: {}, To: {}, From: {}, Car type: {}, passengers: {}, price: {}, User id: {}", carId, driverId, to, from, carType, passengers, price, userId);
			try {
				Order order = dbm.createOrder(carId, driverId, to, from, carType, passengers, price, userId);
				session.setAttribute("order", order);
				session.setAttribute("price", order.getPrice());
				response.getWriter().println(order);
				response.setStatus(HttpServletResponse.SC_OK);
			} catch (SQLException | IOException e) {
				response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
				response.getWriter().println("Couldn't create an order");
				logger.error("Couldn't create an order. Database is unavailable.\n", e);
				return;
			}

		} catch (JSONException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			try {
				response.getWriter().println("Error while formatting data from JSON");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to the client", e);
			}
			logger.error("Error while formatting data from JSON.\n", e);
			return;
		} catch (IOException e) {
			logger.error("Couldn't send character text to the client", e);
		} catch (SQLException e2) {
			response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
			logger.error("Couldn't get spent money. Database is unavailable.\n", e2);
			return;
		}
	}
}
