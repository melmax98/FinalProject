package web;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;

/** Extending the TagSupport interface */

public class CopyrightTag extends TagSupport {
	private static final long serialVersionUID = 6724228552107290340L;

	/** Invokes when the start tag of the custom tag is encountered */
	@Override
	public int doStartTag() throws JspException {
		try {
			/** Creating an instance of the JSPWriter for displaying the output */
			JspWriter out = pageContext.getOut();
			out.println("<BR><center><B>Copyright Maksym Melnyk �<B><center>");
		} catch (Exception ioException) {
			return SKIP_BODY;
		}
		/** Returning the SKIP_BODY, as the body content is not be evaluated */
		return SKIP_BODY;
	}

	/** Invokes when the end tag of the custom tag is encountered */
	@Override
	public int doEndTag() throws JspException {
		/** Skip the processing of the rest of the page */
		return SKIP_BODY;
	}
}