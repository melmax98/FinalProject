package web.userservice;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;

import database.DBManager;
import database.UserDAO;
import database.entity.user.User;
import database.entity.user.UserType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Servlet implementation class GetAllUsers
 * 
 * Gets all users from database and converts them into list of User entities.
 * @see dabatase.UserDAO
 * @see database.entity.User
 */

public class GetAllUsers extends HttpServlet {

	private static final Logger logger = LogManager.getLogger(GetAllUsers.class);
	private static final long serialVersionUID = 541140114421321435L;


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");

		if (user == null || user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				logger.warn("Not enough permissions.");
				response.sendRedirect("/index.jsp");
			} catch (IOException e) {
				logger.error("Couldn't send character data to user");
				return;
			}
		}

		UserDAO dbm = UserDAO.getInstance(DBManager.getInstance());

		try {
			List<User> users = new ArrayList<>(dbm.getAllUsers());

			try {
				JSONArray jsArray = new JSONArray(users);

				response.getWriter().print(jsArray);
				response.setStatus(HttpServletResponse.SC_OK);
			} catch (JSONException | IOException e) {
				logger.error(e);
			}

		} catch (SQLException e) {
			logger.error("Couldn't get data from SQL.\n", e);
			try {
				response.getWriter().println("Couldn't get data from SQL");
			} catch (IOException e1) {
				logger.error("Couldn't send character data to user", e1);
			}
			return;
		}
	}
}
