package web.userservice;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import com.auth0.jwt.exceptions.JWTCreationException;

import database.DBManager;
import database.GetFromProperties;
import database.UserDAO;
import database.entity.user.User;
import json.JSONDescriptor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This servlet allows to log into system. If login and password corresponds - it returns status OK (200), if not - returns unauthorized status (401)
 */
public class Login extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(Login.class);

	private static final long serialVersionUID = 5856194469321308710L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		JSONObject data = null;
		HttpSession session = request.getSession();
		try {
			data = JSONDescriptor.getJSON(request);
		} catch (IOException e) {
			logger.error("Couldn't obtain data from json", e);
			return;
		}
		response.setContentType("application/json");

		try {
			String login = data.getString("login");
			String password = data.getString("password");

			UserDAO dbm = UserDAO.getInstance(DBManager.getInstance());

			try {
				User user = dbm.authUser(login, password);

				try {
					
					String jsonString = new JSONStringer().object()
							.key("token")
							.value(user.toJWT(GetFromProperties.getPropValue("secret"))).endObject().toString();

					session.setAttribute("user", user);
					response.setStatus(HttpServletResponse.SC_OK);
					response.getWriter().print(jsonString);
				} catch (JWTCreationException | IOException e) {
					response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
					try {
						response.getWriter().println("Can't sign JWT token");
					} catch (IOException e1) {
						logger.error("Couldn't send character text to user", e);
					}
					logger.error("Can't sign JWT token. \n", e);
					return;
				} catch (JSONException e) {
					logger.error("Can't convert data from JSON. \n", e);
				}
			} catch (SQLException e) {
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				try {
					response.getWriter().println("No user with such data");
				} catch (IOException e1) {
					logger.error("Couldn't send character text to user", e);
				}
				logger.error("Can't get data from SQL. \n", e);
				return;
			}

		} catch (JSONException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			try {
				response.getWriter().println("No login or password in JSON data");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to user", e);
			}
			logger.error("No such login or password in JSON data. \n", e);
			return;
		}
	}
}
