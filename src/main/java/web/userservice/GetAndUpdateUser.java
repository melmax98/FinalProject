package web.userservice;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import database.DBManager;
import database.UserDAO;
import database.entity.user.User;
import database.entity.user.UserType;
import json.JSONDescriptor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Get method searches for User by provided id that is extracted from URL.
 * Put method updates Car's parameters.
 * 
 * In put method data has to be send in JSON format. Type and status have to
 * correspond the certain ENUM.
 * @see database.UserDAO
 * @see database.entity.User
 */
public class GetAndUpdateUser extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(GetAndUpdateUser.class);

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		HttpSession session = request.getSession();
		response.setCharacterEncoding("UTF-8");
		UserDAO dbm = UserDAO.getInstance(DBManager.getInstance());

		try {
			String pathInfo = request.getPathInfo();
			int id = Integer.parseInt(pathInfo.substring(1));

			User user = (User) session.getAttribute("user");

			if (user.getRole() != UserType.ADMIN && user.getId() != id) {
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.getWriter().println("Not enough permissions");
				logger.warn("Not enough permissions");
				response.sendRedirect("/index.jsp");
				return;
			}

			user = dbm.getUser(id);

			response.getWriter().println(user);
			request.setAttribute("user", user);
			request.getRequestDispatcher("/admin/user.jsp").forward(request, response);
			
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (SQLException | IOException e) {
			logger.error("Couldn't get data from SQL./n", e);
			try {
				response.getWriter().println("Couldn't get data from SQL");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to user", e);
			}
			return;
		}
	}

	/**
	 * In put method data has to be send in JSON format. role has to be one of
	 * CLIENT, ADMIN, DRIVER
	 */
	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		JSONObject data = null;
		HttpSession session = request.getSession();
		
		try {
			data = JSONDescriptor.getJSON(request);
		} catch (IOException e) {
			logger.error("Couldn't obtain info from json", e);
			return;
		}
		response.setContentType("application/json");

		String login;
		String password;
		UserType role;
		int id = 0;

		String pathInfo = request.getPathInfo();
		try {
			id = Integer.parseInt(pathInfo.substring(1));
		} catch (NumberFormatException e) {
			logger.error("Couldn't obtain user id.", e);
			return;
		}

		User user = (User) session.getAttribute("user");

		if (user.getRole() != UserType.ADMIN && user.getId() != id) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				logger.warn("Not enough permissions");
				response.sendRedirect("/index.jsp");
			} catch (IOException e) {
				logger.error("Couldn't send character text to user");
				return;
			}
		}

		try {
			login = data.getString("login");
			if ("".equals(login)) {
				login = null;
			}
		} catch (JSONException e) {
			login = null;
		}

		try {
			password = data.getString("password");
		} catch (JSONException e) {
			password = null;
		}
		try {
			role = data.getEnum(UserType.class, "role");
		} catch (JSONException e) {
			role = null;
		}

		user = new User();
		user.setLogin(login);
		user.setPassword(password);
		user.setRole(role);
		user.setId(id);

		UserDAO dbm = UserDAO.getInstance(DBManager.getInstance());

		try {
			dbm.updateUser(user);
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().print(user);

		} catch (SQLException | IOException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			try {
				response.getWriter().println("Couldn't update user");
			} catch (IOException e1) {
				logger.error("Couldn't send character data to user.", e);
			}
			logger.error("Couldn't update user.", e);
			return;
		}
	}
	
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDAO dbm = UserDAO.getInstance(DBManager.getInstance());
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");
		logger.info(user);

		if (user.getRole() != UserType.ADMIN) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			try {
				response.getWriter().println("Not enough permissions");
				response.sendRedirect("/index.jsp");
			} catch (IOException e) {
				logger.error("Couldn't send character text to user", e);
			}
		}

		int id = 0;
		String pathInfo = request.getPathInfo();
		try {
			id = Integer.parseInt(pathInfo.substring(1));
		} catch (NumberFormatException e) {
			logger.error("Couldn't obtain user id", e);
			return;
		}

		try {
			boolean result = dbm.deleteUser(id);
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println("User was deleted: " + result);
		} catch (SQLException | IOException e) {
			logger.error("Couldn't get data from SQL./n", e);
			try {
				response.getWriter().println("Couldn't get data from SQL");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to user", e);
			}
			return;
		}
	}
}
