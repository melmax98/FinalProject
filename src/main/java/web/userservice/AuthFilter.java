package web.userservice;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.entity.user.User;

/**
 * Auth filter gets attribute "user" from session and if it's null (user is not
 * authorized) then request is being forwarded to login page.
 * 
 * @author Maksym Melnyk
 *
 */

public class AuthFilter implements Filter {

	private static final Logger logger = LogManager.getLogger(AuthFilter.class);

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		HttpSession session = req.getSession();

		String uri = req.getRequestURI();
		User user = (User) session.getAttribute("user");

		logger.info("Requested Resource: {}", uri);

		if (user == null && !(uri.endsWith("/login") || uri.endsWith("/register"))) {
			logger.info("Unauthorized access request {}", uri);
			res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			res.getWriter().println("Unauthorized access request");
			res.sendRedirect("/index.jsp");
			return;
		} else {
			if (user != null) {
				session.setAttribute("user", user);
			}
			chain.doFilter(request, response);
		}

	}

	public void destroy() {
		// close any resources here
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		logger.info("AuthFilter initialized");
	}

}