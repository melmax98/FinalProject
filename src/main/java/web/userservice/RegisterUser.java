package web.userservice;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import database.DBManager;
import database.UserDAO;
import database.entity.user.User;
import database.entity.user.UserType;
import json.JSONDescriptor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * In put method data has to be send in JSON format.
 * role has to be one of CLIENT, ADMIN, DRIVER
 */
public class RegisterUser extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(RegisterUser.class);
	
	private static final long serialVersionUID = 1L;
       
    @Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		JSONObject data = null;
		HttpSession session = request.getSession();
		
		try {
			data = JSONDescriptor.getJSON(request);
		} catch (IOException e) {
			logger.error("Couldn't obtain data from json", e);
			return;
		}
		response.setContentType("application/json");

		try {
			String login = data.getString("login");
			String password = data.getString("password");
			UserType role = data.getEnum(UserType.class, "role");
			
			if (login.length() == 0 || password.length() == 0) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().println("Login or password is empty");
				logger.info("Login or password is empty");
				
				return;
			}
			
			UserDAO dbm = UserDAO.getInstance(DBManager.getInstance());
			boolean isAnyAdmin = false;
			
			try {
				isAnyAdmin = dbm.isAnyAdmin();
			} catch (SQLException e) {
				response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
				try {
				response.getWriter().println("Couldn't register user");
				}catch (IOException e1) {
					logger.error("Couldn't send character text to user", e1);
				}
				logger.error("Couldn't register user. Database is unavailable.\n", e);
				return;
			}
			
			if (!isAnyAdmin) {
				role = UserType.ADMIN;
			}
			
			User user = (User) session.getAttribute("user");
			
			logger.info("User: {}", user);
			
			if ((role == UserType.ADMIN && isAnyAdmin) && (user.getRole() != UserType.ADMIN)) {
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				try {
					response.getWriter().println("Not enough permissions");					
				} catch (IOException e1) {
					logger.error("Couldn't send character text to user", e1);
				}
				logger.info("Not enough permissions");
				return;
			}
			

			try {
				user = dbm.createUser(login, password, role);
				response.setStatus(HttpServletResponse.SC_OK);
				try {					
					response.getWriter().print(user);
				} catch (IOException e1) {
					logger.error("Couldn't send character text to user", e1);
				}

				
			} catch (SQLException e) {
				response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
				try {
				response.getWriter().println("Couldn't register user");
				} catch (IOException e1) {
					logger.error("Couldn't send character text to user", e1);
				}
				logger.error("Couldn't register user. Database is unavailable.\n", e);
				return;
			}

		} catch (JSONException | IOException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			try {
				response.getWriter().println("Error while formatting data from JSON");
			} catch (IOException e1) {
				logger.error("Couldn't send character text to user", e1);
			}
			logger.error("Error while formatting data from JSON.\n", e);
			return;
		}
	}
}
