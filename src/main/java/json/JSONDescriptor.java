package json;

import java.io.IOException;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;
import org.json.JSONException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class JSONDescriptor {

	private static final Logger logger = LogManager.getLogger(JSONDescriptor.class);

	private JSONDescriptor() {
	}

	public static JSONObject getJSON(HttpServletRequest request) throws IOException {

		String jb = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

		try {
			return new JSONObject(jb);
		} catch (JSONException e) {
			logger.error("Error parsing JSON request string.\n{e}", e);
			throw new JSONException("Error parsing JSON request string");
		}
	}
}