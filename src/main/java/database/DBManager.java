package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import com.mysql.cj.jdbc.MysqlDataSource;

public class DBManager implements GetConnection {

	private static final Logger logger = LogManager.getLogger(DBManager.class);

	private static DBManager database;

	DBManager() {
	}

	public static synchronized DBManager getInstance() {
		if (database == null) {
			database = new DBManager();
		}
		return database;
	}

	private static final String URL = GetFromProperties.getPropValue("db.url");
	private static final String USERNAME = GetFromProperties.getPropValue("db.username");
	private static final String PASSWORD = GetFromProperties.getPropValue("db.password");

	public DataSource getMysqlDataSource() {
		MysqlDataSource dataSource = new MysqlDataSource();

		dataSource.setUrl(URL);
		dataSource.setUser(USERNAME);
		dataSource.setPassword(PASSWORD);
		return dataSource;
	}

	@Override
	public Connection getConnection() throws SQLException {
		return getMysqlDataSource().getConnection();
	}

	public void close(Connection con, Statement st, ResultSet rs) {
		close(rs);
		close(st);
		close(con);
	}

	public void close(Connection con, Statement st) {
		close(st);
		close(con);
	}

	public void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				logger.error("Couldn't close connection.\n{e}", e);
			}
		}
	}

	public void close(Statement st) {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				logger.error("Couldn't close statement.\n{e}", e);
			}
		}
	}

	public void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				logger.error("Couldn't close result set.\n", e);
			}
		}
	}

	public void rollback(Connection connection) {
		if (connection != null) {
			try {
				connection.rollback();
			} catch (SQLException e) {
				logger.error("Cannot rollback transaction", e);
			}
		}
	}
}