package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;

import database.bean.CarUsage;
import database.entity.car.CarType;
import database.entity.order.Order;
import database.entity.order.OrderStatus;

public class OrderDAO {
	private static final Logger logger = LogManager.getLogger(OrderDAO.class);

	private static OrderDAO dbm;
	private static DBManager database;

	private OrderDAO() {
	}
	
	public static synchronized OrderDAO getInstance(DBManager db) {
		Connection connection = null;
		Statement st = null;

		if (dbm == null) {
			dbm = new OrderDAO();
		}
		database = db;

		try {
			connection = database.getConnection();
			st = connection.createStatement();
			st.executeUpdate(INIT_TABLE);
		} catch (SQLException e) {
			logger.fatal("Couldn't get connetion with database.", e);
			System.exit(1);
		} finally {
			database.close(connection, st);
		}

		return dbm;
	}

	public static final String INIT_TABLE = "CREATE TABLE IF NOT EXISTS orders ("
			+ "`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " + "`car_id` INT NOT NULL, "
			+ "`driver_id` INT NOT NULL, " + "`user_id` INT NOT NULL, " + "`from` POINT SRID 4326 NOT NULL, "
			+ "`to` POINT SRID 4326 NOT NULL, "
			+ "`status` ENUM('created', 'driver on the way', 'in progress', 'done', 'cancelled') DEFAULT 'created', "
			+ "`car_type`ENUM('basic', 'comfort', 'van') DEFAULT 'basic', "
			+ "`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, " + "`finished_at` DATETIME, "
			+ "`price` INT NOT NULL, " + "`passengers` INT NOT NULL, " + "`wait_time` INT NOT NULL DEFAULT 7, "
			+ "FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`) ON DELETE CASCADE, "
			+ "FOREIGN KEY (`driver_id`) REFERENCES `users` (`id`) ON DELETE CASCADE)";

	public static final String INSERT_ORDER = "INSERT INTO orders (`car_id`, `driver_id`, `to`, `from`, `car_type`, `passengers`, `price`, `user_id`) "
			+ "VALUES(?, ?, ST_GeomFromText(?, 4326), ST_GeomFromText(?, 4326), ?, ?, ?, ?)";
	public static final String FIND_ALL_ORDERS = "SELECT `id`, `car_id`, `driver_id`, ST_AsText(`from`) AS `from`, ST_AsText(`to`) AS `to`, "
			+ "`status`, `car_type`, `created_at`, `finished_at`, `price`, `passengers`, `wait_time`, `user_id` FROM orders";
	public static final String FIND_ALL_ORDERS_DATE = "SELECT `id`, `car_id`, `driver_id`, ST_AsText(`from`) AS `from`, ST_AsText(`to`) AS `to`, "
			+ "`status`, `car_type`, `created_at`, `finished_at`, `price`, `passengers`, `wait_time`, `user_id` FROM orders WHERE `created_at` > ? AND `created_at` < ?";
	public static final String FIND_ORDER_BY_ID = "SELECT `id`, `car_id`, `driver_id`, ST_AsText(`from`) AS `from`, ST_AsText(`to`) AS `to`, "
			+ "`status`, `car_type`, `created_at`, `finished_at`, `price`, `passengers`, `wait_time`, `user_id` FROM orders WHERE `id` = ?";
	public static final String FIND_ORDERS_BY_USER_ID = "SELECT `id`, `car_id`, `driver_id`, ST_AsText(`from`) AS `from`, ST_AsText(`to`) AS `to`, "
			+ "`status`, `car_type`, `created_at`, `finished_at`, `price`, `passengers`, `wait_time`, `user_id` FROM orders WHERE `user_id` = ?";
	public static final String UPDATE_ORDER = "UPDATE orders SET `car_id` = IFNULL(?, `car_id`), `driver_id` = IFNULL(?, `driver_id`), "
			+ "`user_id` = IFNULL(? , `user_id`), `from` = IFNULL(ST_GeomFromText(?, 4326) ,`from`), `to` = IFNULL(ST_GeomFromText(?, 4326) ,`to`), "
			+ "`status` = IFNULL(?, `status`), "
			+ "`car_type` = IFNULL(?, `car_type`), `price` = IFNULL(?, `price`), `passengers` = IFNULL(?, `passengers`), "
			+ "`wait_time` = IFNULL(?, `wait_time`), `finished_at` = IFNULL(?, `finished_at`) WHERE id = ?";
	public static final String DELETE_ORDER = "DELETE FROM `orders` WHERE id = ?";
	public static final String COUNT_ORDERS = "SELECT COUNT(*) FROM `orders`";
	public static final String GET_SPENT_MONEY_BY_USER_ID = "SELECT sum(`price`) FROM `orders` WHERE `user_id` = ? AND `status` = 'done'";

	/**
	 * @return
	 * @throws JSONException
	 * @throws SQLException
	 */
	
	public int getSpentMoney(int userId) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int result = 0;

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(GET_SPENT_MONEY_BY_USER_ID);
			ps.setInt(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				result += rs.getInt(1);
			}
			return result;
		} catch (SQLException e) {
			logger.error("Couldn't get spent money.", e);
			throw new SQLException("Couldn't get spent money.");
		} finally {
			database.close(connection, ps, rs);
		}
	}
	
	public List<Order> getOrdersFilterByDate (Integer limit, Integer start, Integer sort, String dateFrom, String dateTo) throws SQLException {
		List<Order> list = new ArrayList<>();

		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			connection = database.getConnection();
			if (limit == null) {
				st = connection.createStatement();
				rs = st.executeQuery(FIND_ALL_ORDERS);
			} else {
				
				if (sort == 1) {
					ps = connection.prepareStatement(FIND_ALL_ORDERS_DATE + " ORDER BY `price`" + " LIMIT ?, ?");					
				}
				else if(sort == 2) {
					ps = connection.prepareStatement(FIND_ALL_ORDERS_DATE + " ORDER BY `price` DESC" + " LIMIT ?, ?");	
				}
				else if (sort == 3) {
					ps = connection.prepareStatement(FIND_ALL_ORDERS_DATE + " ORDER BY `created_at`" + " LIMIT ?, ?");	
				}
				else if(sort == 4) {
					ps = connection.prepareStatement(FIND_ALL_ORDERS_DATE + " ORDER BY `created_at` DESC" + " LIMIT ?, ?");
				}
				
				ps.setString(1, dateFrom);					
				ps.setString(2, dateTo);
				ps.setInt(3, start);
				ps.setInt(4, limit);
				rs = ps.executeQuery();
			}
			while (rs.next()) {
				list.add(getOrder(rs));
			}
		} catch (SQLException e) {
			logger.error("Was unnable  to get orders from db.", e);
			throw new SQLException("Was unnable to get orders from db");
		} finally {
			database.close(connection, st, rs);
			database.close(ps);
		}
		return list;
	}

	public Integer getCountOrders () throws SQLException {
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		Integer i = null;
		
		try {
			connection = database.getConnection();
			st = connection.createStatement();
			rs = st.executeQuery(COUNT_ORDERS);
			
			while (rs.next()) {
				i = rs.getInt(1);
			}
		}
		catch(SQLException e) {
			logger.error("Couldn't get count of orders.", e);
			throw new SQLException("Couldn't get count of orders.");
		}
		finally {
			database.close(connection, st, rs);
		}
		return i;
	}
	public Order createOrder(int carId, int driverId, JSONArray to, JSONArray from, CarType carType, int passengers,
			int price, int userId) throws SQLException {

		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(INSERT_ORDER, Statement.RETURN_GENERATED_KEYS);
			Order order = new Order();

			order.setCarId(carId);
			order.setDriverId(driverId);
			order.setFrom(from);
			order.setTo(to);
			order.setStatus(OrderStatus.CREATED);
			order.setCarType(carType);
			order.setPrice(price);
			order.setPassengers(passengers);
			order.setUserId(userId);

			ps.setInt(1, carId);
			ps.setInt(2, driverId);
			ps.setString(3, Order.jsonArrayToGeoString(to));
			ps.setString(4, Order.jsonArrayToGeoString(from));
			ps.setString(5, carType.getValue());
			ps.setInt(6, passengers);
			ps.setInt(7, price);
			ps.setInt(8, userId);

			int affectedRows = ps.executeUpdate();

			if (affectedRows == 0) {
				logger.error("Creating order failed, no rows affected.");
				throw new SQLException("Creating order failed, no rows affected.");
			}

			try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					order.setId(generatedKeys.getInt(1));
				} else {
					logger.error("Creating order failed, no ID obtained.");
					throw new SQLException("Creating order failed, no ID obtained.");
				}
			}
			return order;

		} catch (SQLException e) {
			logger.error("Couldn't insert order to database.", e);
			throw new SQLException("Couldn't insert order to database");
		} finally {
			database.close(connection, ps);
		}
	}

	public List<Order> getAllOrders(Integer limit, Integer start, Integer sort) throws SQLException {
		List<Order> list = new ArrayList<>();

		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			connection = database.getConnection();
			if (limit == null) {
				st = connection.createStatement();
				rs = st.executeQuery(FIND_ALL_ORDERS);
			} else {
				if (sort == 1) {
					ps = connection.prepareStatement(FIND_ALL_ORDERS + " ORDER BY `price`" + " LIMIT ?, ?");					
				}
				else if(sort == 2) {
					ps = connection.prepareStatement(FIND_ALL_ORDERS + " ORDER BY `price` DESC" + " LIMIT ?, ?");	
				}
				else if (sort == 3) {
					ps = connection.prepareStatement(FIND_ALL_ORDERS + " ORDER BY `created_at`" + " LIMIT ?, ?");	
				}
				else if(sort == 4) {
					ps = connection.prepareStatement(FIND_ALL_ORDERS + " ORDER BY `created_at` DESC" + " LIMIT ?, ?");
				}
				
				ps.setInt(1, start);
				ps.setInt(2, limit);
				rs = ps.executeQuery();
			}
			while (rs.next()) {
				list.add(getOrder(rs));
			}
		} catch (SQLException e) {
			logger.error("Was unnable  to get orders from db.", e);
			throw new SQLException("Was unnable to get orders from db");
		} finally {
			database.close(connection, st, rs);
			database.close(ps);
		}
		return list;
	}

	public List<Order> getAllOrders() throws SQLException {
		return this.getAllOrders(null, null, 1);
	}

	public Order getOrder(int id) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Order order = null;

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(FIND_ORDER_BY_ID);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				order = getOrder(rs);
			}
			return order;
		} catch (SQLException e) {
			logger.error("Couldn't get order by id.", e);
			throw new SQLException("Couldn't get order by id");
		} finally {
			database.close(connection, ps, rs);
		}
	}

	public Order getOrder(ResultSet rs) throws SQLException {
		Order order = new Order();

		order.setId(rs.getInt("id"));
		order.setCarId(rs.getInt("car_id"));
		order.setDriverId(rs.getInt("driver_id"));
		order.setFrom(Order.geoStringToJSONArray(rs.getString("from")));
		order.setTo(Order.geoStringToJSONArray(rs.getString("to")));
		order.setStatus(OrderStatus.findByValue(rs.getString("status")));
		order.setCarType(CarType.findByValue(rs.getString("car_type")));
		order.setCreatedAt(LocalDateTime.parse(rs.getString("created_at"), CarUsage.formatter));
		try {
			order.setFinishedAt(LocalDateTime.parse(rs.getString("finished_at"), CarUsage.formatter));
		} catch (NullPointerException e) {
			logger.info("No finished time for this order yet (order id: {})", order.getId());
		}
		order.setPrice(rs.getInt("price"));
		order.setPassengers(rs.getInt("passengers"));
		order.setWaitTime(rs.getInt("wait_time"));
		order.setUserId(rs.getInt("user_id"));

		return order;
	}

	public List<Order> getOrdersByUserId(int userId, Integer limit, Integer start, Integer sort) throws SQLException {
		List<Order> list = new ArrayList<>();

		Connection connection = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			connection = database.getConnection();
			if (limit == null) {
				ps = connection.prepareStatement(FIND_ORDERS_BY_USER_ID);
				ps.setInt(1, userId);
				rs = ps.executeQuery();
			} else {
				if (sort == 1) {
					ps = connection.prepareStatement(FIND_ORDERS_BY_USER_ID + " ORDER BY `price`" + " LIMIT ?, ?");					
				}
				else if(sort == 2) {
					ps = connection.prepareStatement(FIND_ORDERS_BY_USER_ID + " ORDER BY `price` DESC" + " LIMIT ?, ?");	
				}
				else if (sort == 3) {
					ps = connection.prepareStatement(FIND_ORDERS_BY_USER_ID + " ORDER BY `created_at`" + " LIMIT ?, ?");	
				}
				else if(sort == 4) {
					ps = connection.prepareStatement(FIND_ORDERS_BY_USER_ID + " ORDER BY `created_at` DESC" + " LIMIT ?, ?");
				}
				
				ps.setInt(1, userId);
				ps.setInt(2, start);
				ps.setInt(3, limit);
				rs = ps.executeQuery();
			}
			while (rs.next()) {
				list.add(getOrder(rs));
			}
		} catch (SQLException e) {
			logger.error("Was unnable  to get orders from db.", e);
			throw new SQLException("Was unnable to get orders from db");
		} finally {
			database.close(connection, ps, rs);
		}
		return list;
	}
	public List<Order> getOrdersByUserId(int userId) throws SQLException {
		return getOrdersByUserId(userId, null, null, 1);
	}
	

	public void updateOrder(Order order) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(UPDATE_ORDER);
			try {
				ps.setInt(1, order.getCarId());
			} catch (NullPointerException e) {
				ps.setString(1, null);
			}
			try {
				ps.setInt(2, order.getDriverId());
			} catch (NullPointerException e) {
				ps.setString(2, null);
			}
			try {
				ps.setInt(3, order.getUserId());
			} catch (NullPointerException e) {
				ps.setString(3, null);
			}
			try {
				ps.setString(4, Order.jsonArrayToGeoString(order.getFrom()));
			} catch (NullPointerException e) {
				ps.setString(4, null);
			}
			try {
				ps.setString(5, Order.jsonArrayToGeoString(order.getTo()));
			} catch (NullPointerException e) {
				ps.setString(5, null);
			}
			try {
				ps.setString(6, order.getStatus().getValue());
			} catch (NullPointerException e) {
				ps.setString(6, null);
			}
			try {
				ps.setString(7, order.getCarType().getValue());
			} catch (NullPointerException e) {
				ps.setString(7, null);
			}
			try {
				ps.setInt(8, order.getPrice());
			} catch (NullPointerException e) {
				ps.setString(8, null);
			}
			try {
				ps.setInt(9, order.getPassengers());
			} catch (NullPointerException e) {
				ps.setString(9, null);
			}
			try {
				ps.setInt(10, order.getWaitTime());
			} catch (NullPointerException e) {
				ps.setString(10, null);
			}
			if (OrderStatus.DONE.equals(order.getStatus())) {
				try {
					ps.setString(11, LocalDateTime.now().format(CarUsage.formatter));
				} catch (NullPointerException e) {
					ps.setString(11, null);
				}
			}
			else {
				ps.setString(11, null);				
			}
			try {
				ps.setInt(12, order.getId());
			} catch (NullPointerException e) {
				ps.setString(11, null);
			}

			ps.executeUpdate();
		} catch (SQLException e) {
			logger.error("Was unnable to update order.\n", e);
			throw new SQLException("Was unnable to update order" + e);
		} finally {
			database.close(connection, ps);
		}
	}

	public boolean deleteOrder(int id) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;

		boolean flag = false;

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(DELETE_ORDER);
			ps.setInt(1, id);
			ps.executeUpdate();
			flag = true;
			return flag;
		} catch (SQLException e) {
			logger.error("Was unnable to delete order.\n", e);
			throw new SQLException("Was unnable to delete order.\n");
		} finally {
			database.close(connection, ps);
		}
	}
}