package database.bean;

public enum UsageStatus {
	AVAILABLE("available"), WITHOUT_DRIVER("without driver");

	private String status;

	UsageStatus(String value) {
		this.status = value;
	}

	public String getValue() {
		return status;
	}

	public static UsageStatus findByValue(String status) {
		for (UsageStatus value : values()) {
			if (value.getValue().equals(status)) {
				return value;
			}
		}
		return null;
	}
}
