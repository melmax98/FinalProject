package database.bean;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.json.JSONObject;

public class CarUsage {

	/**
	 * At is a LocalDateTime object. When sending information to database - it
	 * should be sent like: DateTimeFormatter formatter =
	 * DateTimeFormatter.ofPattern("yyy-MM-dd HH:mm:ss"); LocalDateTime at =
	 * LocalDateTime.now(); String toSQL = time.format(formatter);
	 * 
	 * When sending information to database transaction has to be executed: firstly
	 * - sendint data to car_usage table, secondly - updating data in cars.status
	 * table.
	 */

	public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	private int id;
	private UsageStatus status;
	private int driverId;
	private int carId;
	private LocalDateTime at;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public UsageStatus getStatus() {
		return status;
	}

	public void setStatus(UsageStatus status) {
		this.status = status;
	}

	public int getDriverId() {
		return driverId;
	}

	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}

	public int getCarId() {
		return carId;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}

	public LocalDateTime getAt() {
		return at;
	}

	public void setAt(LocalDateTime at) {
		this.at = at;
	}

	public static CarUsage fromJSON(JSONObject jsonObject) {
		CarUsage carUsage = new CarUsage();

		carUsage.setId(jsonObject.getInt("id"));
		carUsage.setStatus(jsonObject.getEnum(UsageStatus.class, "status"));
		carUsage.setDriverId(jsonObject.getInt("driver_id"));
		carUsage.setCarId(jsonObject.getInt("car_id"));
		carUsage.setAt(LocalDateTime.parse(jsonObject.getString("at"), formatter));

		return carUsage;
	}

	public static CarUsage fromJSON(String jsonString) {
		return fromJSON(new JSONObject(jsonString));
	}

	public JSONObject toJSON() {
		JSONObject data = new JSONObject();

		data.put("id", this.getId());
		data.put("status", this.getStatus());
		data.put("driver_id", this.getDriverId());
		data.put("car_id", this.getCarId());
		data.put("at", this.getAt().format(formatter));

		return data;
	}

	@Override
	public String toString() {
		return this.toJSON().toString();
	}
}
