package database.entity.user;

public enum UserType {
	ADMIN("administrator"), CLIENT("client"), DRIVER("driver");

	private String role;

	UserType(String value) {
		this.role = value;
	}

	public String getValue() {
		return role;
	}

	public static UserType findByValue(String role) {
		for (UserType value : values()) {
			if (value.getValue().equals(role)) {
				return value;
			}
		}
		return null;
	}
}
