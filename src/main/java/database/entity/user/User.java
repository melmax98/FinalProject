package database.entity.user;

import org.json.JSONObject;
import org.json.JSONException;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class User implements Serializable {
	
	private static final long serialVersionUID = 2409161368462621495L;

	private static final Logger logger = LogManager.getLogger(User.class);

	private Integer id;
	private String login;
	private String password;
	private UserType role;

	public static User fromJSON(JSONObject jsonObject) {
		User user = new User();

		user.setId(jsonObject.getInt("id"));
		user.setLogin(jsonObject.getString("login"));
		user.setRole(jsonObject.getEnum(UserType.class, "role"));

		try {
			// optional field
			user.setPassword(jsonObject.getString("password"));
		} catch (JSONException e) {
			logger.info("Password is null.\n");
		}

		return user;
	}

	public static User fromJSON(String jsonString) {
		return fromJSON(new JSONObject(jsonString));
	}

	public static User fromJWT(String jwtString) {
		return fromJWT(decodeJWT(jwtString));
	}

	public static User fromJWT(DecodedJWT jwtData) {
		return fromJSON(new String(Base64.getDecoder().decode(jwtData.getPayload()), StandardCharsets.UTF_8));
	}

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserType getRole() {
		return role;
	}

	public void setRole(UserType role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return this.toJSON().toString();
	}

	public JSONObject toJSON() {
		JSONObject data = new JSONObject();

		data.put("id", this.getId());
		data.put("login", this.getLogin());
		data.put("role", this.getRole());

		return data;
	}

	public String toJWT(String secret) {
		Algorithm algorithm = Algorithm.HMAC256(secret);
		return JWT.create().withClaim("id", this.getId()).withClaim("login", this.getLogin())
				.withClaim("role", this.getRole().toString()).sign(algorithm);
	}

	public static DecodedJWT verifyJWT(String secret, String jwtString) {
		Algorithm algorithm = Algorithm.HMAC256(secret);

		return JWT.require(algorithm).build().verify(jwtString);
	}

	public static DecodedJWT decodeJWT(String jwtString) {
		return JWT.decode(jwtString);
	}
}
