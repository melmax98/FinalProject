package database.entity.car;

public enum CarStatus {
	AVAILABLE("available"), IN_USAGE("in usage"), WITHOUT_DRIVER("without driver");

	private String status;

	CarStatus(String value) {
		this.status = value;
	}

	public String getValue() {
		return status;
	}

	public static CarStatus findByValue(String status) {
		for (CarStatus value : values()) {
			if (value.getValue().equals(status)) {
				return value;
			}
		}
		return null;
	}
}
