package database.entity.car;

import java.io.Serializable;

import org.json.JSONObject;

public class Car implements Serializable {

	private static final long serialVersionUID = -1236883032870729164L;
	
	private Integer id;
	private String model;
	private String manufacturer;
	private CarType type;
	private Integer seats;
	private CarStatus status;

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public CarType getType() {
		return type;
	}

	public void setType(CarType type) {
		this.type = type;
	}

	public Integer getSeats() {
		return seats;
	}

	public void setSeats(Integer seats) {
		this.seats = seats;
	}

	public CarStatus getStatus() {
		return status;
	}

	public void setStatus(CarStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return this.toJSON().toString();
	}

	public JSONObject toJSON() {
		JSONObject data = new JSONObject();
		
		data.put("id", this.getId());
		data.put("model", this.getModel());
		data.put("manufacturer", this.getManufacturer());
		data.put("type", this.getType());
		data.put("seats", this.getSeats());
		data.put("status", this.getStatus());

		return data;
	}

	public static Car fromJSON(JSONObject jsonObject) {
		Car car = new Car();

		car.setId(jsonObject.getInt("id"));
		car.setManufacturer(jsonObject.getString("manufacturer"));
		car.setModel(jsonObject.getString("model"));
		car.setType(jsonObject.getEnum(CarType.class, "type"));
		car.setSeats(jsonObject.getInt("seats"));
		car.setStatus(jsonObject.getEnum(CarStatus.class, "status"));

		return car;
	}

	public static Car fromJSON(String jsonString) {
		return fromJSON(new JSONObject(jsonString));
	}
}
