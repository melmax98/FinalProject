package database.entity.car;

public enum CarType {
	BASIC("basic"), COMFORT("comfort"), VAN("van");

	private String type;

	CarType(String value) {
		this.type = value;
	}

	public String getValue() {
		return type;
	}

	public static CarType findByValue(String type) {
		for (CarType value : values()) {
			if (value.getValue().equals(type)) {
				return value;
			}
		}
		return null;
	}
}
