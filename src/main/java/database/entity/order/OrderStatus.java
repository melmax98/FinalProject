package database.entity.order;


public enum OrderStatus {
	CREATED("created"), DRIVER_ON_THE_WAY("driver on the way"), IN_PROGRESS("in progress"), DONE("done"), CANCELLED("cancelled");

	private String status;

	OrderStatus(String value) {
		this.status = value;
	}

	public String getValue() {
		return status;
	}

	public static OrderStatus findByValue(String status) {
		for (OrderStatus value : values()) {
			if (value.getValue().equals(status)) {
				return value;
			}
		}
		return null;
	}
}