package database.entity.order;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import database.bean.CarUsage;
import database.entity.car.CarType;


public class Order implements Serializable{
	private static final long serialVersionUID = 3237285480304997227L;

	private static final Logger logger = LogManager.getLogger(Order.class);

	public Integer getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(Integer waitTime) {
		this.waitTime = waitTime;
	}

	private Integer id;
	private Integer carId;
	private Integer driverId;
	private Integer userId;
	private JSONArray from;
	private JSONArray to;
	private OrderStatus status;
	private CarType carType;
	private LocalDateTime createdAt;
	private LocalDateTime finishedAt;
	private Integer price;
	private Integer passengers;
	private Integer waitTime;

	public Integer getPassengers() {
		return passengers;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setPassengers(Integer passengers) {
		this.passengers = passengers;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCarId() {
		return carId;
	}

	public void setCarId(Integer carId) {
		this.carId = carId;
	}

	public Integer getDriverId() {
		return driverId;
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	public JSONArray getFrom() {
		return from;
	}

	public void setFrom(JSONArray from) {
		this.from = from;
	}

	public JSONArray getTo() {
		return to;
	}

	public void setTo(JSONArray to) {
		this.to = to;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public CarType getCarType() {
		return carType;
	}

	public void setCarType(CarType carType) {
		this.carType = carType;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getFinishedAt() {
		return finishedAt;
	}

	public void setFinishedAt(LocalDateTime finishedAt) {
		this.finishedAt = finishedAt;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return this.toJSON().toString();
	}

	public static String jsonArrayToGeoString(JSONArray jsonArray) {
		StringBuffer str = new StringBuffer();
		str.append("POINT(");
		str.append(jsonArray.get(1));
		str.append(" ");
		str.append(jsonArray.get(0));
		str.append(")");
		return str.toString();
	}

	public static JSONArray geoStringToJSONArray(String geoString) {
		StringBuffer str = new StringBuffer(geoString);
		String substring = str.substring(6, str.length() - 1);
		double[] array = Arrays.stream(substring.split(" ")).mapToDouble(Double::parseDouble).toArray();

		JSONArray jsonArray = new JSONArray();
		jsonArray.put(array[1]);
		jsonArray.put(array[0]);

		return jsonArray;
	}

	public JSONObject toJSON() {
		JSONObject data = new JSONObject();

		data.put("id", this.getId());
		data.put("car_id", this.getCarId());
		data.put("driver_id", this.getDriverId());
		data.put("from", this.getFrom());
		data.put("to", this.getTo());
		data.put("status", this.getStatus());
		data.put("car_type", this.getCarType());
		try {
			data.put("created_at", this.getCreatedAt().format(CarUsage.formatter));
		} catch (NullPointerException e) {
			logger.info("No created time for order. Created time is initialized in databese.\n");
		}
		try {
			data.put("finished_at", this.getFinishedAt().format(CarUsage.formatter));
		} catch (NullPointerException e) {
			logger.info("No finished time for current order yet.\n");
		}
		data.put("price", this.getPrice());
		data.put("passengers", this.getPassengers());
		data.put("wait_time", this.getWaitTime());
		data.put("user_id", this.getUserId());

		return data;
	}

	public static Order fromJSON(JSONObject jsonObject) {
		Order order = new Order();

		order.setId(jsonObject.getInt("id"));
		order.setCarId(jsonObject.getInt("car_id"));
		order.setDriverId(jsonObject.getInt("driver_id"));
		order.setFrom(jsonObject.getJSONArray(("from")));
		order.setTo(jsonObject.getJSONArray(("to")));
		order.setStatus(jsonObject.getEnum(OrderStatus.class, "status"));
		order.setCarType(jsonObject.getEnum(CarType.class, "car_type"));
		order.setCreatedAt(LocalDateTime.parse(jsonObject.getString("created_at"), CarUsage.formatter));
		order.setFinishedAt(LocalDateTime.parse(jsonObject.getString("finished_at"), CarUsage.formatter));
		order.setPrice(jsonObject.getInt("price"));
		order.setPassengers(jsonObject.getInt("passengers"));
		order.setWaitTime(jsonObject.getInt("wait_time"));
		order.setUserId(jsonObject.getInt("user_id"));

		return order;
	}

	public static Order fromJSON(String jsonString) {
		return fromJSON(new JSONObject(jsonString));
	}
}
