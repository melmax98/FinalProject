package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.entity.car.CarStatus;
import database.entity.car.CarType;
import database.entity.car.Car;

public class CarDAO {
	private static final Logger logger = LogManager.getLogger(CarDAO.class);

	private static CarDAO dbm;
	private static DBManager database;

	private CarDAO() {
	}
	
	public static synchronized CarDAO getInstance(DBManager db) {
		Connection connection = null;
		Statement st = null;

		if (dbm == null) {
			dbm = new CarDAO();
		}
		database = db;

		try {
			connection = database.getConnection();
			st = connection.createStatement();
			st.executeUpdate(INIT_TABLE);
		} catch (SQLException e) {
			logger.fatal("Couldn't get connetion with database.", e);
			System.exit(1);
		} finally {
			database.close(connection, st);
		}

		return dbm;
	}

	public static final String INIT_TABLE = "CREATE TABLE IF NOT EXISTS cars ("
			+ "id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, model VARCHAR(250) NOT NULL DEFAULT '', "
			+ "manufacturer VARCHAR(32) NOT NULL DEFAULT '',  "
			+ "type ENUM('basic', 'comfort', 'van') DEFAULT 'basic', " + "seats INT NOT NULL, "
			+ "status ENUM('available', 'in usage', 'without driver') DEFAULT 'without driver')";

	public static final String INSERT_CAR = "INSERT INTO cars(`model`, `manufacturer`, `type`, `seats`, `status`) VALUES(?, ?, ?, ?, ?);";
	public static final String FIND_ALL_CARS = "SELECT * FROM cars ORDER BY id";
	public static final String FIND_CARS_FOR_DRIVER = "SELECT * FROM cars WHERE `status` = 'without driver' ORDER BY `id` LIMIT ?, ?";
	public static final String FIND_CAR_BY_ID = "SELECT * FROM cars WHERE id = ?";
	public static final String UPDATE_CAR = "UPDATE cars SET `model` = IFNULL(?, model), `manufacturer` = IFNULL(?, manufacturer), "
			+ "`type` = IFNULL(? , type), `seats` = IFNULL(?, seats), `status` = IFNULL(?, status) WHERE id = ?";
	public static final String DELETE_CAR = "DELETE FROM cars WHERE id = ?";
	public static final String FIND_APPROPRIATE_CAR = "SELECT * FROM cars WHERE `seats` >= ? AND `type` = ? AND `status` = 'available' LIMIT 1";
	public static final String FIND_APPROPRIATE_CARS = "SELECT x.* FROM cars AS x INNER JOIN cars AS y ON y.`status` = 'available'  WHERE x.`status` = 'available' GROUP BY x.id HAVING SUM(y.seats) >= ?";
	public static final String COUNT_CARS = "SELECT COUNT(*) FROM `cars`";	

	/**
	 * @return
	 * @throws SQLException
	 */
	
	public Integer getCountCars () throws SQLException {
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		Integer i = null;
		
		try {
			connection = database.getConnection();
			st = connection.createStatement();
			rs = st.executeQuery(COUNT_CARS);
			
			while (rs.next()) {
				i = rs.getInt(1);
			}
		}
		catch(SQLException e) {
			logger.error("Couldn't get count of cars.", e);
			throw new SQLException("Couldn't get count of cars.");
		}
		finally {
			database.close(connection, st, rs);
		}
		return i;
	}
	
	public Integer getCountCarsWithoutDriver () throws SQLException {
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		Integer i = null;
		
		try {
			connection = database.getConnection();
			st = connection.createStatement();
			rs = st.executeQuery(COUNT_CARS + "WHERE `status` = 'without driver'");
			
			while (rs.next()) {
				i = rs.getInt(1);
			}
		}
		catch(SQLException e) {
			logger.error("Couldn't get count of cars without driver.", e);
			throw new SQLException("Couldn't get count of cars whithout driver.");
		}
		finally {
			database.close(connection, st, rs);
		}
		return i;
	}

	public Car createCar(String manufacturer, String model, CarType type, int seats, CarStatus status)
			throws SQLException {

		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(INSERT_CAR, Statement.RETURN_GENERATED_KEYS);
			Car car = new Car();

			car.setManufacturer(manufacturer);
			car.setModel(model);
			car.setType(type);
			car.setSeats(seats);
			car.setStatus(status);

			ps.setString(1, model);
			ps.setString(2, manufacturer);
			ps.setString(3, type.getValue());
			ps.setInt(4, seats);
			ps.setString(5, status.getValue());

			int affectedRows = ps.executeUpdate();

			if (affectedRows == 0) {
				logger.error("Creating car failed, no rows affected.");
				throw new SQLException("Creating car failed, no rows affected.");
			}

			try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					car.setId(generatedKeys.getInt(1));
				} else {
					logger.error("Creating car failed, no ID obtained.");
					throw new SQLException("Creating car failed, no ID obtained.");
				}
			}
			return car;

		} catch (SQLException e) {
			logger.error("Couldn't insert car to database.", e);
			throw new SQLException("Couldn't insert car to database");
		} finally {
			database.close(connection, ps);
		}
	}

	public List<Car> getAllCars(Integer limit, Integer start) throws SQLException {
		List<Car> list = new ArrayList<>();

		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			connection = database.getConnection();
			if (limit == null) {
				st = connection.createStatement();
				rs = st.executeQuery(FIND_ALL_CARS);				
			}
			else {
				ps = connection.prepareStatement(FIND_ALL_CARS + " LIMIT ?, ?");
				ps.setInt(1, start);
				ps.setInt(2, limit);
				rs = ps.executeQuery();
			}
			while (rs.next()) {
				list.add(getCar(rs));
			}
		} catch (SQLException e) {
			logger.error("Was unnable  to get cars from db.", e);
			throw new SQLException("Was unnable  to get cars from db");
		} finally {
			database.close(connection, st, rs);
			database.close(ps);
		}
		return list;
	}
	
	public List<Car> getAllCars() throws SQLException {
		return this.getAllCars(null, null);
	}
	
	public List<Car> getAllCarsForDriver(Integer limit, Integer start) throws SQLException {
		List<Car> list = new ArrayList<>();

		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			connection = database.getConnection();
			if (limit == null) {
				st = connection.createStatement();
				rs = st.executeQuery(FIND_CARS_FOR_DRIVER);				
			}
			else {
				ps = connection.prepareStatement(FIND_CARS_FOR_DRIVER);
				ps.setInt(1, start);
				ps.setInt(2, limit);
				rs = ps.executeQuery();
			}
			while (rs.next()) {
				list.add(getCar(rs));
			}
		} catch (SQLException e) {
			logger.error("Was unnable  to get cars from db.", e);
			throw new SQLException("Was unnable  to get cars from db");
		} finally {
			database.close(connection, st, rs);
			database.close(ps);
		}
		return list;
	}

	public Car getCar(int id) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Car car = new Car();

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(FIND_CAR_BY_ID);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				car.setId(rs.getInt("id"));
				car.setManufacturer(rs.getString("manufacturer"));
				car.setModel(rs.getString("model"));
				car.setType(CarType.findByValue(rs.getString("type")));
				car.setSeats(rs.getInt("seats"));
				car.setStatus(CarStatus.findByValue(rs.getString("status")));
			}
			return car;
		} catch (SQLException e) {
			logger.error("Couldn't get car by id.", e);
			throw new SQLException("Couldn't get car by id");
		} finally {
			database.close(connection, ps, rs);
		}
	}

	public Car getCar(ResultSet rs) throws SQLException {
		Car car = new Car();

		car.setId(rs.getInt("id"));
		car.setManufacturer(rs.getString("manufacturer"));
		car.setModel(rs.getString("model"));
		car.setType(CarType.findByValue(rs.getString("type")));
		car.setSeats(rs.getInt("seats"));
		car.setStatus(CarStatus.findByValue(rs.getString("status")));

		return car;
	}

	public void updateCar(Car car) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(UPDATE_CAR);
			ps.setString(1, car.getModel());
			ps.setString(2, car.getManufacturer());
			try {
				ps.setString(3, car.getType().getValue());
			} catch (NullPointerException e) {
				ps.setString(3, null);
			}
			try {
				ps.setInt(4, car.getSeats());
			} catch (NullPointerException e) {
				ps.setString(4, null);
			}
			try {
				ps.setString(5, car.getStatus().getValue());
			} catch (NullPointerException e) {
				ps.setString(5, null);
			}
			try {
				ps.setInt(6, car.getId());				
			}
			catch (NullPointerException e) {
				ps.setString(6, null);
			}

			ps.executeUpdate();
		} catch (SQLException e) {
			logger.error("Was unnable to update car.\n", e);
			throw new SQLException("Was unnable to update car" + e);
		} finally {
			database.close(connection, ps);
		}
	}

	public boolean deleteCar(int id) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;

		boolean flag = false;

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(DELETE_CAR);
			ps.setInt(1, id);
			ps.executeUpdate();
			flag = true;
			return flag;
		} catch (SQLException e) {
			logger.error("Was unnable to delete car.\n", e);
			throw new SQLException("Was unnable to delete car.\n");
		} finally {
			database.close(connection, ps);
		}
	}

	public List<Car> findAppropriateCar(int seats, CarType type) throws SQLException {
		List<Car> list = new ArrayList<>();

		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(FIND_APPROPRIATE_CAR);
			ps.setInt(1, seats);
			ps.setString(2, type.getValue());
			rs = ps.executeQuery();

			while (rs.next()) {
				list.add(getCar(rs));
			}

			database.close(ps);
			if (list.isEmpty()) {
				ps = connection.prepareStatement(FIND_APPROPRIATE_CARS);
				ps.setInt(1, seats);
				rs = ps.executeQuery();
				List<Car> helpList = new ArrayList<>();

				while (rs.next()) {
					helpList.add(getCar(rs));
				}
				int completed = 0;
				for (Car car: helpList) {
					if (completed < seats) {
						list.add(car);
						completed += car.getSeats();
					}
				}
			}

		} catch (SQLException e) {
			logger.error("Was unnable  to get car(s) for user from db.", e);
			throw new SQLException("Was unnable  to get car(s) for user from db.");
		} finally {
			database.close(connection, ps, rs);
		}
		return list;
	}
}