package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.bean.CarUsage;
import database.bean.UsageStatus;
import database.entity.car.Car;
import database.entity.car.CarStatus;

public class CarUsageDAO {
	private static final Logger logger = LogManager.getLogger(CarUsageDAO.class);

	private static CarUsageDAO dbm;
	private static DBManager database;

	private CarUsageDAO() {
	}

	public static synchronized CarUsageDAO getInstance(DBManager db) {
		Connection connection = null;
		Statement st = null;

		if (dbm == null) {
			dbm = new CarUsageDAO();
		}
		database = db;

		try {
			connection = database.getConnection();
			st = connection.createStatement();
			st.executeUpdate(INIT_TABLE);
		} catch (SQLException e) {
			logger.fatal("Couldn't get connetion with database.", e);
			System.exit(1);
		} finally {
			database.close(connection, st);
		}

		return dbm;
	}

	public static final String INIT_TABLE = "CREATE TABLE IF NOT EXISTS car_usage ("
			+ "`id` BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT, "
			+ "`status` ENUM('available', 'without driver') DEFAULT 'without driver', " + "`driver_id` INT NOT NULL, "
			+ "`car_id` INT NOT NULL, " + "`at` DATETIME, "
			+ "FOREIGN KEY (`driver_id`) REFERENCES users (`id`) ON DELETE CASCADE, "
			+ "FOREIGN KEY (`car_id`) REFERENCES cars (`id`) ON DELETE CASCADE)";

	public static final String CREATE_CAR_USAGE = "INSERT INTO car_usage (`status`, `driver_id`, `car_id`, `at`) VALUES (? , ?, ?, ?)";
	public static final String UPDATE_CAR_STATUS = "UPDATE cars SET `status` = ? WHERE `id` = ?";
	public static final String GET_CAR_USAGE_STATISTICS = "SELECT * FROM car_usage";
	public static final String GET_DRIVER_ID = "SELECT `driver_id` FROM car_usage WHERE `car_id` = ? AND `status` = 'available' ORDER BY `id` DESC LIMIT 1";
	public static final String COUNT_CAR_USAGE = "SELECT COUNT(*) FROM `car_usage`";
	public static final String GET_LAST_CAR_USAGE_BY_CAR_ID = "SELECT * FROM `car_usage` WHERE `car_id` = ? ORDER BY `id` DESC LIMIT 1";
	public static final String GET_LAST_CAR_USAGE_BY_DRIVER_ID = "SELECT * FROM `car_usage` WHERE `driver_id` = ? ORDER BY `id` DESC LIMIT 1";
	
	/**
	 * When sending information to database transaction has to be executed: firstly
	 * - sendint data to car_usage table, secondly - updating data in cars.status
	 * table. If one of operation fails - then rollback of transaction is to be
	 * performed
	 * 
	 * At is a LocalDateTime object. When sending time to database - it has to be
	 * sent like: DateTimeFormatter formatter =
	 * DateTimeFormatter.ofPattern("yyy-MM-dd HH:mm:ss"); LocalDateTime at =
	 * LocalDateTime.now(); String toSQL = time.format(formatter);
	 * 
	 * @see database.entity.CarUsage
	 * @return true if the data is updated
	 * @throws SQLException
	 */

	public Integer getCountCarUsage() throws SQLException {
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		Integer i = null;

		try {
			connection = database.getConnection();
			st = connection.createStatement();
			rs = st.executeQuery(COUNT_CAR_USAGE);

			while (rs.next()) {
				i = rs.getInt(1);
			}
		} catch (SQLException e) {
			logger.error("Couldn't get count of orders.", e);
			throw new SQLException("Couldn't get count of orders.");
		} finally {
			database.close(connection, st, rs);
		}
		return i;
	}

	public boolean createCarUsage(UsageStatus status, int driverId, int carId) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		CarDAO carDAO = CarDAO.getInstance(database);

		boolean flag = false;

		try {
			Car car = null;
			connection = database.getConnection();
			connection.setAutoCommit(false);

			ps = connection.prepareStatement(CarDAO.FIND_CAR_BY_ID + " FOR UPDATE");
			ps.setInt(1, carId);
			rs = ps.executeQuery();

			while (rs.next()) {
				car = carDAO.getCar(rs);
			}

			if (car == null) {
				logger.warn("Can't create car usage on nonexistent car");
				return false;
			}

			if (car.getStatus().toString().equals(status.toString())) {
				logger.warn("Can't set the same status on car");
				return false;
			}

			if (car.getStatus() == CarStatus.IN_USAGE && status == UsageStatus.WITHOUT_DRIVER) {
				logger.warn("Can't leave a car while the order is in process");
				return false;
			}

			database.close(rs);
			database.close(ps);

			ps = connection.prepareStatement(CREATE_CAR_USAGE);
			ps.setString(1, status.getValue());
			ps.setInt(2, driverId);
			ps.setInt(3, carId);
			ps.setString(4, LocalDateTime.now().format(CarUsage.formatter));
			ps.executeUpdate();

			database.close(ps);

			ps = connection.prepareStatement(UPDATE_CAR_STATUS);
			ps.setString(1, status.getValue());
			ps.setInt(2, carId);
			ps.executeUpdate();

			connection.commit();
			flag = true;
			return flag;
		} catch (SQLException e) {
			logger.error("Was unnable to update car usage", e);
			database.rollback(connection);
			throw new SQLException("Was unnable to update car usage");
		} finally {
			database.close(rs);
			database.close(connection, ps);
		}
	}

	public List<CarUsage> getCarUsageStatistics(Integer limit, Integer start) throws SQLException {
		List<CarUsage> list = new ArrayList<>();

		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			connection = database.getConnection();
			if (limit == null) {
				st = connection.createStatement();
				rs = st.executeQuery(GET_CAR_USAGE_STATISTICS);
			} else {
				ps = connection.prepareStatement(GET_CAR_USAGE_STATISTICS + " LIMIT ?, ?");
				ps.setInt(1, start);
				ps.setInt(2, limit);
				rs = ps.executeQuery();
			}

			while (rs.next()) {
				CarUsage carUsage = new CarUsage();

				carUsage.setId(rs.getInt("id"));
				carUsage.setStatus(UsageStatus.findByValue(rs.getString("status")));
				carUsage.setDriverId(rs.getInt("driver_id"));
				carUsage.setCarId(rs.getInt("car_id"));
				carUsage.setAt(LocalDateTime.parse(rs.getString("at"), CarUsage.formatter));

				list.add(carUsage);
			}
		} catch (SQLException e) {
			logger.error("Was unnable  to get car usage statistics from db.", e);
			throw new SQLException("Was unnable  to get car usage statistics from db.");
		} finally {
			database.close(connection, st, rs);
			database.close(ps);
		}
		return list;
	}

	public List<CarUsage> getCarUsageStatistics() throws SQLException {
		return getCarUsageStatistics(null, null);
	}

	public Integer getDriverIdByCarId(Integer carId) throws SQLException {

		Connection connection = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		Integer result = null;

		try {
			connection = database.getConnection();

			ps = connection.prepareStatement(GET_DRIVER_ID);
			ps.setInt(1, carId);
			rs = ps.executeQuery();

			while (rs.next()) {
				result = rs.getInt(1);
			}
		} catch (SQLException e) {
			logger.error("Was unnable  to get car usage statistics from db.", e);
			throw new SQLException("Was unnable  to get car usage statistics from db.");
		} finally {
			database.close(connection, ps, rs);
		}
		return result;
	}

	public CarUsage getLastCarUsageByCarId(Integer carId) throws SQLException {
		Connection connection = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		CarUsage carUsage = new CarUsage();

		try {
			connection = database.getConnection();

			ps = connection.prepareStatement(GET_LAST_CAR_USAGE_BY_CAR_ID);
			ps.setInt(1, carId);
			rs = ps.executeQuery();

			while (rs.next()) {
				carUsage.setId(rs.getInt("id"));
				carUsage.setStatus(UsageStatus.findByValue(rs.getString("status")));
				carUsage.setDriverId(rs.getInt("driver_id"));
				carUsage.setCarId(rs.getInt("car_id"));
				carUsage.setAt(LocalDateTime.parse(rs.getString("at"), CarUsage.formatter));
			}
			if (carUsage.getStatus() != null) {
				return carUsage;
			} else {
				return null;
			}

		} catch (SQLException e) {
			logger.error("Was unnable  to get last car usage by car id from db.", e);
			throw new SQLException("Was unnable  to get last car usage by car id from db.");
		} finally {
			database.close(connection, ps, rs);
		}
	}
	
	public CarUsage getLastCarUsageByDriverId(Integer driverId) throws SQLException {
		Connection connection = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		CarUsage carUsage = new CarUsage();

		try {
			connection = database.getConnection();

			ps = connection.prepareStatement(GET_LAST_CAR_USAGE_BY_DRIVER_ID);
			ps.setInt(1, driverId);
			rs = ps.executeQuery();

			while (rs.next()) {
				carUsage.setId(rs.getInt("id"));
				carUsage.setStatus(UsageStatus.findByValue(rs.getString("status")));
				carUsage.setDriverId(rs.getInt("driver_id"));
				carUsage.setCarId(rs.getInt("car_id"));
				carUsage.setAt(LocalDateTime.parse(rs.getString("at"), CarUsage.formatter));
			}
			if (carUsage.getStatus() != null) {
				return carUsage;
			} else {
				return null;
			}

		} catch (SQLException e) {
			logger.error("Was unnable  to get last car usage by car id from db.", e);
			throw new SQLException("Was unnable  to get last car usage by car id from db.");
		} finally {
			database.close(connection, ps, rs);
		}
	}
}