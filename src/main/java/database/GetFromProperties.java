package database;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GetFromProperties {

	private static final Logger logger = LogManager.getLogger(GetFromProperties.class);

	private GetFromProperties() {
	}

	public static String getPropValue(String property) {
		String os = System.getProperty("os.name");

		String path;

		if (os.startsWith("Windows")) {
			path = new File("").getAbsolutePath() + "//app.properties";
		} else {
			path = "/etc/FinalProject/app.properties";
		}

		try (InputStream input = new FileInputStream(path)) {
			Properties prop = new Properties();
			prop.load(input);
			return prop.getProperty(property);

		} catch (IOException e) {
			logger.error("Couldn't find property file at {}.\n", path, e);
			return null;
		}
	}
}