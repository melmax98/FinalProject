package database;

import java.sql.Connection;
import java.sql.SQLException;

public interface GetConnection {
	Connection getConnection() throws SQLException;
}