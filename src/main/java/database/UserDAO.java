package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.entity.user.User;
import database.entity.user.UserType;

public class UserDAO {
	private static final Logger logger = LogManager.getLogger(UserDAO.class);

	private static UserDAO dbm;
	private static DBManager database;

	private UserDAO() {
	}

	public static synchronized UserDAO getInstance(DBManager db) {
		Connection connection = null;
		Statement st = null;

		if (dbm == null) {
			dbm = new UserDAO();
		}
		database = db;

		try {
			connection = database.getConnection();
			st = connection.createStatement();
			st.executeUpdate(INIT_TABLE);
		} catch (SQLException e) {
			logger.fatal("Couldn't get connetion with database.", e);
			System.exit(1);
		} finally {
			database.close(connection, st);
		}

		return dbm;
	}

	public static final String INIT_TABLE = "CREATE TABLE IF NOT EXISTS users ("
			+ "id INT PRIMARY KEY NOT NULL AUTO_INCREMENT," + "login VARCHAR(250) NOT NULL DEFAULT '' UNIQUE,"
			+ "password VARCHAR(32) NOT NULL DEFAULT '', role ENUM('client', 'administrator', 'driver'))";

	public static final String INSERT_USER = "INSERT INTO users(login, password, role) VALUES(?, MD5(?), ?)";
	public static final String FIND_ALL_USERS = "SELECT * FROM users ORDER BY id";
	public static final String FIND_USER_BY_ID = "SELECT * FROM users WHERE id = ?";
	public static final String UPDATE_USER = "UPDATE users SET login = IFNULL(?, login), password = IFNULL(MD5(?), password), role = IFNULL( ? , role) WHERE id = ?";
	public static final String DELETE_USER = "DELETE FROM users WHERE id = ?";
	public static final String AUTH_USER = "SELECT * FROM users WHERE login = ? AND password = MD5(?)";
	public static final String IS_ANY_ADMIN = "SELECT 1 FROM users WHERE role = \"administrator\" HAVING COUNT(*) > 0 LIMIT 1";
	public static final String COUNT_USERS = "SELECT COUNT(*) FROM `users`";

	/**
	 * @return
	 * @throws SQLException
	 */
	public boolean isAnyAdmin() throws SQLException {
		Connection connection = null;
		Statement s = null;
		ResultSet rs = null;
		try {
			connection = database.getConnection();
			s = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = s.executeQuery(IS_ANY_ADMIN);

			rs.last();
			int rowNumber = rs.getRow();
			return rowNumber > 0;
		} catch (SQLException e) {
			logger.error("Was unnable to communicate with database.\n", e);
			throw new SQLException("Was unnable to communicate with database");
		} finally {
			database.close(connection, s, rs);
		}

	}

	public User createUser(String login, String password, UserType role) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
			User user = new User();
			user.setLogin(login);
			user.setPassword(password);
			user.setRole(role);

			ps.setString(1, login);
			ps.setString(2, password);
			ps.setString(3, role.getValue());

			int affectedRows = ps.executeUpdate();

			if (affectedRows == 0) {
				logger.error("Creating user failed, no rows affected.");
				throw new SQLException("Creating user failed, no rows affected.");
			}

			try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					user.setId(generatedKeys.getInt(1));
				} else {
					logger.error("Creating user failed, no ID obtained.");
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			}
			return user;

		} catch (SQLException e) {
			logger.error("Couldn't insert user to database.", e);
			throw new SQLException("Couldn't insert user to database");
		} finally {
			database.close(connection, ps);
		}
	}

	public List<User> getAllUsers(Integer limit, Integer start) throws SQLException {
		List<User> list = new ArrayList<>();

		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			connection = database.getConnection();
			if (limit == null) {
				st = connection.createStatement();				
				rs = st.executeQuery(FIND_ALL_USERS);
			}
			else {
				ps = connection.prepareStatement(FIND_ALL_USERS + " LIMIT ?, ?");
				ps.setInt(1, start);
				ps.setInt(2, limit);
				rs = ps.executeQuery();
			}
			while (rs.next()) {
				list.add(getUser(rs));
			}
		} catch (SQLException e) {
			logger.error("Was unnable  to get users from db.", e);
			throw new SQLException("Was unnable  to get users from db");
		} finally {
			database.close(connection, st, rs);
			database.close(ps);
		}
		return list;
	}
	
	public List<User> getAllUsers() throws SQLException {
		return this.getAllUsers(null, null);
	}

	public User getUser(int id) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = new User();

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(FIND_USER_BY_ID);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				user.setRole(UserType.findByValue(rs.getString("role")));
			}
			return user;
		} catch (SQLException e) {
			logger.error("Couldn't get user by id.", e);
			throw new SQLException("Couldn't get user by id");
		} finally {
			database.close(connection, ps, rs);
		}
	}

	public User authUser(String login, String password) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = new User();

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(AUTH_USER);
			ps.setString(1, login);
			ps.setString(2, password);
			rs = ps.executeQuery();
			boolean isFound = false;
			while (rs.next()) {
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				user.setRole(UserType.findByValue(rs.getString("role")));
				isFound = true;
			}
			if (isFound) {
				return user;
			} else {
				logger.error("User not found");
				throw new SQLException("User not found");
			}
		} catch (SQLException e) {
			logger.error("Was unnable to auth user.\n", e);
			throw new SQLException("Was unnable to auth user");
		} finally {
			database.close(connection, ps, rs);
		}
	}

	public User getUser(ResultSet rs) throws SQLException {
		User user = new User();

		user.setId(rs.getInt("id"));
		user.setLogin(rs.getString("login"));
		user.setRole(UserType.findByValue(rs.getString("role")));

		return user;
	}

	public void updateUser(User user) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(UPDATE_USER);
			ps.setString(1, user.getLogin());
			ps.setString(2, user.getPassword());
			try {
				ps.setString(3, user.getRole().getValue());
			} catch (NullPointerException e) {
				ps.setString(3, null);
			}
			ps.setInt(4, user.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			logger.error("Was unnable to update user.\n", e);
			throw new SQLException("Was unnable to update user" + e);
		} finally {
			database.close(connection, ps);
		}
	}

	public boolean deleteUser(int id) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;

		boolean flag = false;

		try {
			connection = database.getConnection();
			ps = connection.prepareStatement(DELETE_USER);
			ps.setInt(1, id);
			ps.executeUpdate();
			flag = true;
			return flag;
		} catch (SQLException e) {
			logger.error("Was unnable to delete car.\n", e);
			throw new SQLException("Was unnable to delete user.\n");
		} finally {
			database.close(connection, ps);
		}
	}

	public Integer getCountUsers() throws SQLException {
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		Integer i = null;
		
		try {
			connection = database.getConnection();
			st = connection.createStatement();
			rs = st.executeQuery(COUNT_USERS);
			
			while (rs.next()) {
				i = rs.getInt(1);
			}
		}
		catch(SQLException e) {
			logger.error("Couldn't get count of users.", e);
			throw new SQLException("Couldn't get count of users.");
		}
		finally {
			database.close(connection, st, rs);
		}
		return i;
	}
}
