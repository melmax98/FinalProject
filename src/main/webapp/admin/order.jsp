<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@page import="database.entity.order.Order"%> 
    <%@ page import="java.time.format.DateTimeFormatter" %>
    <%@ taglib uri="/CopyrightTag.tld" prefix="my" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
 <fmt:setLocale value="${language}" />
 <fmt:setBundle basename="messages" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "https://www.w3.org/TR/html4/loose.dtd">
<html>
<head> 
<link rel="icon" type="image/png" href="/favicon.png"/> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="admin.edit-order"/></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/admin/index"><fmt:message key="admin.edit-order"/></a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="/logout"><fmt:message key="logout"/> <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="/admin/create-car"><fmt:message key="admin.create-car"/></a>
      <a class="nav-item nav-link" href="/admin/create-car-usage"><fmt:message key="admin.create-car-usage"/></a>
      <a class="nav-item nav-link" href="/admin/car-usage-statistics"><fmt:message key="admin.car-usage-statistics"/></a>
      <a class="nav-item nav-link" href="/admin/cars"><fmt:message key="admin.cars"/></a>
      <a class="nav-item nav-link" href="/admin/edit-car"><fmt:message key="admin.edit-car"/></a>
      <a class="nav-item nav-link" href="/admin/orders?page=1&sort=4"><fmt:message key="admin.orders"/></a>
      <a class="nav-item nav-link" href="/admin/edit-order"><fmt:message key="admin.edit-order"/></a>
      <a class="nav-item nav-link" href="/admin/users"><fmt:message key="admin.users"/></a>
      <a class="nav-item nav-link" href="/admin/edit-user"><fmt:message key="admin.edit-user"/></a>
    </div>
  </div>
  <form>
<select class="form-control form-control-sm" id="language" name="language" onchange="submit()">
  <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
  <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
</select>
</form>
</nav>
<br>

<div class="view-order">
  <fmt:message key="order.id" var="placeholderValueOrderId"/>
<form action="/admin/order">
        <br />
        <input
          class="form-control mr-sm-2"
          placeholder="${placeholderValueOrderId}"
          type="text"
          name="id"
        />
        <br />
      <button type="submit" class="btn btn-primary"><fmt:message key="admin.find-an-order"/></button>
</form>
</div>
<br>
<div class="change-order">
    <form>
<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col"><fmt:message key="driver.car_id"/></th>
      <th scope="col"><fmt:message key="driver.id"/></th>
      <th scope="col"><fmt:message key="user.id"/></th>
      <th scope="col"><fmt:message key="from"/></th>
      <th scope="col"><fmt:message key="to"/></th>
      <th scope="col"><fmt:message key="status"/></th>
      <th scope="col"><fmt:message key="car_type"/></th>
      <th scope="col"><fmt:message key="created_at"/></th>
      <th scope="col"><fmt:message key="finished_at"/></th>
      <th scope="col"><fmt:message key="price"/></th>
      <th scope="col"><fmt:message key="passengers"/></th>
      <th scope="col"><fmt:message key="wait_time"/></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="col"><c:out value="${order.getId()}" /></th>
      <th scope="col"><c:out value="${order.getCarId()}" /></th>
      <th scope="col"><c:out value="${order.getDriverId()}" /></th>
      <th scope="col"><c:out value="${order.getUserId()}" /></th>
      <th scope="col"><c:out value="${order.getFrom()}" /></th>
      <th scope="col"><c:out value="${order.getTo()}" /></th>
      <th scope="col"><fieldset class="form-group">
    <div class="row">
      <div class="col-sm-10">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="status" value="CREATED">
          <label class="form-check-label" for="status">
            <fmt:message key="created"/>
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="status" value="DRIVER_ON_THE_WAY">
          <label class="form-check-label" for="status">
            <fmt:message key="driver_on_the_way"/>
          </label>
        </div>
         <div class="form-check">
          <input class="form-check-input" type="radio" name="status" value="IN_PROGRESS">
          <label class="form-check-label" for="status">
            <fmt:message key="in_progress"/>
          </label>
        </div>
                <div class="form-check">
          <input class="form-check-input" type="radio" name="status" value="DONE" checked>
          <label class="form-check-label" for="status">
            <fmt:message key="done"/>
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="status" value="CANCELLED">
          <label class="form-check-label" for="status">
            <fmt:message key="cancelled"/>
          </label>
        </div>
      </div>
    </div>
  </fieldset></th>
      <th scope="col"><fmt:message key="orders.carType.${order.getCarType()}" /></th>
      <th scope="col"><c:out value="${order.getCreatedAt().format(DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm:ss'))}" /></th>
      <th scope="col"><c:out value="${order.getFinishedAt().format(DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm:ss'))}" /></th>
      <th scope="col"><c:out value="${order.getPrice()}" /></th>
      <th scope="col"><c:out value="${order.getPassengers()}" /></th>
      <th scope="col"><c:out value="${order.getWaitTime()}" /></th>
    </tr>
  </tbody>
</table>
<br>

<c:if test="${not empty order.getId()}">
    <div class="edit">
        <form action="api/order/edit">
            <button type="submit" class="btn btn-primary"><fmt:message key="admin.edit-order"/></button>
        </form>
    </div>
</c:if>
</form>

</div>
<footer>
  <my:CopyrightTag />
</footer>
</div>
  <script src="/dist/bundle.js"></script>
</body>
</html>