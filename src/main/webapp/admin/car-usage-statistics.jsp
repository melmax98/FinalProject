<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
 <%@ page import="java.time.format.DateTimeFormatter" %>
 <%@ taglib uri="/CopyrightTag.tld" prefix="my" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
 <fmt:setLocale value="${language}" />
 <fmt:setBundle basename="messages" />
   

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "https://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" type="image/png" href="/favicon.png"/>  
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="admin.car-usage-statistics" /></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/admin/index"><fmt:message key="admin.car-usage-statistics" /></a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="/logout"><fmt:message key="logout" /> <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="/admin/create-car"><fmt:message key="admin.create-car" /></a>
      <a class="nav-item nav-link" href="/admin/create-car-usage"><fmt:message key="admin.create-car-usage" /></a>
      <a class="nav-item nav-link" href="/admin/car-usage-statistics"><fmt:message key="admin.car-usage-statistics" /></a>
      <a class="nav-item nav-link" href="/admin/cars"><fmt:message key="admin.cars" /></a>
      <a class="nav-item nav-link" href="/admin/edit-car"><fmt:message key="admin.edit-car"/></a>
      <a class="nav-item nav-link" href="/admin/orders?page=1&sort=4"><fmt:message key="admin.orders" /></a>
      <a class="nav-item nav-link" href="/admin/edit-order"><fmt:message key="admin.edit-order" /></a>
      <a class="nav-item nav-link" href="/admin/users"><fmt:message key="admin.users"/></a>
      <a class="nav-item nav-link" href="/admin/edit-user"><fmt:message key="admin.edit-user"/></a>
    </div>
  </div>
   <form>
<select class="form-control form-control-sm" id="language" name="language" onchange="submit()">
  <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
  <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
</select>
</form>
</nav>
<br>

<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col"><fmt:message key="status" /></th>
      <th scope="col"><fmt:message key="driver.id" /></th>
      <th scope="col"><fmt:message key="driver.car_id" /></th>
      <th scope="col"><fmt:message key="status_change_time" /></th>
    </tr>
  </thead>
   <c:forEach items="${carUsage}" var="carUsage">
  <tbody>
    <tr>
      <th scope="col"><c:out value="${carUsage.getId()}" /></th>
      <th scope="col"><fmt:message key="car-usage-statistics.status.${carUsage.getStatus()}" /></th>
      <th scope="col"><c:out value="${carUsage.getDriverId()}" /></th>
      <th scope="col"><c:out value="${carUsage.getCarId()}" /></th>
      <th scope="col"><c:out value="${carUsage.getAt().format(DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm:ss'))}" /></th>
    </tr>
  </tbody>
  </c:forEach>
</table>

<nav>
  <ul class="pagination">
      <c:if test="${page > 1}">
     <li class="page-item">
      <a class="page-link" href="/admin/car-usage-statistics?page=${page - 1}" tabindex="-1"><fmt:message key="previous" /></a>
    </li>
 </c:if>
 <c:if test= "${page <= 1}">
    <li class="page-item disabled">
      <a class="page-link" tabindex="-1"><fmt:message key="previous" /></a>
    </li>
  </c:if>

      <c:forEach begin='1' end="${pages}" var="p"> 
   
      <c:if test="${p == page}">
      <li class="page-item active">
      <a class="page-link" href="/admin/car-usage-statistics?page=${p}">${p}</a>
    </li>
   </c:if>
   <c:if test="${p != page}">
    <li class="page-item"><a class="page-link" href="/admin/car-usage-statistics?page=${p}">${p}</a></li>
  </c:if>
    </c:forEach>
    
      <c:if test="${page < pages}">
     <li class="page-item">
      <a class="page-link" href="/admin/car-usage-statistics?page=${page + 1}" tabindex="-1"><fmt:message key="next" /></a>
    </li>
    </c:if>
    <c:if test="${page >= pages}">
     <li class="page-item disabled">
      <a class="page-link" tabindex="-1"><fmt:message key="next" /></a>
    </li>
  </c:if>
   
  </ul>
</nav>
<footer>
  <my:CopyrightTag />
</footer>
</div>

  <script src="/dist/bundle.js"></script>
</body>
</html>