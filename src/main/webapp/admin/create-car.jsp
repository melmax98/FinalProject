<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/CopyrightTag.tld" prefix="my" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
 <fmt:setLocale value="${language}" />
 <fmt:setBundle basename="messages" />

<html>
<head>  
<link rel="icon" type="image/png" href="/favicon.png"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="admin.create-car" /></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/admin/index"><fmt:message key="admin.create-car" /></a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
     <a class="nav-item nav-link active" href="/logout"><fmt:message key="logout" /> <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="/admin/create-car"><fmt:message key="admin.create-car" /></a>
      <a class="nav-item nav-link" href="/admin/create-car-usage"><fmt:message key="admin.create-car-usage" /></a>
      <a class="nav-item nav-link" href="/admin/car-usage-statistics"><fmt:message key="admin.car-usage-statistics" /></a>
      <a class="nav-item nav-link" href="/admin/cars"><fmt:message key="admin.cars" /></a>
      <a class="nav-item nav-link" href="/admin/edit-car"><fmt:message key="admin.edit-car"/></a>
      <a class="nav-item nav-link" href="/admin/orders?page=1&sort=4"><fmt:message key="admin.orders" /></a>
      <a class="nav-item nav-link" href="/admin/edit-order"><fmt:message key="admin.edit-order" /></a>
      <a class="nav-item nav-link" href="/admin/users"><fmt:message key="admin.users"/></a>
      <a class="nav-item nav-link" href="/admin/edit-user"><fmt:message key="admin.edit-user"/></a>
    </div>
  </div>
  <form>
<select class="form-control form-control-sm" id="language" name="language" onchange="submit()">
  <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
  <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
</select>
</form>
</nav>
</br>
<div class="create-car">
<fmt:message key="manufacturer" var="placeholderValueManufacturer" />
<fmt:message key="model" var="placeholderValueModel" />
<fmt:message key="seats" var="placeholderValueSeats" />

<form action="/api/car">
   <input class="form-control mr-sm-2" placeholder="${placeholderValueManufacturer}" name="manufacturer" />
        <br />
        <input
          class="form-control mr-sm-2"
          placeholder="${placeholderValueModel}"
          name="model"
        />
        <br />
      <fieldset class="form-group">
    <div class="row">
      <legend class="col-form-label col-sm-2 pt-0"><fmt:message key="type"/></legend>
      <div class="col-sm-10">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="type" value="BASIC" checked>
          <label class="form-check-label" for="type">
            <fmt:message key="basic"/>
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="type" value="COMFORT">
          <label class="form-check-label" for="type">
            <fmt:message key="comfort"/>
          </label>
        </div>
         <div class="form-check">
          <input class="form-check-input" type="radio" name="type" value="VAN">
          <label class="form-check-label" for="type">
            <fmt:message key="van"/>
          </label>
        </div>
      </div>
    </div>
  </fieldset>
  <input class="form-control mr-sm-2" placeholder="${placeholderValueSeats}" name="seats" />
  <input value="WITHOUT_DRIVER" type="hidden" name="status" />
    </br>
      <button type="submit" class="btn btn-primary"><fmt:message key="admin.create-car" /></button>
    </div>
</form>
<form action="/admin/index">
    <button type="submit" class="btn btn-primary" action="/admin/index"><fmt:message key="back.button" /></button>
</form>
</div>
<footer>
  <my:CopyrightTag />
</footer>
</div>
  <script src="/dist/bundle.js"></script>
</body>
</html>