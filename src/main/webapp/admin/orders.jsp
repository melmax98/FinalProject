<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ taglib uri="/CopyrightTag.tld" prefix="my" %>
<%@ page import="java.util.*, database.OrderDAO, database.entity.order.Order, database.DBManager" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
  value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
  scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "https://www.w3.org/TR/html4/loose.dtd">
<html>


  <head>
    <link rel="icon" type="image/png" href="/favicon.png"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>
      <fmt:message key="admin.orders" />
    </title>
  </head>

  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="/admin/index">
        <fmt:message key="admin.orders" /></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-item nav-link active" href="/logout">
            <fmt:message key="logout" /> <span class="sr-only">(current)</span></a>
          <a class="nav-item nav-link" href="/admin/create-car">
            <fmt:message key="admin.create-car" /></a>
          <a class="nav-item nav-link" href="/admin/create-car-usage">
            <fmt:message key="admin.create-car-usage" /></a>
          <a class="nav-item nav-link" href="/admin/car-usage-statistics">
            <fmt:message key="admin.car-usage-statistics" /></a>
          <a class="nav-item nav-link" href="/admin/cars">
            <fmt:message key="admin.cars" /></a>
          <a class="nav-item nav-link" href="/admin/edit-car">
            <fmt:message key="admin.edit-car" /></a>
          <a class="nav-item nav-link" href="/admin/orders?page=1&sort=4">
            <fmt:message key="admin.orders" /></a>
          <a class="nav-item nav-link" href="/admin/edit-order">
            <fmt:message key="admin.edit-order" /></a>
          <a class="nav-item nav-link" href="/admin/users">
            <fmt:message key="admin.users"/></a>
          <a class="nav-item nav-link" href="/admin/edit-user">
            <fmt:message key="admin.edit-user"/></a>
        </div>
      </div>
      <form>
        <select class="form-control form-control-sm" id="language" name="language" onchange="submit()">
          <option value="en" ${language=='en' ? 'selected' : '' }>English</option>
          <option value="ru" ${language=='ru' ? 'selected' : '' }>Русский</option>
        </select>
      </form>
    </nav>
    <br>
    <div class="container">
      <div class="sort-orders">
        <a href="/admin/orders?sort=1" data-append-query class="btn btn-secondary">
          <fmt:message key="sort_by_price_up" /></a>
        <a href="/admin/orders?sort=2" data-append-query class="btn btn-secondary">
          <fmt:message key="sort_by_price_down" /></a>
        <a href="/admin/orders?sort=3" data-append-query class="btn btn-secondary">
          <fmt:message key="sort_by_data_up" /></a>
        <a href="/admin/orders?sort=4" data-append-query class="btn btn-secondary">
          <fmt:message key="sort_by_data_down" /></a>
      </div>
      <br>
    </div>
    <br>
    <div class="filter-orders container">
      <form action="/admin/orders" class="row mt-2">
        <fmt:message key="user.id" var="userId" />
        <input class="form-control col-2" placeholder="${userId}" type="text" name="id" />
        <input type="hidden" name="sort" value="${sort}" />
        <input type="hidden" name="page" value="${page}" />
        <input type="hidden" name="filter" value="2" />
        <button type="submit" data-append-query class="btn btn-info ml-auto col-4">
          <fmt:message key="filter_by_user" /></button>
      </form>
      <form class="row mt-2 mb-2">
        <fmt:message key="date" var="date" />
        <input class="form-control mr-2 col-2" placeholder="${date}" type="text" name="date_from" />
        <input class="form-control mr-2 col-2" placeholder="${date}" type="text" name="date_to" />
        <input type="hidden" name="sort" value="${sort}" />
        <input type="hidden" name="page" value="${page}" />
        <input type="hidden" name="filter" value="1" />
        <button type="submit" href="/admin/orders?filter=1" class="btn btn-info ml-auto col-4" data-append-query>
          <fmt:message key="filter_by_date" /></button>
      </form>
    </div>

    <div class="container-fluid">
      <div class="row">
        <table class="table col-12 table-striped table-dark">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">
                <fmt:message key="driver.car_id" />
              </th>
              <th scope="col">
                <fmt:message key="driver.id" />
              </th>
              <th scope="col">
                <fmt:message key="user.id" />
              </th>
              <th scope="col">
                <fmt:message key="from" />
              </th>
              <th scope="col">
                <fmt:message key="to" />
              </th>
              <th scope="col">
                <fmt:message key="status" />
              </th>
              <th scope="col">
                <fmt:message key="car_type" />
              </th>
              <th scope="col">
                <fmt:message key="created_at" />
              </th>
              <th scope="col">
                <fmt:message key="finished_at" />
              </th>
              <th scope="col">
                <fmt:message key="price" />
              </th>
              <th scope="col">
                <fmt:message key="passengers" />
              </th>
              <th scope="col">
                <fmt:message key="wait_time" />
              </th>
            </tr>
          </thead>
          <c:forEach items="${orders}" var="order">
            <tbody>
              <tr>
                <th scope="col">
                  <c:out value="${order.getId()}" />
                </th>
                <th scope="col">
                  <c:out value="${order.getCarId()}" />
                </th>
                <th scope="col">
                  <c:out value="${order.getDriverId()}" />
                </th>
                <th scope="col">
                  <c:out value="${order.getUserId()}" />
                </th>
                <th scope="col">
                  <c:out value="${order.getFrom()}" />
                </th>
               <th scope="col">
                  <c:out value="${order.getTo()}" />
                </th>
                <th scope="col">
                  <fmt:message key="orders.status.${order.getStatus()}" />
                </th>
                <th scope="col">
                  <fmt:message key="orders.carType.${order.getCarType()}" />
                </th>
                <th scope="col">
                  <c:out value="${order.getCreatedAt().format(DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm:ss'))}" />
                </th>
                <th scope="col">
                  <c:out value="${order.getFinishedAt().format(DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm:ss'))}" />
                </th>
                <th scope="col">
                  <c:out value="${order.getPrice()}" />
                </th>
                <th scope="col">
                  <c:out value="${order.getPassengers()}" />
                </th>
                <th scope="col">
                  <c:out value="${order.getWaitTime()}" />
                </th>
              </tr>
            </tbody>
          </c:forEach>
        </table>
      </div>
    </div>

    <div class="container">
      <nav>
        <ul class="pagination justify-content-center">
          <c:if test="${page > 1}">
            <li class="page-item">
              <a class="page-link" href="/admin/orders?page=${page - 1}&sort=${sort}" data-append-query tabindex="-1">
                <fmt:message key="previous" /></a>
            </li>
          </c:if>
          <c:if test="${page <= 1}">
            <li class="page-item disabled">
              <a class="page-link" tabindex="-1">
                <fmt:message key="previous" /></a>
            </li>
          </c:if>

          <c:forEach begin='1' end="${pages}" var="p">

            <c:if test="${p == page}">
              <li class="page-item active">
                <a class="page-link" href="/admin/orders?page=${p}&sort=${sort}" data-append-query>${p}</a>
              </li>
            </c:if>
            <c:if test="${p != page}">
              <li class="page-item"><a class="page-link" href="/admin/orders?page=${p}&sort=${sort}"
                  data-append-query>${p}</a></li>
            </c:if>
          </c:forEach>

          <c:if test="${page < pages}">
            <li class="page-item">
              <a class="page-link" href="/admin/orders?page=${page + 1}&sort=${sort}" tabindex="-1" data-append-query>
                <fmt:message key="next" /></a>
            </li>
          </c:if>
          <c:if test="${page >= pages}">
            <li class="page-item disabled">
              <a class="page-link" tabindex="-1">
                <fmt:message key="next" /></a>
            </li>
          </c:if>

        </ul>
      </nav>
    </div>
    <footer>
      <my:CopyrightTag />
    </footer>
    </div>
    <script src="/dist/bundle.js"></script>
  </body>

</html>