<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/CopyrightTag.tld" prefix="my" %>
<%@page import="database.entity.car.Car"%> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
 <fmt:setLocale value="${language}" />
 <fmt:setBundle basename="messages" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "https://www.w3.org/TR/html4/loose.dtd">
<html>
<head>  
<link rel="icon" type="image/png" href="/favicon.png"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="role.client" /></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/admin/index"><fmt:message key="admin.edit-car" /></a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="/logout"><fmt:message key="logout" /> <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="/admin/create-car"><fmt:message key="admin.create-car" /></a>
      <a class="nav-item nav-link" href="/admin/create-car-usage"><fmt:message key="admin.create-car-usage" /></a>
      <a class="nav-item nav-link" href="/admin/car-usage-statistics"><fmt:message key="admin.car-usage-statistics" /></a>
      <a class="nav-item nav-link" href="/admin/cars"><fmt:message key="admin.cars" /></a>
      <a class="nav-item nav-link" href="/admin/edit-car"><fmt:message key="admin.edit-car" /></a>
      <a class="nav-item nav-link" href="/admin/orders?page=1&sort=4"><fmt:message key="admin.orders" /></a>
      <a class="nav-item nav-link" href="/admin/edit-order"><fmt:message key="admin.edit-order" /></a>
      <a class="nav-item nav-link" href="/admin/users"><fmt:message key="admin.users"/></a>
      <a class="nav-item nav-link" href="/admin/edit-user"><fmt:message key="admin.edit-user"/></a>
    </div>
  </div>
  <form>
<select class="form-control form-control-sm" id="language" name="language" onchange="submit()">
  <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
  <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
</select>
</form>
</nav>
<br>

<div class="view-user">
<form action="/admin/user">
  <fmt:message key="user.id" var="placeholderValue" />
        <br />
        <input
          class="form-control mr-sm-2"
          placeholder="${placeholderValue}"
          type="text"
          name="id"
        />
        <br />
      <button type="submit" class="btn btn-primary"><fmt:message key="admin.find-user"/></button>
</form>
</div>
<br>
<div class="change-user">
    <form>
<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col"><fmt:message key="user.login" /></th>
      <th scope="col"><fmt:message key="user.role" /></th>
    </tr>
  </thead>

  <tbody>
    <tr>
        <th scope="col"><c:out value="${user.getId()}" /></th>
        <th scope="col"><input
          class="form-control mr-sm-2"
          placeholder="${user.getLogin()}"
          type="text"
          name="login"
        /></th>
         <th scope="col"><fieldset class="form-group">
    <div class="row">
      <div class="col-sm-10">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="role" value="CLIENT" checked>
          <label class="form-check-label" for="role">
            <fmt:message key="users.userRole.CLIENT" />
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="role" value="DRIVER">
          <label class="form-check-label" for="role">
            <fmt:message key="users.userRole.DRIVER" />
          </label>
        </div>
         <div class="form-check">
          <input class="form-check-input" type="radio" name="role" value="ADMIN">
          <label class="form-check-label" for="role">
            <fmt:message key="users.userRole.ADMIN" />
          </label>
        </div>
      </div>
    </div>
  </fieldset></th>
    </tr>
  </tbody>
</table>
<br>

<c:if test="${not empty user.getId()}">
    <div class="edit">
        <form action="api/user/edit">
            <button type="submit" class="btn btn-primary"><fmt:message key="admin.edit-user" /></button>
        </form>
    </div>
</c:if>
</form>
</div>
<br>
<div class="delete-user">
<c:if test="${not empty user.getId()}">
        <form action="api/user/delete">
            <button type="submit" class="btn btn-danger"><fmt:message key="admin.delete-user" /></button>
        </form>
</c:if>
</div>
<footer>
  <my:CopyrightTag />
</footer>
</div>
  <script src="/dist/bundle.js"></script>
</body>
</html>