<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="/CopyrightTag.tld" prefix="my" %>
     <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
 <fmt:setLocale value="${language}" />
 <fmt:setBundle basename="messages" />

<html>

  <head>
    <link rel="icon" type="image/png" href="/favicon.png"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title><fmt:message key="registration" /></title>
  </head>

  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="/index.jsp"><fmt:message key="registration" /></a>
      <form class="ml-auto">
<select class="form-control form-control-sm" id="language" name="language" onchange="submit()">
  <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
  <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
</select>
</form>
    </nav>
    <div class="registration jumbotron">
      <form action="/api/user/register">
        <fmt:message key="login" var="placeholderValueLogin" />
      <fmt:message key="password" var="placeholderValuePassword" />
        <input class="form-control mr-sm-2" placeholder="${placeholderValueLogin}" name="login" />
        <br />
        <input class="form-control mr-sm-2" placeholder="${placeholderValuePassword}" type="password" name="password" />
        <br />
        <fieldset class="form-group">
          <div class="row">
            <legend class="col-form-label col-sm-2 pt-0"><fmt:message key="role" /></legend>
            <div class="col-sm-10">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="role" value="CLIENT" checked>
                <label class="form-check-label" for="role">
                  <fmt:message key="role.client" />
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="role" value="DRIVER">
                <label class="form-check-label" for="role">
                  <fmt:message key="role.driver" />
                </label>
              </div>
            </div>
          </div>
        </fieldset>
        </br>
        <button type="submit" class="btn btn-primary"><fmt:message key="sign_in" /></button>

        <a class="btn btn-outline-success my-2 my-sm-0" href="/index.jsp"><fmt:message key="back" /></a>
    </div>
    </form>
    </div>
    <div>
<footer>
  <my:CopyrightTag />
</footer>
</div>
    <script src="/dist/bundle.js"></script>
  </body>

</html>