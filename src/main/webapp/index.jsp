<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@ taglib prefix="c"
uri="http://java.sun.com/jsp/jstl/core" %> <%@ taglib uri="/CopyrightTag.tld"
prefix="my" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
 <fmt:setLocale value="${language}" />
 <fmt:setBundle basename="messages" />

<html>
  <head>
    <link rel="icon" type="image/png" href="/favicon.png"/>
    <title><fmt:message key="login.button" /></title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="#"><fmt:message key="login.button" /></a>
      <form class="ml-auto">
<select class="form-control form-control-sm" id="language" name="language" onchange="submit()">
  <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
  <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
</select>
</form>
    </nav>
    <div class="jumbotron login-jumbotron">
      <form
        class="form-inline my-2 my-lg-0"
        method="post"
        action="/api/user/login"
      >
      <fmt:message key="login" var="placeholderValueLogin" />
      <fmt:message key="password" var="placeholderValuePassword" />
        <input class="form-control mr-sm-2" placeholder="${placeholderValueLogin}" name="login" />
        <br />
        <input
          class="form-control mr-sm-2"
          placeholder="${placeholderValuePassword}"
          type="password"
          name="password"
        />
        <br />
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
          <fmt:message key="login.button" />
        </button>
      </form>
      <br />
      <br />
      <br />
      <br />
      <a href="/registration.jsp" class="btn btn-outline-success my-2 my-sm-0">
       <fmt:message key="register" />
      </a>
    </div>
    <div>
      <footer>
        <my:CopyrightTag />
      </footer>
    </div>
    <script src="/dist/bundle.js"></script>
  </body>
</html>
