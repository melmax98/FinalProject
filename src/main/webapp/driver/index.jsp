<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="/CopyrightTag.tld" prefix="my" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
 <fmt:setLocale value="${language}" />
 <fmt:setBundle basename="messages" />


<html>
<head> 
<link rel="icon" type="image/png" href="/favicon.png"/> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="driver.panel" /></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/driver/panel"><fmt:message key="driver.panel" /></a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="/logout"><fmt:message key="logout" /> <span class="sr-only">(current)</span></a>
</div>
</div>
<form>
<select class="form-control form-control-sm" id="language" name="language" onchange="submit()">
  <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
  <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
</select>
</form>
</nav>
<br><br><br>
<form action='/driver/index'>
  <fmt:message key="driver.show_cars" var="buttonValue" />
  <input type="submit" class="btn btn-info" value="${buttonValue}"></input>
</form>
<div>
<footer>
  <my:CopyrightTag />
</footer>
</div>
  <script src="/dist/bundle.js"></script>
</body>
</html>