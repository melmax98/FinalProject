<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="/CopyrightTag.tld" prefix="my" %>
     <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
 <fmt:setLocale value="${language}" />
 <fmt:setBundle basename="messages" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "https://www.w3.org/TR/html4/loose.dtd">
<html>
<head> 
<link rel="icon" type="image/png" href="/favicon.png"/> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="driver.panel" /></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/driver/panel"><fmt:message key="driver.panel" /></a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="/logout"><fmt:message key="logout" /> <span class="sr-only">(current)</span></a>
</div>
</div>
<form>
<select class="form-control form-control-sm" id="language" name="language" onchange="submit()">
  <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
  <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
</select>
</form>
</nav>
<br><br>
<div class="take-car">
    <form>
         <div class="form-group">
    <fmt:message key="driver.car_id" var="placeholderValue" />
    <input type="text" class="form-control" name="car_id" placeholder="${placeholderValue}"><br>
    <button class="btn btn-dark take-car-button"><fmt:message key="driver.take_a_car" /></button>
    <button class="btn btn-dark leave-car-button"><fmt:message key="driver.leave_a_car" /></button>
    </form>
  </div>
</div>
<br>

<c:if test="${not empty takenCar}">
    <div class="edit">
        <p><strong>
  <fmt:message key="takenCar"/><c:out value="${takenCar}"/></strong></p>
    </div>
</c:if>
<br>
  <table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col"><fmt:message key="manufacturer" /></th>
      <th scope="col"><fmt:message key="model" /></th>
      <th scope="col"><fmt:message key="type" /></th>
      <th scope="col"><fmt:message key="seats" /></th>
    </tr>
  </thead>
   <c:forEach items="${cars}" var="car">
  <tbody>
    <tr>
      <th scope="col"><c:out value="${car.getId()}" /></th>
      <th scope="col"><c:out value="${car.getManufacturer()}" /></th>
      <th scope="col"><c:out value="${car.getModel()}" /></th>
      <th scope="col"><fmt:message key="orders.carType.${car.getType()}" /></th>
      <th scope="col"><c:out value="${car.getSeats()}" /></th>
    </tr>
  </tbody>
  </c:forEach>
</table>

<nav>
  <ul class="pagination">
      <c:if test="${page > 1}">
     <li class="page-item">
      <a class="page-link" href="/driver/index?page=${page - 1}" tabindex="-1"><fmt:message key="previous" /></a>
    </li>
 </c:if>
 <c:if test= "${page <= 1}">
    <li class="page-item disabled">
      <a class="page-link" tabindex="-1"><fmt:message key="previous" /></a>
    </li>
  </c:if>

      <c:forEach begin='1' end="${pages}" var="p"> 
   
      <c:if test="${p == page}">
      <li class="page-item active">
      <a class="page-link" href="/driver/index?page=${p}">${p}</a>
    </li>
   </c:if>
   <c:if test="${p != page}">
    <li class="page-item"><a class="page-link" href="/driver/index?page=${p}">${p}</a></li>
  </c:if>
    </c:forEach>
    
      <c:if test="${page < pages}">
     <li class="page-item">
      <a class="page-link" href="/driver/index?page=${page + 1}" tabindex="-1"><fmt:message key="next" /></a>
    </li>
    </c:if>
    <c:if test="${page >= pages}">
     <li class="page-item disabled">
      <a class="page-link" tabindex="-1"><fmt:message key="next" /></a>
    </li>
  </c:if>
   
  </ul>
</nav>
<div>
<footer>
  <my:CopyrightTag />
</footer>
</div>

  <script src="/dist/bundle.js"></script>
</body>
</html>