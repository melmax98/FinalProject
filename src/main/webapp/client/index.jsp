<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="/CopyrightTag.tld" prefix="my" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
 <fmt:setLocale value="${language}" />
 <fmt:setBundle basename="messages" />


<html>
<body>
<head>  
<link rel="icon" type="image/png" href="/favicon.png"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="title"/></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="index.jsp"><fmt:message key="title"/></a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="/logout"><fmt:message key="logout"/> <span class="sr-only">(current)</span></a>
</div>
</div>
 <form>
<select class="form-control form-control-sm" id="language" name="language" onchange="submit()">
  <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
  <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
</select>
</form>
</nav>
<br><br><br>
<div class="take-a-ride">
<form action='/api/order'>
  <fmt:message key="address" var="addressPlaceholder"/>
  <label for="from"><fmt:message key="from"/></label>
    <input class="form-control" name="from" placeholder="${addressPlaceholder}">
<label for="to"><fmt:message key="to"/></label>
<input class="form-control" name="to" placeholder="${addressPlaceholder}">
<label for="passengers"><fmt:message key="passengers"/></label>
<input class="form-control" name="passengers">
<fieldset class="form-group">
    <div class="row">
      <legend class="col-form-label col-sm-2 pt-0"><fmt:message key="car_type"/></legend>
      <div class="col-sm-10">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="car_type" value="BASIC" checked>
          <label class="form-check-label" for="car_type">
            <fmt:message key="basic"/>
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="car_type" value="COMFORT">
          <label class="form-check-label" for="car_type">
            <fmt:message key="comfort"/>
          </label>
        </div>
         <div class="form-check">
          <input class="form-check-input" type="radio" name="car_type" value="VAN">
          <label class="form-check-label" for="car_type">
            <fmt:message key="van"/>
          </label>
        </div>
      </div>
    </div>
  </fieldset>
   <fmt:message key="take_a_ride" var="take_ride"/>
  <input type = "submit" class="btn btn-dark" value="${take_ride}">
</form>
</div>
<div>
<footer>
  <my:CopyrightTag />
</footer>
</div>
  <script src="/dist/bundle.js"></script>
</body>
</html>