<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="/CopyrightTag.tld" prefix="my" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
 <fmt:setLocale value="${language}" />
 <fmt:setBundle basename="messages" />

<html>
<body>
<head> 
<link rel="icon" type="image/png" href="/favicon.png"/> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="title"/></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><fmt:message key="title"/></a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <form class="ml-auto">
<select class="form-control form-control-sm" id="language" name="language" onchange="submit()">
  <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
  <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
</select>
</form>
</nav>
<br><br><br>
<h2><fmt:message key="thank"/></h2>
<br>
<div class="finish-order">
  <fmt:message key="finish" var="but"/>
    <form>
        <input type="submit" value="${but}" class="btn btn-dark"/>
    </form>
</div>
<div>
<footer>
  <my:CopyrightTag />
</footer>
</div>
  <script src="/dist/bundle.js"></script>
</body>
</html>