<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="/CopyrightTag.tld" prefix="my" %>
     <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
 <fmt:setLocale value="${language}" />
 <fmt:setBundle basename="messages" />


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "https://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<head>
<link rel="icon" type="image/png" href="/favicon.png"/>  
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><fmt:message key="order.details"/></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><fmt:message key="order.details"/></a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <form class="ml-auto">
<select class="form-control form-control-sm" id="language" name="language" onchange="submit()">
  <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
  <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
</select>
</form>
</nav>
<br>
<div class="jumbotron">
  <table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col"><fmt:message key="car"/></th>
      <th scope="col"><fmt:message key="type"/></th>
      <th scope="col"><fmt:message key="capacity"/></th>
    </tr>
  </thead>
   <c:forEach items="${cars}" var="car">
  <tbody>
    <tr>
      <th scope="col"><c:out value="${car.getManufacturer()} ${car.getModel()}" /></th>
      <th scope="col"><fmt:message key="orders.carType.${car.getType()}"/></th>
      <th scope="col"><c:out value="${car.getSeats()}" /></th>
    </tr>
  </tbody>
  </c:forEach>
</table>
  <br>

  <p><strong>
  <fmt:message key="distance"/><c:out value="${distance}"/><br>
 <fmt:message key="driving_time"/><c:out value="${drivingTime}"/><br>
 <fmt:message key="price"/>: <c:out value="${price}"/> <fmt:message key="uah"/><br>
 <fmt:message key="wait_time"/>: <c:out value="7"/> <fmt:message key="min"/><br></strong></p>

</div>
<div class="confirm-order">
  <fmt:message key="confirm-order" var="confirmOrder"/>
  <fmt:message key="cancel-order" var="cancelOrder"/>
  <form action="confirm-order" method = "post">
      <input value="${confirmOrder}" type="submit" class="btn btn-dark"/>
  </form>
</div>
<br>
<div class="cancel-order">
  <form action="cancel-order" method = "post">
      <input value="${cancelOrder}" type="submit" class="btn btn-danger"/>
  </form>
</div>
<footer>
  <my:CopyrightTag />
</footer>
</div>
 <script src="/dist/bundle.js"></script>
</body>
</html>